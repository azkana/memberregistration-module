<?php

namespace Modules\MemberRegistration\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramMessage;

class MemberRegisterNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['telegram'];
    }

    /**
     * Get the telegram message representation of the notification.
     *
     * @param mixed $notifiable
     * @return \NotificationChannels\Telegram\TelegramMessage;
     */

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to('@azkanamedianotification')
            ->content(
                'DKU - MemberRegistration :: ' .
                $notifiable->full_name . '<' . $notifiable->email . '> registered at ' . Carbon::now()->format('d-m-Y H:i:s')
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
