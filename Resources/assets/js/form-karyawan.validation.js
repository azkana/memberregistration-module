var FormValidation = function () {
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, function(size, element) {
        return "Ukuran file harus kurang dari " + filesize(size, {exponent:2,round:1}) +
            ". Ukuran file kamu " + filesize(element.files[0].size, {exponent:2,round:1});
    });

    var formMember = function () {
        var form = $('#frm_karyawan');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'span',
            errorClass: 'help-block help-block-error small',
            focusInvalid: false,
            noSpace: true,
            ignore: "",
            rules: {
                // Data Diri
                fullname: { required: true },
                firstname: { required: true },
                tmp_lahir: { required: true },
                tgl_lahir: { required: true },
                gender: { required: true },
                agama: { required: true },
                nik: { 
                    required: true, 
                    minlength: 16, 
                    digits: true,
                    remote: {
                        url: "check/nik"
                    }
                },
                ktp_type: { required: true },
                marital: { required: true },
                email: { 
                    required: true, email: true,
                    remote: {
                        url: "check/email"
                    }
                },
                no_hp: { required: true, digits: true },
                npwp: { required: false, minlength: 15, digits: true },
                kk: { required: true, minlength: 16, digits: true },
                mother: { required: true },
                father: { required: true },
                // Alamat Sesuai KTP
                alamat: { required: true },
                alamat_rt: { required: true, digits: true },
                alamat_rw: { required: true, digits: true },
                kelurahan: { required: true },
                kecamatan: { required: true },
                kabupaten: { required: true },
                provinsi: { required: true },
                kodepos: { required: true, minlength: 5, digits: true },
                phone: { required: true, digits: true },
                // Alamat Domisili
                alamat_dom: { required: true },
                alamat_rt_dom: { required: true, digits: true },
                alamat_rw_dom: { required: true, digits: true },
                kelurahan_dom: { required: true },
                kecamatan_dom: { required: true },
                kabupaten_dom: { required: true },
                provinsi_dom: { required: true },
                kodepos_dom: { required: true, minlength: 5, digits: true },
                phone_dom: { required: true, digits: true },
                // Kontak Darurat
                emr_name: { required: true },
                emr_hubkel: { required: true },
                emr_phone: { required: true, digits: true },
                emr_alamat: { required: true },
                emr_alamat_rt: { required: true, digits: true },
                emr_alamat_rw: { required: true, digits: true },
                emr_kelurahan: { required: true },
                emr_kecamatan: { required: true },
                emr_kabupaten: { required: true },
                emr_provinsi: { required: true },
                emr_kodepos: { required: true, minlength: 5, digits: true },
                // Pendidikan
                // jenjang: { required: true },
                sekolah: { required: true },
                jurusan: { required: true },
                sekolah_kab: { required: true },
                thn_lulus: { required: true, minlength: 4, digits: true },
                // Data Bank
                bank_name: { required: false },
                // bank_account_name: { required: false },
                bank_account_no: { required: false, digits: true },
                bank_branch: { required: false },

                jemputan: { required: true },
                tgl_tes: { required: true },
                skck_validity: { required: true },

                file_ktp: {
                    filesize: 1024 * (1024 * 4)
                },
                file_kk: {
                    filesize: 1024 * (1024 * 4)
                },
                file_skck: {
                    filesize: 1024 * (1024 * 4)
                },
                file_tab: {
                    filesize: 1024 * (1024 * 4)
                }
            },
            messages: {
                // Data diri
                fullname: { required: "harus diisi." },
                firstname: { required: "harus diisi." },
                tmp_lahir: { required: "harus diisi." },
                tgl_lahir: { required: "harus diisi." },
                gender: { required: "harus diisi." },
                agama: { required: "harus diisi." },
                nik: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 16 angka", remote: "NIK sudah terdaftar." },
                ktp_type: { required: "harus diisi." },
                marital: { required: "harus diisi." },
                email: { required: "harus diisi.", email: "email harus benar", remote: "Email sudah terdaftar." },
                no_hp: { required: "harus diisi.", digits: "harus angka." },
                npwp: { digits: "harus angka.", minlength: "harus 15 angka" },
                kk: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 16 angka" },
                mother: { required: "harus diisi." },
                father: { required: "harus diisi." },
                // Alamat Sesuai KTP
                alamat: { required: "harus diisi." },
                alamat_rt: { required: "harus diisi.", digits: "harus angka." },
                alamat_rw: { required: "harus diisi.", digits: "harus angka." },
                kelurahan: { required: "harus diisi." },
                kecamatan: { required: "harus diisi." },
                kabupaten: { required: "harus diisi." },
                provinsi: { required: "harus diisi." },
                kodepos: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 5 angka" },
                phone: { required: "harus diisi.", digits: "harus angka." },
                // Alamat Domisili
                alamat_dom: { required: "harus diisi." },
                alamat_rt_dom: { required: "harus diisi.", digits: "harus angka." },
                alamat_rw_dom: { required: "harus diisi.", digits: "harus angka." },
                kelurahan_dom: { required: "harus diisi." },
                kecamatan_dom: { required: "harus diisi." },
                kabupaten_dom: { required: "harus diisi." },
                provinsi_dom: { required: "harus diisi." },
                kodepos_dom: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 5 angka" },
                phone_dom: { required: "harus diisi.", digits: "harus angka." },
                // Kontak Darurat
                emr_name: { required: "harus diisi." },
                emr_hubkel: { required: "harus diisi." },
                emr_phone: { required: "harus diisi.", digits: "harus angka." },
                emr_alamat: { required: "harus diisi." },
                emr_alamat_rt: { required: "harus diisi.", digits: "harus angka." },
                emr_alamat_rw: { required: "harus diisi.", digits: "harus angka." },
                emr_kelurahan: { required: "harus diisi." },
                emr_kecamatan: { required: "harus diisi." },
                emr_kabupaten: { required: "harus diisi." },
                emr_provinsi: { required: "harus diisi." },
                emr_kodepos: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 5 angka" },
                // Pendidikan
                // jenjang: { required: "harus diisi." },
                sekolah: { required: "harus diisi." },
                jurusan: { required: "harus diisi." },
                sekolah_kab: { required: "harus diisi." },
                thn_lulus: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 4 angka" },
                // Data Bank
                bank_name: { required: "harus diisi." },
                // bank_account_name: { required: "harus diisi." },
                bank_account_no: { required: "harus diisi.", digits: "harus angka." },
                bank_branch: { required: "harus diisi." },

                jemputan: { required: "harus diisi." },
                tgl_tes: { required: "harus diisi." },
                skck_validity: { required: "harus diisi." },
            },
            invalidHandler: function(event, validator) {
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) {
                if (element.is(':checkbox')) {
                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                } else if (element.is(':radio')) {
                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                } else if (element.is(':file')) {
                    error.insertAfter(element.closest(".fileinput, .fileinput-new"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            highlight: function(element) {
                $(element)
                    .closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error');
            },
            submitHandler: function(form) {
                success.show();
                error.hide();
                form[0].submit();
            }
        });
    }
    return {
        init: function(){
            formMember();
        }
    }
}();

jQuery(document).ready(function() {
    FormValidation.init();
});