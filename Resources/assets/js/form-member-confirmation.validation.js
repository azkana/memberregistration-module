var FormValidation = function () {

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, function(size, element) {
        return "Ukuran file harus kurang dari " + filesize(size, {exponent:2,round:1}) +
            ". Ukuran file kamu " + filesize(element.files[0].size, {exponent:2,round:1});
    });

    var formMember = function () {
        var form = $('#frm_member_payment_confirmation');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'span',
            errorClass: 'help-block help-block-error small',
            focusInvalid: false,
            noSpace: true,
            ignore: ":not(:visible)",
            rules: {
                bank_account_id: { required: true },
                transfer_date: { required: true },
                transfer_amount: { 
                    required: true,
                    equalTo: subs_price,
                    digits: true
                },
                transfer_bank_id: { required: true },
                transfer_bank_name: { required: true },
                transfer_bank_account: { 
                    required: true,
                    digits: true
                },
                transfer_bank_holder_name: { required: true },
                transfer_attachment: { 
                    required: true,
                    filesize: 1024 * (1024 * 4)
                },
            },
            messages: {
                bank_account_id: { required: "harus diisi." },
                transfer_date: { required: "harus diisi." },
                transfer_amount: { 
                    required: "harus diisi.",
                    equalTo: "Jumlah Bayar harus sama dengan Jumlah Tagihan",
                    digits: "harus angka"
                },
                transfer_bank_id: { required: "harus diisi." },
                transfer_bank_name: { required: "harus diisi." },
                transfer_bank_account: { 
                    required: "harus diisi.",
                    digits: "harus angka."
                },
                transfer_bank_holder_name: { required: "harus diisi." },
                transfer_attachment: { required: "harus diisi." },
            },
            invalidHandler: function(event, validator) {
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) {
                if (element.is(':checkbox')) {
                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                } else if (element.is(':radio')) {
                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                } else if (element.is(':file')) {
                    error.insertAfter(element.closest(".fileinput, .fileinput-new"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            highlight: function(element) {
                $(element)
                    .closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error');
            },
            submitHandler: function(form) {
                success.show();
                error.hide();
                form[0].submit();
            }
        });
    }
    return {
        init: function(){
            formMember();
        }
    }
}();

jQuery(document).ready(function() {
    FormValidation.init();
});