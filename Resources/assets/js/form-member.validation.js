var FormValidation = function () {

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, function(size, element) {
        return  "Ukuran file harus kurang dari " + filesize(size, {exponent:2,round:1}) +
            ". Ukuran file kamu " + filesize(element.files[0].size, {exponent:2,round:1});
    });

    var formMember = function () {
        var form = $('#frm_member');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        form.validate({
            errorElement: 'span',
            errorClass: 'help-block help-block-error small',
            focusInvalid: false,
            noSpace: true,
            ignore: "",
            rules: {
                full_name: { required: true },
                nik: {
                    required: true,
                    minlength: 16,
                    digits: true,
                    remote: {
                        url: "check/nik"
                    }
                },
                email: {
                    required: true, email: true,
                    remote: {
                        url: "check/email"
                    }
                },
                no_hp: { required: true },
                no_wa: { required: true },
                tmp_lahir: { required: true },
                tgl_lahir: {
                    required: true,
                    date: true
                },
                gender: { required: true },
                tb: { required: true, digits: true },
                bb: { required: true, digits: true },
                alamat: { required: true },
                kabupaten: { required: true },
                provinsi: { required: true },
                kodepos: { required: true, minlength: 5, digits: true },
                jenjang: { required: true },
                sekolah: { required: true },
                jurusan: { required: true },
                thn_lulus: { required: true },
                pas_foto: { extension: "jpg,jpeg,png", filesize: 1024 * (1024 * 4) },
                tnc: { required: true },
            },
            messages: {
                full_name: { required: "harus diisi." },
                nik: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 16 angka", remote: "NIK sudah terdaftar." },
                email: { required: "harus diisi.", email: "email harus benar", remote: "Email sudah terdaftar." },
                no_hp: { required: "harus diisi." },
                no_wa: { required: "harus diisi." },
                tmp_lahir: { required: "harus diisi." },
                tgl_lahir: {
                    required: "harus diisi.",
                    date: "tanggal harus benar."
                },
                gender: { required: "harus diisi." },
                tb: { required: "harus diisi.", digits: "harus angka." },
                bb: { required: "harus diisi.", digits: "harus angka." },
                alamat: { required: "harus diisi." },
                kabupaten: { required: "harus diisi." },
                provinsi: { required: "harus diisi." },
                kodepos: { required: "harus diisi.", digits: "harus angka.", minlength: "harus 5 angka" },
                jenjang: { required: "harus diisi." },
                sekolah: { required: "harus diisi." },
                jurusan: { required: "harus diisi." },
                thn_lulus: { required: "harus diisi." },
                pas_foto: { extension: "file harus .png, .jpg, .jpeg" },
                tnc: { required: "harus di centang." },
            },
            invalidHandler: function(event, validator) {
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },
            errorPlacement: function(error, element) {
                if (element.is(':checkbox')) {
                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                } else if (element.is(':radio')) {
                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                } else if (element.is(':file')) {
                    error.insertAfter(element.closest(".fileinput, .fileinput-new"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            highlight: function(element) {
                $(element)
                    .closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error');
            },
            submitHandler: function(form) {
                success.show();
                error.hide();
                form[0].submit();
            }
        });
    }
    return {
        init: function(){
            formMember();
        }
    }
}();

jQuery(document).ready(function() {
    FormValidation.init();
});
