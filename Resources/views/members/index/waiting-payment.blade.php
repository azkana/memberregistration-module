@extends('memberregistration::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title">Waiting Payment</h3>
                        </div>
                        <div class="col-md-6">
                            <div class="box-tools pull-right" id="btn-table">
                                <a class="btn btn-sm btn-warning" href="{!! route('member.register.form') !!}" target="_blank">
                                    <i class="fa fa-file-text-o"></i> Form
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="start_date" class="small">Start Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('start_date', $startDate, [ 'class' => 'form-control datepicker input-sm', 'id' => 'start_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="end_date" class="small">End Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('end_date', $endDate, [ 'class' => 'form-control datepicker input-sm', 'id' => 'end_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="button" class="small"></label>
                                        <div class="input-group">
                                            <button class="btn btn-sm btn-success" style="margin-top: 4.5px" id="btn_search">
                                                <i class="fa fa-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="grid-members" class="table table-bordered table-striped" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 2%; vertical-align: middle">#</th>
                                            <th class="text-center" style="width: 10%; vertical-align: middle">NIK</th>
                                            <th class="text-center" style="vertical-align: middle">Nama Lengkap</th>
                                            <th class="text-center" style="vertical-align: middle">Gender</th>
                                            <th class="text-center" style="vertical-align: middle">Email</th>
                                            <th class="text-center" style="width: 10%; vertical-align: middle">Tgl.<br>Daftar</th>
                                            <th class="text-center" style="width: 10%; vertical-align: middle">Jumlah<br>Tagihan</th>
                                            <th class="text-center" style="width: 10%; vertical-align: middle">Tgl.<br>Jatuh Tempo</th>
                                            <th class="text-center" style="width: 10%; vertical-align: middle">Status</th>
                                            <th class="text-center" style="width: 2%; vertical-align: middle">
                                                <i class="fa fa-navicon"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="detail">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div id="content"></div>
                        </div>
                    </div>
                </div>
                <div id="confirmation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirmation">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div id="confirmation_content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .datepicker table tr td.disabled{
            color:red;
            cursor: not-allowed;
        }
        .datepicker table tr td.today{
            background: orange;
        }
        .datepicker table tr td.active{
            font-weight: bold;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var startDate = $('#start_date');
            var endDate = $('#end_date');
            var btnSearch = $('#btn_search');

            var table = $('#grid-members').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paging: true,
                keys: false,
                scrollX: true,
                stateSave: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        extend: 'selectAll',
                        text: 'Select All',
                        className: "btn-sm",
                    },
                    {
                        extend:'selectNone',
                        text: 'Select None',
                        className: "btn-sm",
                    },
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: {
                    url: "{!! route('member.waiting-payment.data') !!}",
                    data: function(d) {
                        d.start_date = startDate.val();
                        d.end_date = endDate.val();
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'nik', name: 'nik', className: "small", orderable: false},
                    {data: 'full_name', name: 'full_name', className: "small"},
                    {data: 'gender', name: 'gender', className: "small text-center", orderable: false},
                    {data: 'email', name: 'email', className: "small", orderable: false},
                    {data: 'reg_date', name: 'reg_date', className: "small text-right"},
                    {data: 'invoice_amount', name: 'invoice_amount', className: "small text-right", orderable: false},
                    {data: 'payment_due_date', name: 'payment_due_date', className: "small text-right"},
                    {data: 'expired', name: 'expired', className: "small"},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [
                    {targets: 5, render: $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD-MM-YYYY' )},
                    {targets: 6, render: $.fn.dataTable.render.number( '.', ',', 0, '' )},
                    {targets: 7, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )}
                ],
                order: [
                    [5, 'asc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });

            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                endDate: '+0d',
                language: 'id'
            });

            btnSearch.on('click', function(e) {
                e.preventDefault();
                getDataMember();
            });

            $('.dataTables_scrollBody').css('height', '350px');

            function getDataMember() {
                $.ajax({
                    url: "{!! route('member.waiting-payment.data') !!}",
                    type: "GET",
                    data: { 
                        start_date: startDate.val(),
                        end_date: endDate.val()
                    },
                    success: function(data) {
                        table.ajax.reload();
                    }
                });
            }

            function getTime(d) {
                return new Date(d.split("-").reverse().join("-")).getTime()
            }

        });

        function getDetail(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.show.modal') !!}",
                data: {id: id},
                success: function(data) {
                    $('#detail').modal('toggle');
                    $('#content').html(data);
                }
            });
        }

        function editEmail(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.edit-email') !!}",
                data: {id: id},
                success: function(data) {
                    $('#confirmation').modal('toggle');
                    $('#confirmation_content').html(data);
                }
            });
        }

        function resendEmail(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.resend-email') !!}",
                data: {id: id},
                success: function(data) {
                    $('#confirmation').modal('toggle');
                    $('#confirmation_content').html(data);
                }
            });
        }

        function uploadBuktiTransfer(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.upload-bukti-transfer') !!}",
                data: {id: id},
                success: function(data) {
                    $('#detail').modal('toggle');
                    $('#content').html(data);
                }
            });
        }

    </script>
@endsection
