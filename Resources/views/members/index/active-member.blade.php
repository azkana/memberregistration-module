@extends('memberregistration::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title">Active Members</h3>
                        </div>
                        <div class="col-md-6">
                            <div class="box-tools pull-right" id="btn-table">
                                @can('member-delete')
                                <button class="btn btn-sm btn-danger hide" id="bulk_delete">
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </button>
                                @endcan
                                <a class="btn btn-sm btn-warning" href="{!! route('member.register.form') !!}" target="_blank">
                                    <i class="fa fa-file-text-o"></i> Form
                                </a>
                                @can('member-export')
                                <a class="btn btn-sm btn-success" href="#" data-toggle="modal" data-target="#export">
                                    <i class="fa fa-cloud-download"></i> Export
                                </a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="start_date" class="small">Start Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('start_date', $startDate, [ 'class' => 'form-control datepicker input-sm text-center', 'id' => 'start_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="end_date" class="small">End Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('end_date', $endDate, [ 'class' => 'form-control datepicker input-sm text-center', 'id' => 'end_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="button" class="small"></label>
                                        <div class="input-group">
                                            <button class="btn btn-sm btn-success" style="margin-top: 4.5px" id="btn_search">
                                                <i class="fa fa-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="grid-members" class="table table-bordered table-striped" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 2%; vertical-align: middle"></th>
                                            <th class="text-center" style="width: 2%; vertical-align: middle">#</th>
                                            <th class="text-center" style="width: 5%; vertical-align: middle">No. Member</th>
                                            <th class="text-center" style="vertical-align: middle">N.I.K</th>
                                            <th class="text-center" style="vertical-align: middle">Nama Lengkap</th>
                                            <th class="text-center" style="vertical-align: middle">Email</th>
                                            <th class="text-center" style="vertical-align: middle">Kota</th>
                                            <th class="text-center" style="width: 2%; vertical-align: middle">JK</th>
                                            <th class="text-center" style="width: 5%; vertical-align: middle">Pendidikan</th>
                                            <th class="text-center" style="width: 50px; vertical-align: middle">Tgl. Daftar</th>
                                            <th class="text-center" style="width: 10%; vertical-align: middle">Tgl. Approved</th>
                                            <th class="text-center" style="width: 5%; vertical-align: middle">
                                                <i class="fa fa-navicon"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade modal-default" id="export" tabindex="-1" role="dialog" aria-labelledby="export">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open([ 'route' => ['member.exports'], 'method' => 'get', 'class' => 'form-horizontal' ]) !!}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">
                                    Export Data Active Member
                                </h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="date_type" class="col-sm-3 control-label">Jenis Tanggal</label>
                                            <div class="col-sm-4">
                                                {!! Form::select('date_type', $dateType, null, [ 'class' => 'form-control text-uppercase small']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl_from" class="col-sm-3 control-label">Tanggal <span class="required">*</span></label>
                                            <div class="col-sm-3">
                                                {!! Form::text('tgl_from', date('d-m-Y'), [ 'class' => 'form-control datepicker', 'id' => 'tgl_from', 'required']) !!}
                                            </div>
                                            <label for="tgl_to" class="col-sm-2 control-label">Sampai <span class="required">*</span></label>
                                            <div class="col-sm-3">
                                                {!! Form::text('tgl_to', date('d-m-Y'), [ 'class' => 'form-control datepicker', 'id' => 'tgl_to', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender" class="col-sm-3 control-label">Jenis Kelamin</label>
                                            <div class="col-sm-3">
                                                {!! Form::select('gender', $gender, null, [ 'class' => 'form-control text-uppercase small', 'placeholder' => 'Semua']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="file_type" class="col-sm-3 control-label">Jenis File <span class="required">*</span></label>
                                            <div class="col-sm-6">
                                                <div class="checkbox">
                                                    <label>
                                                        {!! Form::checkbox('file_type', 'xlsx', true, ['required']) !!} <i class="fa fa-file-excel-o"></i> Ms. Excel
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-cloud-download"></i>
                                    Export
                                </button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="detail">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div id="content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .datepicker table tr td.disabled{
            color:red;
            cursor: not-allowed;
        }
        .datepicker table tr td.today{
            background: orange;
        }
        .datepicker table tr td.active{
            font-weight: bold;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var btnBulkDelete = $('#bulk_delete');
            var startDate = $('#start_date');
            var endDate = $('#end_date');
            var btnSearch = $('#btn_search');

            var table = $('#grid-members').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                scrollX: true,
                stateSave: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        extend: 'selectAll',
                        text: 'Select All',
                        className: "btn-sm",
                    },
                    {
                        extend:'selectNone',
                        text: 'Select None',
                        className: "btn-sm",
                    },
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: { 
                    url: "{!! route('member.active-member.data') !!}",
                    data: function(d) {
                        d.start_date = startDate.val();
                        d.end_date = endDate.val();
                    }
                },
                columns: [
                    {data: 'checkboxes', name: 'checkboxes', orderable: false, searchable: false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'id_member', name: 'id_member', className: "small"},
                    {data: 'nik', name: 'nik', className: "small"},
                    {data: 'full_name', name: 'full_name', className: "small"},
                    {data: 'email', name: 'email', className: "small"},
                    {data: 'kabupaten', name: 'kabupaten', className: "small"},
                    {data: 'gender', name: 'gender', className: "small text-center", searchable: false},
                    {data: 'jenjang', name: 'jenjang', className: "small"},
                    {data: 'reg_date', name: 'reg_date', className: "small"},
                    {data: 'approved_at', name: 'approved_at', className: "small"},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {targets: 0, className: 'select-checkbox'},
                    {targets: 9, render: $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD-MM-YYYY' )},
                    {targets: 10, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )},
                ],
                order: [
                    [9, 'desc'],
                    [10, 'desc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });

            table.on( 'select', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count > 0 ) {
                    btnBulkDelete.removeClass('hide');
                }
            } );

            table.on( 'deselect', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count === 0 ) {
                    btnBulkDelete.addClass('hide');
                }
            } );

            btnBulkDelete.on('click', function(e) {
                e.preventDefault();
                var countRows = table.rows( { selected: true } ).data().count();
                swal({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this ' + countRows + ' row(s)?',
                    buttons: ["Cancel", "Yes"],
                    dangerMode: true,
                    closeOnClickOutside: false,
                }).then(function(value) {
                    if(value) {
                        var selectedRows = table.rows( { selected: true } ).data().to$();
                        var selectedId = Array();
                        $.each(selectedRows, function(i, e) {
                            selectedId.push(e.id);
                        });

                        $.ajax({
                            url: "{!! route('member.destroy.bulk') !!}",
                            type: "post",
                            data: {
                                _method: "delete",
                                _token: "{!! csrf_token() !!}",
                                id: selectedId,
                            },
                            success: function(res, stat, xhr) {
                                swal({
                                    text: res.message,
                                    icon: 'success',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                                table.draw();
                                btnBulkDelete.addClass('hide');
                            },
                            error: function(err) {
                                swal({
                                    text: err,
                                    icon: 'error',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                            }
                        });
                    }
                });

            });

            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                endDate: '+1d',
                language: 'id'
            });

            btnSearch.on('click', function(e) {
                e.preventDefault();
                getDataMember();
            });

            $('.dataTables_scrollBody').css('height', '350px');

            function getDataMember() {
                $.ajax({
                    url: "{!! route('member.active-member.data') !!}",
                    type: "GET",
                    data: { 
                        start_date: startDate.val(),
                        end_date: endDate.val(),
                    },
                    success: function(data) {
                        table.ajax.reload();
                    }
                });
            }

            function getTime(d) {
                return new Date(d.split("-").reverse().join("-")).getTime()
            }

        });

        function getDetail(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.show.modal') !!}",
                data: {id: id},
                success: function(data) {
                    $('#detail').modal('toggle');
                    $('#content').html(data);
                }
            });
        }

    </script>
@endsection
