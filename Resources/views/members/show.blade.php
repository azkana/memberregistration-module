@extends('memberregistration::layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary" style="min-height: 580px">
                <div class="box-body box-profile">
                    @if(!empty($data->pas_foto))
                    <img class="profile-user-img img-responsive img-circle" src="{!! asset('storage/members/'.$data->nik.'/'.$data->pas_foto) !!}" alt="User profile picture">
                    @else
                    <img class="profile-user-img img-responsive img-circle" src="{!! asset('images/default-avatar.png') !!}" alt="User profile picture">
                    @endif
                    <h3 class="profile-username text-center text-uppercase">{!! $data->full_name !!}</h3>
                    <p class="text-muted text-center">{!! $data->tmp_lahir !!}, {!! Carbon::parse($data->tgl_lahir)->format('d-m-Y') !!}</p>
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-xs btn-warning btn-block text-uppercase" data-toggle="{!! isset($data->has_subs->payment_status) && $data->has_subs->payment_status == 'CP' ? 'modal' : null !!}" data-target="#confirm-{!!$data->id!!}">
                                <i class="fa fa-info-circle"></i>
                                @if(isset($data->has_subs->payment_status) && $data->has_subs->payment_status == 'AM')
                                <b>{!! $data->status !!}</b>
                                @else
                                <b>{!! isset($data->has_subs->payment_status) ? getMemberStatus($data->has_subs->payment_status) : $data->status !!}</b>
                                @endif
                            </a>
                            <a href="{!! route('member.print.pdf', $data->id) !!}" class="btn btn-primary btn-block">
                                <i class="fa fa-file-pdf-o"></i>
                                <b>Download</b>
                            </a>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 5px">
                        <div class="col-md-6">
                            <a href="{!! route('member.edit', $data->id) !!}" class="btn btn-sm btn-success btn-block">
                                <i class="fa fa-edit"></i>
                                <b>Edit</b>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#delete-{!!$data->id!!}">
                                <i class="fa fa-trash"></i>
                                <b>Delete</b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#datadiri" data-toggle="tab">Data Diri</a></li>
                    <li><a href="#alamat" data-toggle="tab">Alamat</a></li>
                    <li><a href="#pendidikan" data-toggle="tab">Pendidikan</a></li>
                    <li><a href="#pengalaman" data-toggle="tab">Pengalaman</a></li>
                    <li><a href="#keahlian" data-toggle="tab">Keahlian</a></li>
                    <li><a href="#dokumen" data-toggle="tab">Dokumen</a></li>
                    <li><a href="#payment" data-toggle="tab">Payment</a></li>
                    <div class="box-tools pull-right">
                        <a class="btn btn-sm btn-danger" href="{!! route('member.index') !!}" style="margin-top: 7px; margin-right: 7px;">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </ul>
                <div class="tab-content" style="min-height: 540px">
                    <div class="active tab-pane" id="datadiri">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">No. Member</th>
                                        <td>
                                            {!! $data->id_member !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <td class=" text-uppercase">{!! $data->full_name !!}</td>
                                    </tr>
                                    <tr>
                                        <th>NIK (KTP)</th>
                                        <td>{!! $data->nik !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{!! $data->email !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Nomor HP</th>
                                        <td>{!! $data->no_hp !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Nomor WA</th>
                                        <td>{!! $data->no_wa !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Tempat Lahir</th>
                                        <td class=" text-uppercase">{!! $data->tmp_lahir !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Lahir</th>
                                        <td>{!! Carbon::parse($data->tgl_lahir)->format('d-m-Y') !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kelamin</th>
                                        <td>{!! $data->gender !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Tinggi Badan</th>
                                        <td>{!! $data->tb !!} <small>cm</small></td>
                                    </tr>
                                    <tr>
                                        <th>Berat Badan</th>
                                        <td>{!! $data->bb !!} <small>kg</small></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                    <div class="tab-pane" id="alamat">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Alamat</th>
                                        <td class="text-uppercase">{!! $data->alamat !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kota/Kab.</th>
                                        <td class="text-uppercase">{!! $data->kabupaten !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Provinsi</th>
                                        <td class="text-uppercase">
                                            @if(isset($data->rel_provinsi->name))
                                                {!! $data->rel_provinsi->name !!}
                                            @else
                                                {!! $data->provinsi !!}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kodepos</th>
                                        <td>{!! $data->kodepos !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pendidikan">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Jenjang</th>
                                        <td class="text-uppercase">{!! $data->jenjang !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Sekolah / Universitas</th>
                                        <td class="text-uppercase">{!! $data->sekolah !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jurusan</th>
                                        <td class="text-uppercase">{!! $data->jurusan !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Tahun Lulus</th>
                                        <td>{!! $data->thn_lulus !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pengalaman">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Pengalaman 1</h3>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Perusahaan</th>
                                        <td class="text-uppercase">{!! $data->exp_company_1 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jabatan</th>
                                        <td class="text-uppercase">{!! $data->exp_position_1 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Lama Bekerja</th>
                                        <td>
                                            {!! !empty($data->exp_duration_year_1) ? $data->exp_duration_year_1 . ' tahun' : null !!}
                                            {!! !empty($data->exp_duration_month_1) ? $data->exp_duration_month_1 . ' bulan' : null !!}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h3>Pengalaman 2</h3>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Perusahaan</th>
                                        <td class="text-uppercase">{!! $data->exp_company_2 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jabatan</th>
                                        <td class="text-uppercase">{!! $data->exp_position_2 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Lama Bekerja</th>
                                        <td>
                                            {!! !empty($data->exp_duration_year_2) ? $data->exp_duration_year_2 . ' tahun' : null !!}
                                            {!! !empty($data->exp_duration_month_2) ? $data->exp_duration_month_2 . ' bulan' : null !!}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="keahlian">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    @forelse(explode(',', $data->skills) as $skill)
                                    <tr>
                                        <td>{!! $skill !!}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <th class="text-danger">Tidak ada keahlian</th>
                                    </tr>
                                    @endforelse
                                </table>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="dokumen">
                        <div class="row">
                            <div class="col-md-6">
                                @if(!empty($data->pas_foto))
                                <img class="img-responsive" src="{!! asset('storage/members/'.$data->nik.'/'.$data->pas_foto) !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @else
                                <img class="img-responsive" src="{!! asset('images/no-image.png') !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @endif
                                Pas Foto
                            </div>
                            <div class="col-md-6">
                                @if(!empty($data->id_card))
                                <img class="img-responsive" src="{!! asset('storage/members/'.$data->nik.'/'.$data->id_card) !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @else
                                <img class="img-responsive" src="{!! asset('images/no-image.png') !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @endif
                                KTP
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="payment">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Data Pembayaran</h4>
                                @if(isset($data->has_subs))
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Biaya Member</th>
                                        <td class="text-uppercase">{!! isset($data->has_subs->subs_price) ? numberFormat($data->has_subs->subs_price) : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kode Unik</th>
                                        <td class="text-uppercase">{!! isset($data->has_subs->unique_no) ? $data->has_subs->unique_no : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Pembayaran</th>
                                        <td class="text-uppercase">{!! numberFormat( (isset($data->has_subs->subs_price) ? $data->has_subs->subs_price : 0) + (isset($data->has_subs->unique_no) ? $data->has_subs->unique_no : 0)) !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Batas Waktu Pembayaran</th>
                                        <td>
                                            {!! isset($data->has_subs->payment_due_date) ? date('d-m-Y H:i:s', strtotime($data->has_subs->payment_due_date)) : null !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Status Pembayaran</th>
                                        <td>
                                            <span class="label label-warning">{!! getMemberStatus($data->has_subs->payment_status) !!}</span>
                                        </td>
                                    </tr>
                                </table>
                                @else
                                <span class="text-danger">Belum ada data pembayaran.</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <h4>Konfirmasi Pembayaran</h4>
                                @if(isset($data->has_subs->has_confirmation))
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Transfer Ke</th>
                                        <td class="text-uppercase">
                                            @if($data->has_subs->has_confirmation->bank_account_id == 'DKUCMB')
                                            CIMB NIAGA - 800132891100
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Jumlah Transfer</th>
                                        <td class="">{!! numberFormat($data->has_subs->has_confirmation->transfer_amount) !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Transfer</th>
                                        <td>
                                            {!! date('d-m-Y', strtotime($data->has_subs->has_confirmation->transfer_date)) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Dari Bank</th>
                                        <td>
                                            {!! $data->has_subs->has_confirmation->bank_rel->name !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>No. Rekening</th>
                                        <td>
                                            {!! $data->has_subs->has_confirmation->transfer_bank_account !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Nama Pemegang Rekening</th>
                                        <td>
                                            {!! $data->has_subs->has_confirmation->transfer_bank_holder_name !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Konfirmasi</th>
                                        <td>
                                            {!! date('d-m-Y H:i:s', strtotime($data->has_subs->has_confirmation->created_at)) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Lampiran</th>
                                        <td>
                                            @if(!empty($data->has_subs->has_confirmation->transfer_attachment))
                                            <img src="{!! asset('storage/members/'.$data->nik.'/'.$data->has_subs->has_confirmation->transfer_attachment) !!}" alt="" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px">
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                                @else
                                <span class="text-danger">Belum ada konfirmasi pembayaran.</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Delete  --}}
        <div class="modal fade" id="delete-{!!$data->id!!}" tabindex="-1" role="dialog" aria-labelledby="delete">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open([ 'route' => ['member.destroy', $data->id], 'method' => 'delete' ]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                        Apakah anda yakin akan menghapus data member <b>{!! $data->full_name !!}</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        {{-- Approve Payment --}}
        <div class="modal fade" id="confirm-{!!$data->id!!}" tabindex="-1" role="dialog" aria-labelledby="confirm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open([ 'route' => ['member.payment.approve', $data->id], 'method' => 'patch' ]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Konfirmasi Pembayaran</h4>
                    </div>
                    <div class="modal-body">
                        Apakah anda yakin akan menyetujui konfirmasi pembayaran dari <b>{!! $data->full_name !!}</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Confirm</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection