<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>MEMBERSHIP CARD</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style media="screen">
            @page { margin: 1.5cm 1.5cm 1.5cm 1.5cm; }
            body {
                font-family: 'Roboto', sans-serif;
                font-size: 14px;
            }
            .uppercase {
                text-transform: uppercase;
            }
            .tabel {
                width: 100%;
                border-collapse: collapse;
            }
        </style>
    </head>
    <body>
        <table class="tabel" border="0">
            <tr>
                <td width="20%" style="text-align: center">
                    <img src="{!! asset('images/favicon-dku.png') !!}" style="max-width: 100px" />
                </td>
                <td style="text-align: center">
                    <span>{!! $date->formatLocalized('%A, %e %B %Y') !!}</span>
                    <h2 class="uppercase">{!! $data->full_name !!}</h2>
                    <h3>{!! $data->id_member !!}</h3>
                </td>
                <td width="20%" style="text-align: center">
                    <div style="">
                        @if(!empty($data->pas_foto))
                            <img src="{!! Storage::disk('s3')->url('members/' . $data->nik . '/'.$data->pas_foto) !!}" style="width:113px;height:133px" />
                        @endif
                    </div>
                </td>
            </tr>
        </table>
        <hr>
        <table style="width:100%" border="0">
            <tr>
                <td width="50%" style="vertical-align: top">
                    <table style="width:100%;line-height: 1.5em" border="0">
                        <tr>
                            <th style="width: 120px">NIK (KTP)</th>
                            <td><small>{!! $data->nik !!}</small></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><small>{!! strtolower($data->email) !!}</small></td>
                        </tr>
                        <tr>
                            <th>No. HP</th>
                            <td><small>{!! $data->no_hp !!}</small></td>
                        </tr>
                        <tr>
                            <th>Tempat Lahir</th>
                            <td><small>{!! strtoupper($data->tmp_lahir) !!}</small></td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td><small>{!! Carbon::parse($data->tgl_lahir)->format('d-m-Y') !!}</small></td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                <small>
                                    @if($data->gender == 'L')
                                        Laki-laki
                                    @elseif($data->gender == 'P')
                                        Perempuan
                                    @endif
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <th>Sekolah</th>
                            <td><small>({!! strtoupper($data->jenjang) !!}) {!! strtoupper($data->sekolah) !!}</small></td>
                        </tr>
                        <tr>
                            <th>Jurusan</th>
                            <td><small>{!! strtoupper($data->jurusan) !!}</small></td>
                        </tr>
                        <tr>
                            <th>Tahun Lulus</th>
                            <td><small>{!! $data->thn_lulus !!}</small></td>
                        </tr>
                        <tr>
                            <th style="width: 120px;vertical-align: top">Alamat</th>
                            <td>
                                <small>
                                    {!! strtoupper($data->alamat) !!},
                                    {!! strtoupper($data->kabupaten) !!},
                                    {!! strlen($data->provinsi) > 2 ? strtoupper($data->provinsi) : uppercase($data->rel_provinsi->name) !!},
                                    {!! $data->kodepos !!}
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <th>Tinggi Badan</th>
                            <td>
                                <small>{!! $data->tb !!} cm</small>
                            </td>
                        </tr>
                        <tr>
                            <th>Berat Badan</th>
                            <td>
                                <small>{!! $data->bb !!} kg</small>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="50%" style="vertical-align: top;line-height: 1.5em">
                    <table style="width:100%;" border="0">
                        <tr>
                            <th colspan="2">- Pengalaman Kerja -</th>
                        </tr>
                        <tr>
                            <th style="width: 120px;vertical-align: top">Perusahaan</th>
                            <td><small>{!! $data->exp_company_1 !!}</small></td>
                        </tr>
                        <tr>
                            <th>Jabatan</th>
                            <td><small>{!! $data->exp_position_1 !!}</small></td>
                        </tr>
                        <tr>
                            <th>Lama Bekerja</th>
                            <td>
                                <small>
                                {!! !empty($data->exp_duration_year_1) ? $data->exp_duration_year_1 . ' tahun' : null !!} 
                                {!! !empty($data->exp_duration_month_1) ? $data->exp_duration_month_1 . ' bulan' : null !!}
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <th>Perusahaan</th>
                            <td><small>{!! $data->exp_company_2 !!}</small></td>
                        </tr>
                        <tr>
                            <th>Jabatan</th>
                            <td><small>{!! $data->exp_position_2 !!}</small></td>
                        </tr>
                        <tr>
                            <th>Lama Bekerja</th>
                            <td>
                                <small>
                                {!! !empty($data->exp_duration_year_2) ? $data->exp_duration_year_2 . ' tahun' : null !!} 
                                {!! !empty($data->exp_duration_month_2) ? $data->exp_duration_month_2 . ' bulan' : null !!}
                                </small>
                            </td>
                        </tr>
                        @php $skills = explode(',', $data->skills) @endphp
                        <tr>
                            <th style="vertical-align: top; padding-top: 10px">Keahlian</th>
                            <td style="vertical-align: top">
                                <small>
                                    @if(!empty($data->skills))
                                        <ul style="margin-left: -20px">
                                            @foreach($skills as $skill)
                                            <li>{!! $skill !!}</li>
                                            @endforeach
                                        </ul>
                                    @else
                                        -
                                    @endif
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                Tanda Tangan,
                            </th>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-top: 50px">
                                <b>{!! str_repeat("_", 25) !!}</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="width:100%;margin-top: 20px">
            <tr>
                <th colspan="2" style="text-align: center;border-top: 1px solid;border-bottom: 1px solid;padding:10px">MEMBERSHIP CARD</th>
            </tr>
            <tr>
                <td width="40%" style="vertical-align: top;line-height: 1.5em">
                    <table style="width:100%;" border="0">
                        <tr>
                            <td colspan="2">
                                {!! $date->formatLocalized('%e %B %Y') !!}
                            </td>
                        </tr>
                        <tr>
                            <th width="50%" style="padding-top: 20px">NO. MEMBER</th>
                            <td style="padding-top: 20px">
                                <small>{!! $data->id_member !!}</small>
                            </td>
                        </tr>
                        <tr>
                            <th>NAMA LENGKAP</th>
                            <td>
                                <small>{!! strtoupper($data->full_name) !!}</small>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: justify;padding-top: 20px">
                                <small>
                                    Telah membayar biaya administrasi pendaftaran
                                    member sebesar Rp. {!! numberFormat(getOptions('membership_subscription')) !!} 
                                    (
                                        {!! 
                                            ucwords(
                                                RioAstamal\AngkaTerbilang\Terbilang::create()->terbilang(
                                                    (string) getOptions('membership_subscription')
                                                ) . ' rupiah'
                                            ) 
                                        !!}
                                    )
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">Admin</th>
                        </tr>
                        <tr>
                            <th colspan="2" style="padding-top: 50px">
                                {!! str_repeat("_", 35) !!}
                            </th>
                        </tr>
                        <tr>
                            <td colspan="2">www.jobsdku.co.id | 021-82424255</td>
                        </tr>
                    </table>
                </td>
                <td width="60%" style="vertical-align: top">
                    <table style="width:100%;" border="0">
                        <tr>
                            <th style="padding-left: 30px">
                                TATA TERTIB MEMBER PT. DKU :
                            </th>
                        </tr>
                        <tr>
                            <td style="text-align: justify">
                                <small>
                                <ul>
                                    <li>Parkir diluar komplek Puri Hutama</li>
                                    <li>Arah jalan keluar dari PT. DKU melalui jalan depan ruko perkantoran Puri Hutama & tidak bergerombol</li>
                                    <li>Lepas sepatu & simpan di tempat yang disediakan</li>
                                    <li>Duduk di tempat yang telah di sediakan, dan tidak duduk diluar area PT. DKU</li>
                                    <li>Yang dijemput oleh keluarga, saudara / temannya harap penjemputan dilakukan diluar komplek Puri Hutama</li>
                                    <li>Apabila pesan Gojek / Grab harap pemesanan dilakukan diluar komplek Puri Hutama</li>
                                    <li>Buang sampah ke tempat yang disediakan</li>
                                    <li>Tidak mencoret-coret kursi/meja, sopan, mendahulukan duduk di kursi depan</li>
                                    <li>Apabila ada pertanyaan silahkan hubungi ke no. telepon yang ada di kwitansi member pada hari & jam kerja</li>
                                </ul>
                                </small>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>