
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Payment Approval</h4>
</div>
<form id="member_payment_approval">
@csrf
{!! Form::hidden('_method', 'PATCH') !!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="box-title">Data Tagihan</h4>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(isset($data->has_subs))
                            <div class="table-responsive">
                            <table id="" class="table table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>Biaya Member</th>
                                        <th>Kode Unik</th>
                                        <th>Total Pembayaran</th>
                                        <th>Batas Waktu Pembayaran</th>
                                        <th>Status Pembayaran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-right">{!! isset($data->has_subs->subs_price) ? numberFormat($data->has_subs->subs_price) : null !!}</td>
                                        <td class="text-right">{!! isset($data->has_subs->unique_no) ? $data->has_subs->unique_no : null !!}</td>
                                        <td class="text-right text-bold">{!! numberFormat( (isset($data->has_subs->subs_price) ? $data->has_subs->subs_price : 0) + (isset($data->has_subs->unique_no) ? $data->has_subs->unique_no : 0)) !!}</td>
                                        <td>{!! isset($data->has_subs->payment_due_date) ? date('d-m-Y H:i:s', strtotime($data->has_subs->payment_due_date)) : null !!}</td>
                                        <td><span class="label label-warning">{!! getMemberStatus($data->has_subs->payment_status) !!}</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            @else
                            <span class="text-danger">Belum ada data pembayaran.</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="box-title">Data Pembayaran</h4>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(isset($data->has_subs->has_confirmation))
                            <div class="table-responsive">
                            <table id="" class="table table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>Transfer ke</th>
                                        <th>Jumlah Transfer</th>
                                        <th>Tanggal Transfer</th>
                                        <th>Dari Bank</th>
                                        <th>No. Rekening</th>
                                        <th>Pemegang Rekening</th>
                                        <th>Tanggal Konfirmasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if($data->has_subs->has_confirmation->bank_account_id == 'DKUCMB')
                                                CIMB NIAGA - 800132891100
                                            @endif
                                        </td>
                                        <td>{!! numberFormat($data->has_subs->has_confirmation->transfer_amount) !!}</td>
                                        <td>{!! date('d-m-Y', strtotime($data->has_subs->has_confirmation->transfer_date)) !!}</td>
                                        <td>
                                            {!! $data->has_subs->has_confirmation->bank_rel->name !!}
                                            @if($data->has_subs->has_confirmation->transfer_bank_id == '999')
                                                @if(!empty($data->has_subs->has_confirmation->transfer_bank_name))
                                                    ({!! $data->has_subs->has_confirmation->transfer_bank_name !!})
                                                @endif
                                            @endif
                                        </td>
                                        <td>{!! $data->has_subs->has_confirmation->transfer_bank_account !!}</td>
                                        <td>{!! $data->has_subs->has_confirmation->transfer_bank_holder_name !!}</td>
                                        <td>{!! date('d-m-Y H:i:s', strtotime($data->has_subs->has_confirmation->created_at)) !!}</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <div class="table-responsive">
                            <table id="" class="table table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>Bukti Transfer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if(!empty($data->has_subs->has_confirmation->transfer_attachment))
                                                @if(config('memberregistration.disk') == 's3')
                                                    @if(Storage::disk('s3')->exists('members/'.$data->nik.'/'.$data->has_subs->has_confirmation->transfer_attachment))
                                                    <img data-src="{!! Storage::disk('s3')->url('members/'.$data->nik.'/'.$data->has_subs->has_confirmation->transfer_attachment) !!}" src="{!! asset('img/loading-bar.gif') !!}" alt="" style="max-width:400px;max-height:400px;border:1px solid #ECF0F5;border-radius:10px">
                                                    @else
                                                    <img src="{!! asset('storage/members/'.$data->nik.'/'.$data->has_subs->has_confirmation->transfer_attachment) !!}" alt="" style="max-width:500px;max-height:500px;border:1px solid #ECF0F5;border-radius:10px">
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            @else
                            <span class="text-danger">Belum ada konfirmasi pembayaran.</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-12 control-label">Apakah anda yakin akan menyetujui pembayaran dari <b>{!! $data->full_name !!}</b>?</label>
        </div>
        <div class="form-group">
            <label for="member_no" class="col-sm-2 control-label">No. Member <span class="required">*</span></label>
            <div class="col-sm-3">
                {!! Form::text('member_no', $data->id_member, [ 'class' => 'form-control input-sm text-uppercase', 'id' => 'member_no', 'maxlength' => 7, 'autocomplete' => 'off', 'autofocus', 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="approve_date" class="col-sm-2 control-label">Approve Date <span class="required">*</span></label>
            <div class="col-sm-3">
                {!! Form::text('approve_date', date('d-m-Y'), [ 'class' => 'form-control input-sm datepicker', 'id' => 'approve_date', 'autocomplete' => 'off', 'required']) !!}
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" id="submit"><i class="fa fa-check-square-o"></i> Confirm</button>
</div>
</form>
<style>
    .errors {
        color:#FF0000;  /* red */
    }
</style>
<script>
    $(document).ready(function() {
        new EagerImageLoader();
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            endDate: '+1d',
            language: 'id'
        });
        $("#submit").on('click', function(e) {
            $("#member_payment_approval").validate({
                errorClass: 'errors small',
                rules: {
                    member_no: {
                        required: true,
                        digits: true,
                        maxlength: 7,
                        minlength: 5,
                        remote: {
                            url: "check/nomember"
                        }
                    },
                    approve_date: {
                        required: true
                    }
                },
                messages: {
                    member_no: {
                        required: "harus diisi.",
                        digits: "harus angka",
                        maxlength: "harus 7 digit",
                        minlength: "harus 5 digit",
                        remote: "No. Member sudah dipakai."
                    },
                    approve_date: {
                        required: "harus diisi."
                    }
                },
                submitHandler: function(form) {
                    $.ajax({
                        type: "POST",
                        url: "{!! url('manage/membership/members/payment/approve/confirmation') !!}/{!! $data->id !!}",
                        data: $(form).serialize(),
                        success: function(data) {
                            $("#detail").modal('hide');
                            toastr.success(data.message);
                            $('#grid-members').DataTable().ajax.reload(null, false);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });
</script>