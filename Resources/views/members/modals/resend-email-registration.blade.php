<form id="member_resend_email" class="form-horizontal">
    @csrf
    {!! Form::hidden('_method', 'PATCH') !!}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Resend Email Registration</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="full_name" class="col-sm-4 control-label">Nama Lengkap </label>
                    <div class="col-sm-6">
                        {!! Form::text('full_name', $data->full_name, [ 'class' => 'form-control input-sm', 'id' => 'full_name', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">Email </label>
                    <div class="col-sm-6">
                        {!! Form::email('email', $data->email, [ 'class' => 'form-control input-sm', 'id' => 'email', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="subs_price" class="col-sm-4 control-label">Biaya Member </label>
                    <div class="col-sm-6">
                        {!! Form::text('subs_price', numberFormat($data->has_subs->subs_price + $data->has_subs->unique_no), [ 'class' => 'form-control input-sm', 'id' => 'subs_price', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="reg_date" class="col-sm-4 control-label">Daftar </label>
                    <div class="col-sm-6">
                        {!! Form::text('reg_date', dateFormatDmyHi($data->created_at), [ 'class' => 'form-control input-sm', 'id' => 'reg_date', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="due_date" class="col-sm-4 control-label">Jatuh Tempo </label>
                    <div class="col-sm-6">
                        {!! Form::text('due_date', dateFormatDmyHi($data->has_subs->payment_due_date), [ 'class' => 'form-control input-sm', 'id' => 'due_date', 'readonly']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" id="submit"><i class="fa fa-send"></i> Send</button>
    </div>
</form>
<style>
    .errors {
        color:#FF0000;  /* red */
    }
</style>
<script>
    $(document).ready(function() {
        $("#submit").on('click', function(e) {
            $("#member_resend_email").validate({
                errorClass: 'errors small',
                rules: { },
                messages: { },
                submitHandler: function(form) {
                    $.ajax({
                        type: "POST",
                        url: "{!! url('manage/membership/members/resend/email-registration') !!}/{!! $data->id !!}",
                        data: $(form).serialize(),
                        success: function(data) {
                            $("#confirmation").modal('hide');
                            toastr.success(data.message);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });
</script>