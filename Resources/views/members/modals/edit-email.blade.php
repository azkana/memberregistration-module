<form id="member_email_edit" class="form-horizontal">
    @csrf
    {!! Form::hidden('_method', 'PATCH') !!}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Edit Email</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="old_email" class="col-sm-4 control-label">Email Lama </label>
                    <div class="col-sm-6">
                        {!! Form::text('old_email', $data->email, [ 'class' => 'form-control input-sm', 'id' => 'old_email', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="new_email" class="col-sm-4 control-label">Email Baru <span class="required">*</span></label>
                    <div class="col-sm-6">
                        {!! Form::email('new_email', null, [ 'class' => 'form-control input-sm', 'id' => 'new_email', 'required', 'autofocus']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" id="submit"> Update</button>
    </div>
</form>
<style>
    .errors {
        color:#FF0000;  /* red */
    }
</style>
<script>
    $(document).ready(function() {
        $("#submit").on('click', function(e) {
            $.validator.addMethod("notEqualTo", function(value, element, param) {
                return this.optional(element) || value != $(param).val();
            }, "email tidak boleh sama");
            $("#member_email_edit").validate({
                errorClass: 'errors small',
                rules: {
                    new_email: {
                        required: true,
                        email: true,
                        notEqualTo: "#old_email"
                    }
                },
                messages: {
                    new_email: {
                        required: 'harus diisi',
                        email: 'email harus benar'
                    }
                },
                submitHandler: function(form) {
                    $.ajax({
                        type: "POST",
                        url: "{!! url('manage/membership/members/edit/email') !!}/{!! $data->id !!}",
                        data: $(form).serialize(),
                        success: function(data) {
                            $("#confirmation").modal('hide');
                            toastr.success(data.message);
                            $('#grid-members').DataTable().ajax.reload(null, false);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });
</script>