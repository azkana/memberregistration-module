<form id="member_upload_bukti_transfer" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
    @csrf
    {!! Form::hidden('_method', 'PATCH') !!}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Upload Bukti Transfer</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="full_name" class="col-sm-5 control-label">Nama Lengkap </label>
                            <div class="col-sm-7">
                                {!! Form::text('full_name', $data->full_name, [ 'class' => 'form-control input-sm', 'id' => 'full_name', 'readonly']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nik" class="col-sm-5 control-label">NIK </label>
                            <div class="col-sm-7">
                                {!! Form::text('nik', $data->nik, [ 'class' => 'form-control input-sm', 'id' => 'nik', 'readonly']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-5 control-label">Email </label>
                            <div class="col-sm-7">
                                {!! Form::email('email', $data->email, [ 'class' => 'form-control input-sm', 'id' => 'email', 'readonly']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subs_price" class="col-sm-5 control-label">Biaya Member </label>
                            <div class="col-sm-7">
                                {!! Form::text('subs_price', numberFormat($data->has_subs->subs_price + $data->has_subs->unique_no), [ 'class' => 'form-control input-sm', 'id' => 'subs_price', 'readonly']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="reg_date" class="col-sm-5 control-label">Daftar </label>
                            <div class="col-sm-7">
                                {!! Form::text('reg_date', dateFormatDmyHi($data->created_at), [ 'class' => 'form-control input-sm', 'id' => 'reg_date', 'readonly']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="due_date" class="col-sm-5 control-label">Jatuh Tempo </label>
                            <div class="col-sm-7">
                                {!! Form::text('due_date', dateFormatDmyHi($data->has_subs->payment_due_date), [ 'class' => 'form-control input-sm', 'id' => 'due_date', 'readonly']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="bank_account_id" class="col-sm-5 control-label">Bank Tujuan </label>
                            <div class="col-sm-7">
                                {!! Form::select('bank_account_id', $bankAccount, isset($data->has_subs->has_confirmation->bank_account_id) ? $data->has_subs->has_confirmation->bank_account_id : null, [ 'class' => 'form-control text-uppercase', 'id' => 'bank_account_id', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="transfer_date" class="col-sm-5 control-label">Tanggal Transfer </label>
                            <div class="col-sm-4">
                                {!! Form::text('transfer_date', isset($data->has_subs->has_confirmation->transfer_date) ? dateFormatDmy($data->has_subs->has_confirmation->transfer_date) : date('d-m-Y'), [ 'class' => 'form-control input-sm datepicker text-center', 'id' => 'transfer_date', 'autocomplete' => 'off', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="transfer_amount" class="col-sm-5 control-label">Jumlah Transfer </label>
                            <div class="col-sm-4">
                                {!! Form::text('transfer_amount', isset($data->has_subs->has_confirmation->transfer_amount) ? $data->has_subs->has_confirmation->transfer_amount : 0, [ 'class' => 'form-control input-sm number text-right', 'id' => 'transfer_amount', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="transfer_bank_id" class="col-sm-5 control-label">Bank Asal </label>
                            <div class="col-sm-7">
                                {!! Form::select('transfer_bank_id', $bank, isset($data->has_subs->has_confirmation->transfer_bank_id) ? $data->has_subs->has_confirmation->transfer_bank_id : null, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_id', 'placeholder' => 'Pilih', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group group-bank-name hide">
                            <label for="transfer_bank_name" class="col-sm-5 control-label">Nama Bank Lain </label>
                            <div class="col-sm-7">
                                {!! Form::text('transfer_bank_name', isset($data->has_subs->has_confirmation->transfer_bank_name) ? $data->has_subs->has_confirmation->transfer_bank_name : null, [ 'class' => 'form-control input-sm', 'id' => 'transfer_bank_name']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="transfer_bank_account" class="col-sm-5 control-label">No. Rekening </label>
                            <div class="col-sm-7">
                                {!! Form::text('transfer_bank_account', isset($data->has_subs->has_confirmation->transfer_bank_account) ? $data->has_subs->has_confirmation->transfer_bank_account : null, [ 'class' => 'form-control input-sm', 'id' => 'transfer_bank_account', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="transfer_bank_holder_name" class="col-sm-5 control-label">Pemilik Rekening </label>
                            <div class="col-sm-7">
                                {!! Form::text('transfer_bank_holder_name', isset($data->has_subs->has_confirmation->transfer_bank_holder_name) ? $data->has_subs->has_confirmation->transfer_bank_holder_name : null, [ 'class' => 'form-control input-sm', 'id' => 'transfer_bank_holder_name', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="transfer_attachment" class="col-sm-5 control-label">Bukti Transfer </label>
                            <div class="col-sm-7">
                                @php
                                    if(!isset($data->has_subs->has_confirmation->transfer_attachment) || empty($data->has_subs->has_confirmation->transfer_attachment)) {
                                        $fileInputClass = '';
                                        $fileImageClass = 'hide';
                                    } else {
                                        $fileInputClass = 'hide';
                                        $fileImageClass = '';
                                    }
                                @endphp
                                {!! Form::file('transfer_attachment', [ 'class' => 'form-control ' . $fileInputClass, 'id' => 'file_input']) !!}
                                @if(isset($data->has_subs->has_confirmation->transfer_attachment))
                                <div id="file_image" class="{!! $fileImageClass !!}">
                                    <img src="{!! Storage::disk('s3')->url('members/'.$data->nik.'/'.$data->has_subs->has_confirmation->transfer_attachment) !!}" 
                                    alt="" style="max-height:200px;max-width:250px;border:1px solid #ECF0F5;border-radius:10px">
                                    <a href="#" type="button" class="btn btn-xs btn-danger pull-right" title="Delete" onclick="deleteFile({!! $data->id !!});return false;">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" id="submit">
            @if(!isset($data->has_subs->has_confirmation->transfer_attachment))
                <i class="fa fa-cloud-upload"></i> Upload
            @else
                <i class="fa fa-edit"></i> Ubah
            @endif
        </button>
    </div>
</form>
<style>
    .errors {
        color:#FF0000;
    }
</style>
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            endDate: '+0d',
            language: 'id'
        });
        $('.number').number(true,0);
        $('#transfer_bank_id').on('change', function(e) {
            var bank_id = e.target.value;
            var group_bank_name = $('.group-bank-name');
            if(bank_id === '999') {
                group_bank_name.removeClass('hide');
            } else {
                group_bank_name.addClass('hide');
            }
        });
        var subsCost = "{!! numberFormat($data->has_subs->subs_price) !!}";
        $("#submit").on('click', function(e) {
            $("#member_upload_bukti_transfer").validate({
                errorClass: 'errors small',
                rules: {
                    transfer_amount: { required: true, digits: true, min: 50000 },
                    transfer_bank_account: { required: true },
                    transfer_bank_holder_name: { required: true },
                    transfer_bank_id: { required: true }
                 },
                messages: {
                    transfer_amount: { required: "harus diisi", digits: "harus angka", min: "minimal " + subsCost },
                    transfer_bank_account: { required: "harus diisi" },
                    transfer_bank_holder_name: { required: "harus diisi" },
                    transfer_bank_id: { required: "harus diisi" }
                },
                submitHandler: function(form) {
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "{!! url('manage/membership/members/upload-bukti-transfer') !!}/{!! $data->id !!}",
                        data: formData,
                        mimeType: 'multipart/form-data',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            $("#detail").modal('hide');
                            toastr.success(data.message);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });

    function deleteFile(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.upload-bukti-transfer.delete-file') !!}",
                data: {id: id},
                success: function(data) {
                    $('#confirmation').modal('toggle');
                    $('#confirmation_content').html(data);
                }
            });
        }
</script>