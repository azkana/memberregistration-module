
{!! Form::hidden('id', $data->id, ['id' => 'file_id']) !!}
{!! Form::hidden('token', csrf_token(), ['id' => 'token']) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Confirmation Delete </h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <span>Apakah anda yakin akan menghapus file bukti transfer dari <strong>{!! $data->full_name !!}</strong>?</span>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-danger" id="submit_delete"><i class="fa fa-trash"></i> Delete</button>
</div>
<script>
    $(document).ready(function() {
        $("#submit_delete").on('click', function(e) {
            let id = $('#file_id').val();
            let token = $('#token').val();
            $.ajax({
                type: "POST",
                url: "{!! route('member.upload-bukti-transfer.delete-file.save') !!}",
                data: {
                    id: id,
                    _method: 'DELETE',
                    _token: token
                },
                success: function(data) {
                    $("#confirmation").modal('hide');
                    toastr.success(data.message);
                    $("#file_image").addClass('hide');
                    $("#file_input").removeClass('hide');
                },
                error: function(error) {
                    toastr.error(error.message);
                }
            });
        });
    });
</script>