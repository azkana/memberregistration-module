@extends('memberregistration::layouts.master')

@section('content')
    <iframe src="{!! route('member.print.pdf', $data->id) !!}" width="100%" height="600" frameborder="1"></iframe>
@endsection