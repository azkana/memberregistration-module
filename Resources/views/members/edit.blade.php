@extends('memberregistration::layouts.master')

@section('content')

    {!! Form::open([ 'route' => ['member.update', $data->id], 'method' => 'patch', 'class' => 'form-horizontal', 'files' => true, 'autocomplete' => 'off' ]) !!}

    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Diri</h3>
                    <div class="box-tools pull-right">
                        <a class="btn btn-xs btn-danger" href="{!! route('member.index') !!}">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="id_member" class="col-sm-4 control-label">No. Member <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('id_member', $data->id_member, [ 'class' => 'form-control text-uppercase', 'maxlength' => 7, 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="full_name" class="col-sm-4 control-label">Nama Lengkap <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('full_name', $data->full_name, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nik" class="col-sm-4 control-label">NIK (KTP) <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('nik', $data->nik, [ 'class' => 'form-control', 'maxlength' => 16, 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-4 control-label">Email <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::email('email', $data->email, [ 'class' => 'form-control text-lowercase', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="no_hp" class="col-sm-4 control-label">Nomor HP <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('no_hp', $data->no_hp, [ 'class' => 'form-control', 'maxlength' => 30, 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="tmp_lahir" class="col-sm-4 control-label">Tempat Lahir <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('tmp_lahir', $data->tmp_lahir, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_lahir" class="col-sm-4 control-label">Tanggal Lahir <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('tgl_lahir', Carbon::parse($data->tgl_lahir)->format('d/m/Y'), [ 'class' => 'form-control', 'id' => 'tgl_lahir', 'data-inputmask' =>
                                    "'alias': 'dd/mm/yyyy'", 'data-mask', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gender" class="col-sm-4 control-label">Jenis Kelamin <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <label>
                                        {!! Form::radio('gender', 'L', $data->gender == 'L' ? true : false, [ 'required']) !!} Laki-laki
                                    </label>
                                    <label>
                                        {!! Form::radio('gender', 'P', $data->gender == 'P' ? true : false, [ 'required']) !!} Perempuan
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tb" class="col-sm-4 control-label">TB / BB<span class="required">*</span></label>
                                <div class="col-sm-2">
                                    {!! Form::text('tb', $data->tb, [ 'class' => 'form-control', 'id' => 'tb', 'placeholder' => 'TB', 'required']) !!}
                                </div>
                                <label for="bb" class="col-sm-1 control-label">/</label>
                                <div class="col-sm-2">
                                    {!! Form::text('bb', $data->bb, [ 'class' => 'form-control', 'id' => 'bb', 'placeholder' => 'BB', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Alamat (Sesuai KTP)</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="alamat" class="col-sm-4 control-label">Alamat <span
                                        class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::textarea('alamat', $data->alamat, [ 'class' => 'form-control text-uppercase', 'rows' => 2, 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kabupaten" class="col-sm-4 control-label">Kota/Kab. <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('kabupaten', $data->kabupaten, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="provinsi" class="col-sm-4 control-label">Provinsi <span
                                        class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('provinsi', $data->provinsi, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kodepos" class="col-sm-4 control-label">Kodepos <span
                                        class="required">*</span></label>
                                <div class="col-sm-3">
                                    {!! Form::text('kodepos', $data->kodepos, [ 'class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pendidikan Terakhir</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="jenjang" class="col-sm-4 control-label">Jenjang <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::select('jenjang', $jenjang, $data->jenjang, [ 'class' => 'form-control', 'placeholder' => 'Pilih', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="sekolah" class="col-sm-4 control-label">Sekolah / Universitas <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('sekolah', $data->sekolah, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jurusan" class="col-sm-4 control-label">Jurusan <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('jurusan', $data->jurusan, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="thn_lulus" class="col-sm-4 control-label">Tahun Lulus <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::text('thn_lulus', $data->thn_lulus, [ 'class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengalaman Kerja</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exp_company_1" class="col-sm-4 control-label">Perusahaan </label>
                                <div class="col-sm-6">
                                    {!! Form::text('exp_company_1', $data->exp_company_1, [ 'class' => 'form-control text-uppercase']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exp_position_1" class="col-sm-4 control-label">Jabatan</label>
                                <div class="col-sm-6">
                                    {!! Form::text('exp_position_1', $data->exp_position_1, [ 'class' => 'form-control text-uppercase']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exp_duration_year_1" class="col-sm-4 control-label">Lama Kerja</label>
                                <div class="col-sm-3">
                                    {!! Form::text('exp_duration_year_1', $data->exp_duration_year_1, [ 'class' => 'form-control', 'placeholder' => 'Tahun']) !!}
                                </div>
                                <div class="col-sm-3">
                                    {!! Form::text('exp_duration_month_1', $data->exp_duration_month_1, [ 'class' => 'form-control', 'placeholder' => 'Bulan']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exp_company_2" class="col-sm-4 control-label">Perusahaan </label>
                                <div class="col-sm-6">
                                    {!! Form::text('exp_company_2', $data->exp_company_2, [ 'class' => 'form-control text-uppercase']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exp_position_2" class="col-sm-4 control-label">Jabatan</label>
                                <div class="col-sm-6">
                                    {!! Form::text('exp_position_2', $data->exp_position_2, [ 'class' => 'form-control text-uppercase']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exp_duration_year_2" class="col-sm-4 control-label">Lama Kerja</label>
                                <div class="col-sm-3">
                                    {!! Form::text('exp_duration_year_2', $data->exp_duration_year_2, [ 'class' => 'form-control', 'placeholder' => 'Tahun']) !!}
                                </div>
                                <div class="col-sm-3">
                                    {!! Form::text('exp_duration_month_2', $data->exp_duration_month_2, [ 'class' => 'form-control', 'placeholder' => 'Bulan']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Keahlian</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="" class="col-sm-1 control-label"></label>
                                <div class="col-sm-8">
                                    @foreach($skills as $skill)
                                    <div class="checkbox">
                                        <label>
                                            {!! Form::checkbox('skill[]', $skill, in_array($skill, $selectedSkills) ? true : false) !!} {!! $skill !!}
                                        </label>
                                    </div>
                                    @endforeach
                                    <br>
                                    {!! Form::textarea('skill_lain', array_diff($selectedSkills, $skills) ? implode(',', array_diff($selectedSkills, $skills)) : null, [ 'class' => 'form-control input-sm', 'rows' => 1, 'placeholder' => 'Lainnya']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Dokumen</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            @if(!empty($data->pas_foto))
                            <div class="form-group">
                                <label for="pas_foto" class="col-sm-4 control-label"></label>
                                <div class="col-sm-6">
                                    <img class="img-responsive" src="{!! Storage::disk('s3')->url('members/'.$data->nik.'/'.$data->pas_foto) !!}" alt="Photo"
                                        style="max-height:100px" />
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="pas_foto" class="col-sm-4 control-label">Pas Foto</label>
                                <div class="col-sm-6">
                                    {!! Form::file('pas_foto', null, [ 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            @if(!empty($data->id_card))
                            <div class="form-group">
                                <label for="pas_foto" class="col-sm-4 control-label"></label>
                                <div class="col-sm-6">
                                    <img class="img-responsive" src="{!! Storage::disk('s3')->url('members/'.$data->nik.'/'.$data->id_card)!!}"
                                        alt="Photo" style="max-height:100px" />
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="ktp" class="col-sm-4 control-label">KTP</label>
                                <div class="col-sm-6">
                                    {!! Form::file('ktp', null, [ 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="status" class="col-sm-4 control-label">Status <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    {!! Form::select('status', $status, $data->status, [ 'class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@endsection

@section('scripts')
    <script>
        $(function () {
            $('#tgl_lahir').inputmask('dd/mm/yyyy', {'placeholder':'dd/mm/yyyy'});
        });
    </script>
@endsection