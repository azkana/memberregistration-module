@extends('memberregistration::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title"></h3>
                        </div>
                        <div class="col-md-6">
                            <div class="box-tools pull-right">
                                @can('member-delete')
                                <button class="btn btn-sm btn-danger hide" id="bulk_delete">
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </button>
                                @endcan
                                {{-- @can('candidate-create') --}}
                                <a class="btn btn-sm btn-warning" href="{!! route('candidate.form') !!}" target="_blank">
                                    <i class="fa fa-file-text-o"></i> Form
                                </a>
                                {{-- <a class="btn btn-sm btn-primary" href="{!! route('member.create') !!}">
                                    <i class="fa fa-plus"></i> Member Baru
                                </a> --}}
                                {{-- @if($data->count() > 0) --}}
                                <a class="btn btn-sm btn-success" href="{!! route('candidate.excel') !!}">
                                    <i class="fa fa-file-excel-o"></i> Excel
                                </a>
                                {{-- @endif --}}
                                {{-- @endcan --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="start_date" class="small">Start Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('start_date', $startDate, [ 'class' => 'form-control datepicker input-sm', 'id' => 'start_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6 pull-right">
                                    <div class="form-group">
                                        <label for="end_date" class="small">End Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('end_date', $endDate, [ 'class' => 'form-control datepicker input-sm', 'id' => 'end_date']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="grid-karyawan" class="table table-bordered table-striped table-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-center" style="width: 2%">#</th>
                                <th class="text-center">NIK</th>
                                <th class="text-center">Nama Lengkap</th>
                                <th class="text-center" style="width: 7%">Tgl. Lahir</th>
                                <th class="text-center">Kota/Kab.</th>
                                <th class="text-center">Pendidikan</th>
                                <th class="text-center">Jurusan</th>
                                <th class="text-center">JK</th>
                                <th class="text-center">Email</th>
                                <th class="text-center" style="width: 100px">Tgl. Daftar</th>
                                <th class="text-center" style="width: 5%">
                                    <i class="fa fa-navicon"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .datepicker table tr td.disabled{
            color:red;
            cursor: not-allowed;
        }
        .datepicker table tr td.today{
            background: orange;
        }
        .datepicker table tr td.active{
            font-weight: bold;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var btnBulkDelete = $('#bulk_delete');
            var startDate = $('#start_date');
            var endDate = $('#end_date');

            var table = $("#grid-karyawan").DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                paging: true,
                keys: false,
                scrollX: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        extend: 'selectAll',
                        text: 'Select All',
                        className: "btn-sm",
                    },
                    {
                        extend:'selectNone',
                        text: 'Select None',
                        className: "btn-sm",
                    },
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: { 
                    url: "{!! route('candidate.index.data') !!}",
                    data: function(d) {
                        d.start_date = startDate.val();
                        d.end_date = endDate.val();
                    }
                },
                columns: [
                    {data: 'checkboxes', name: 'checkboxes', orderable: false, searchable: false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'nik', name: 'nik', className: "small"},
                    {data: 'fullname', name: 'fullname', className: "small"},
                    {data: 'tgl_lahir', name: 'tgl_lahir', className: "small"},
                    {data: 'kabupaten', name: 'kabupaten', className: "small"},
                    {data: 'sekolah', name: 'sekolah', className: "small"},
                    {data: 'jurusan', name: 'jurusan', className: "small"},
                    {data: 'gender', name: 'gender', className: "small text-center"},
                    {data: 'email', name: 'email', className: "small"},
                    {data: 'created_at', name: 'created_at', className: "small"},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {
                        targets: 0,
                        className: 'select-checkbox'
                    },
                    {targets: 4, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY' )},
                    {targets: 10, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )}
                ],
                order: [[ 10, 'desc' ]],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });

            table.on( 'select', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count > 0 ) {
                    btnBulkDelete.removeClass('hide');
                }
            } );

            table.on( 'deselect', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count === 0 ) {
                    btnBulkDelete.addClass('hide');
                }
            } );

            btnBulkDelete.on('click', function(e) {
                e.preventDefault();
                var countRows = table.rows( { selected: true } ).data().count();
                swal({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this ' + countRows + ' row(s)?',
                    buttons: ["Cancel", "Yes"],
                    dangerMode: true,
                    closeOnClickOutside: false,
                }).then(function(value) {
                    if(value) {
                        var selectedRows = table.rows( { selected: true } ).data().to$();
                        var selectedId = Array();
                        $.each(selectedRows, function(i, e) {
                            selectedId.push(e.id);
                        });

                        $.ajax({
                            url: "{!! route('candidate.destroy.bulk') !!}",
                            type: "post",
                            data: {
                                _method: "delete",
                                _token: "{!! csrf_token() !!}",
                                id: selectedId,
                            },
                            success: function(res, stat, xhr) {
                                swal({
                                    text: res.message,
                                    icon: 'success',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                                table.draw();
                                btnBulkDelete.addClass('hide');
                            },
                            error: function(err) {
                                swal({
                                    text: err,
                                    icon: 'error',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                            }
                        });
                    }
                });

            });

            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                endDate: '+0d',
                language: 'id'
            });

            $('.dataTables_scrollBody').css('height', '350px');

            startDate.on('change', function(e) {
                e.preventDefault();
                getDataMember();
            });

            endDate.on('change', function(e) {
                e.preventDefault();
                getDataMember();
            });

            function getDataMember() {
                $.ajax({
                    url: "{!! route('candidate.index.data') !!}",
                    type: "GET",
                    data: { 
                        start_date: startDate.val(),
                        end_date: endDate.val(),
                    },
                    success: function(data) {
                        table.ajax.reload();
                    }
                });
            }
        });
    </script>
@endsection
