@extends('memberregistration::layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    @if(!empty($data->pas_foto))
                    <img class="profile-user-img img-responsive img-circle" src="{!! asset('storage/members/'.$data->nik.'/'.$data->pas_foto) !!}" alt="User profile picture">
                    @else
                    <img class="profile-user-img img-responsive img-circle" src="{!! asset('images/default-avatar.png') !!}" alt="User profile picture">
                    @endif
                    <br><br>
                    <h3 class="profile-username text-center text-uppercase">{!! $data->fullname !!}</h3>
                    <p class="text-muted text-center">{!! $data->tmp_lahir !!}, {!! Carbon::parse($data->tgl_lahir)->format('d M Y') !!}</p>
                    <p class="text-muted text-center">Tanggal Tes: {!! Carbon::parse($data->tgl_tes)->format('d M Y') !!}</p>
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-xs btn-warning btn-block text-uppercase">
                                <i class="fa fa-info-circle"></i> 
                                <b>{!! $data->status !!}</b>
                            </a>
                            <a href="{!! route('member.print.pdf', $data->id) !!}" class="btn btn-primary btn-block">
                                <i class="fa fa-file-pdf-o"></i> 
                                <b>Download</b>
                            </a>
                        </div>
                    </div> --}}
                    <div class="row" style="margin-top: 5px">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-sm btn-success btn-block" disabled>
                                <i class="fa fa-edit"></i>
                                <b>Edit</b>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#delete-{!!$data->id!!}">
                                <i class="fa fa-trash"></i>
                                <b>Delete</b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#datadiri" data-toggle="tab">Data Pribadi</a></li>
                    <li><a href="#kontak-darurat" data-toggle="tab">Kontak Darurat</a></li>
                    <li><a href="#alamat" data-toggle="tab">Alamat</a></li>
                    <li><a href="#pendidikan" data-toggle="tab">Pendidikan & Bank</a></li>
                    <li><a href="#pengalaman" data-toggle="tab">Pengalaman Kerja</a></li>
                    <li><a href="#dokumen" data-toggle="tab">Dokumen</a></li>
                    <div class="box-tools pull-right">
                        <a class="btn btn-sm btn-danger" href="{!! route('candidate.index') !!}" style="margin-top: 7px; margin-right: 7px;">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="datadiri">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Nama Lengkap</th>
                                        <td class=" text-uppercase">{!! $data->fullname !!}</td>
                                    </tr>
                                    <tr>
                                        <th>NIK <small>(No. KTP)</small></th>
                                        <td>
                                            {!! $data->nik !!} 
                                            <small>
                                                ({!! $data->ktp_type == 1 ? 'SEUMUR HIDUP' : 'RESI/SUKET' !!})
                                            </small>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kelamin</th>
                                        <td>{!! $data->gender !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Tempat Lahir</th>
                                        <td class=" text-uppercase">{!! $data->tmp_lahir !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Lahir</th>
                                        <td>{!! Carbon::parse($data->tgl_lahir)->format('d M Y') !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{!! $data->email !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Nomor HP</th>
                                        <td>{!! $data->no_hp !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Masa Berlaku SKCK</th>
                                        <td>{!! !empty($data->skck_validity) ? Carbon::parse($data->skck_validity)->format('d M Y') : null !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Agama</th>
                                        <td class=" text-uppercase">{!! isset($data->religion->name) ? $data->religion->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Status Perkawinan</th>
                                        <td>{!! getMaritalStatusForEpsonDetail($data->marital) !!}</td>
                                    </tr>
                                    <tr>
                                        <th>No. KK</th>
                                        <td>{!! $data->kk !!}</td>
                                    </tr>
                                    <tr>
                                        <th>No. NPWP</th>
                                        <td>{!! !empty($data->npwp) ? NPWPFormat($data->npwp) : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Nama Ibu</th>
                                        <td>{!! $data->mother !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Nama Ayah</th>
                                        <td>{!! $data->father !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Rute Jemputan</th>
                                        <td>{!! getRuteJemputanDetail($data->jemputan) !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                    <div class="tab-pane" id="kontak-darurat">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Nama</th>
                                        <td class=" text-uppercase">{!! $data->emr_name !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Hubungan Keluarga</th>
                                        <td class=" text-uppercase">{!! getHubKelForEpsonDetail($data->emr_hubkel) !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Telpon</th>
                                        <td class=" text-uppercase">{!! $data->emr_phone !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat</th>
                                        <td class=" text-uppercase">
                                            {!! $data->emr_alamat !!}
                                            @if(!empty($data->emr_alamat_rt) && !empty($data->emr_alamat_rw))
                                            RT/RW. {!! $data->emr_alamat_rt . '/' . $data->emr_alamat_rw !!}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kelurahan</th>
                                        <td class=" text-uppercase">{!! $data->emr_kelurahan !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kecamatan</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_kec_emr->name) ? $data->rel_kec_emr->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kota/Kab.</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_kab_emr->name) ? $data->rel_kab_emr->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Provinsi</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_provinsi_emr->name) ? $data->rel_provinsi_emr->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kodepos</th>
                                        <td>{!! $data->emr_kodepos !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="alamat">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Alamat Sesuai KTP</h4>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Alamat</th>
                                        <td class=" text-uppercase">
                                            {!! $data->alamat !!}
                                            @if(!empty($data->alamat_rt) && !empty($data->alamat_rw))
                                            RT/RW. {!! $data->alamat_rt . '/' . $data->alamat_rw !!}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kelurahan</th>
                                        <td class=" text-uppercase">{!! $data->kelurahan !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kecamatan</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_kec_ktp->name) ? $data->rel_kec_ktp->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kota/Kab.</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_kab_ktp->name) ? $data->rel_kab_ktp->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Provinsi</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_provinsi_ktp->name) ? $data->rel_provinsi_ktp->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kodepos</th>
                                        <td>{!! $data->kodepos !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Telpon</th>
                                        <td class="">{!! $data->phone !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h4>Alamat Domisili</h4>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Alamat</th>
                                        <td class=" text-uppercase">
                                            {!! $data->alamat_dom !!}
                                            @if(!empty($data->alamat_rt_dom) && !empty($data->alamat_rw_dom))
                                            RT/RW. {!! $data->alamat_rt_dom . '/' . $data->alamat_rw_dom !!}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Kelurahan</th>
                                        <td class=" text-uppercase">{!! $data->kelurahan_dom !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kecamatan</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_kec_dom->name) ? $data->rel_kec_dom->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kota/Kab.</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_kab_dom->name) ? $data->rel_kab_dom->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Provinsi</th>
                                        <td class=" text-uppercase">{!! isset($data->rel_provinsi_dom->name) ? $data->rel_provinsi_dom->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kodepos</th>
                                        <td>{!! $data->kodepos_dom !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Telpon</th>
                                        <td class="">{!! $data->phone_dom !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pendidikan">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Pendidikan</h4>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Sekolah / Universitas</th>
                                        <td class="text-uppercase">{!! $data->sekolah !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jurusan</th>
                                        <td class="text-uppercase">{!! $data->jurusan !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Kota/Kab.</th>
                                        <td class="text-uppercase">{!! $data->sekolah_kab !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Tahun Lulus</th>
                                        <td>{!! $data->thn_lulus !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h4>Bank</h4>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Nama Bank</th>
                                        <td class="text-uppercase">{!! isset($data->bankAccount->name) ? $data->bankAccount->name : null !!}</td>
                                    </tr>
                                    <tr>
                                        <th>No. Rekening</th>
                                        <td class="text-uppercase">{!! $data->bank_account_no !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Cabang Bank Pembuka</th>
                                        <td class="text-uppercase">{!! $data->bank_branch !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pengalaman">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Pengalaman 1</h3>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Perusahaan</th>
                                        <td class="text-uppercase">{!! $data->exp_company_1 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Perusahaan</th>
                                        <td class="text-uppercase">{!! $data->exp_company_type_1 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jabatan</th>
                                        <td class="text-uppercase">{!! $data->exp_position_1 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Gaji Rata-rata / Bulan</th>
                                        <td class="text-uppercase">{!! $data->exp_salary_1 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Masa Kerja <br><small>(Bulan dan Tahun)</small></th>
                                        <td>
                                            {!! $data->exp_start_1 !!}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h3>Pengalaman 2</h3>
                                <table class="table table-hover">
                                    <tr>
                                        <th style="width: 40%">Perusahaan</th>
                                        <td class="text-uppercase">{!! $data->exp_company_2 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Perusahaan</th>
                                        <td class="text-uppercase">{!! $data->exp_company_type_2 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Jabatan</th>
                                        <td class="text-uppercase">{!! $data->exp_position_2 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Gaji Rata-rata / Bulan</th>
                                        <td class="text-uppercase">{!! $data->exp_salary_2 !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Masa Kerja <br><small>(Bulan dan Tahun)</small></th>
                                        <td>
                                            {!! $data->exp_start_2 !!}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="dokumen">
                        <div class="row">
                            <div class="col-md-6">
                                @if(!empty($data->file_ktp))
                                <img class="img-responsive" src="{!! Storage::disk('s3')->url('candidate/iei/'.$data->nik.'/'.$data->file_ktp) !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @else
                                <img class="img-responsive" src="{!! asset('images/no-image.png') !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @endif
                                KTP
                            </div>
                            <div class="col-md-6">
                                @if(!empty($data->file_kk))
                                <img class="img-responsive" src="{!! Storage::disk('s3')->url('candidate/iei/'.$data->nik.'/'.$data->file_kk) !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @else
                                <img class="img-responsive" src="{!! asset('images/no-image.png') !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @endif
                                KK
                            </div>
                            <div class="col-md-6">
                                @if(!empty($data->file_skck))
                                <img class="img-responsive" src="{!! Storage::disk('s3')->url('candidate/iei/'.$data->nik.'/'.$data->file_skck) !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @else
                                <img class="img-responsive" src="{!! asset('images/no-image.png') !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @endif
                                SKCK
                            </div>
                            <div class="col-md-6">
                                @if(!empty($data->file_tab))
                                <img class="img-responsive" src="{!! Storage::disk('s3')->url('candidate/iei/'.$data->nik.'/'.$data->file_tab) !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @else
                                <img class="img-responsive" src="{!! asset('images/no-image.png') !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @endif
                                Buku Tabungan
                            </div>
                            <div class="col-md-6">
                                @if(!empty($data->file_vaksin))
                                <img class="img-responsive" src="{!! Storage::disk('s3')->url('candidate/iei/'.$data->nik.'/'.$data->file_vaksin) !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @else
                                <img class="img-responsive" src="{!! asset('images/no-image.png') !!}" alt="Photo" style="max-height:200px;border:1px solid #ECF0F5;border-radius:10px" />
                                @endif
                                Sertifikat Vaksin
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade modal-danger" id="delete-{!!$data->id!!}" tabindex="-1" role="dialog" aria-labelledby="delete">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open([ 'route' => ['candidate.destroy', $data->pub_id], 'method' => 'delete' ]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                        Apakah anda yakin akan menghapus data member <b>{!! $data->fullname !!}</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline">Hapus</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection