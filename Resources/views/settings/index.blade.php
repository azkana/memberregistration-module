@extends('memberregistration::layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#setting_member" data-toggle="tab">Member</a></li>
                <li class=""><a href="#setting_tnc" data-toggle="tab">Term & Conditions</a></li>
                <li class=""><a href="#setting_iei" data-toggle="tab">Formulir IEI</a></li>
            </ul>
            <div class="tab-content" style="min-height: 520px">
                <div class="tab-pane active" id="setting_member">
                    @include('memberregistration::settings.member')
                </div>
                <div class="tab-pane" id="setting_tnc">
                    @include('memberregistration::settings.tnc')
                </div>
                <div class="tab-pane" id="setting_iei">
                    @include('memberregistration::settings.iei')
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $(".number").number(true, 0);
            $(".timepicker").timepicker({
                showInputs: false,
                showMeridian: false
            });
            $("#membership_tnc").wysihtml5();
            $("#membership_form_unpublished_note").wysihtml5({
                toolbar: {
                    "lists": false,
                    "image": false,
                    "blockquote": false,
                    "size": "xs",
                    "fa": true
                }
            });

            var formIsPublished = $('#form_published');
            var formAvailable   = $('#membership_form_available');
            var formAvailableTime = $('.form-available-time');
            var formAvailableTimeRange = $('.form-available-time-range');
            var formAvailableGroup = $('.form-available');
            var formAvailableWeekdaysGroup = $('.form-available-weekdays');
            var formAvailableWorkingdaysGroup = $('.form-available-workingdays');
            var formUnpublishedNote = $(".form-unpublished-note");
            var formMaxDay  = $('.form-max-day');
            
            formIsPublished.click(function(e) {
                if($(this).prop('checked') === true) {
                    formAvailableGroup.removeClass('hide');
                    formUnpublishedNote.addClass('hide');
                } else {
                    formAvailableGroup.addClass('hide');
                    formUnpublishedNote.removeClass('hide');
                }
            });

            formAvailable.on('change', function(e) {
                if($(this).val() === 'everyday') {
                    formAvailableWeekdaysGroup.removeClass('hide');
                    formAvailableWorkingdaysGroup.addClass('hide');
                } else if($(this).val() === 'workingday') {
                    formAvailableWeekdaysGroup.addClass('hide');
                    formAvailableWorkingdaysGroup.removeClass('hide');
                } else {
                    formAvailableWeekdaysGroup.addClass('hide');
                    formAvailableWorkingdaysGroup.addClass('hide');
                }
            });

            $("#membership_form_available_time").on('change', function(e) {
                if($(this).val() === 'timerange') {
                    formAvailableTimeRange.removeClass('hide');
                } else {
                    formAvailableTimeRange.addClass('hide');
                }
            });

            // Unique Number
            var isUnique = $('#form_unique_no');
            var isUniqueGroup = $('.is-unique');

            isUnique.on('click', function(e) {
                if($(this).prop('checked') === true) {
                    isUniqueGroup.removeClass('hide');
                } else {
                    isUniqueGroup.addClass('hide');
                }
            });

        });
    </script>
@endsection