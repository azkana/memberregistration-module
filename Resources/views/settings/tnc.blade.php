<div class="row">
    <div class="col-md-12">
        {!! Form::open([ 'route' => 'membership.setting.store', 'class' => 'form-horizontal', 'autocomplete' => 'off' ]) !!}
        {!! Form::hidden('tab', 'tnc') !!}
        <div class="box box-solid">
            <div class="box-body" style="min-height: 460px">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="membership_tnc" class="col-sm-2 control-label">Term & Conditions</label>
                            <div class="col-sm-8">
                                {!! Form::textarea('membership_tnc', getOptions('membership_tnc'), [ 'class' => 'form-control', 'id' => 'membership_tnc', 'maxlength' => 1000, 'style' => 'height:350px']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>