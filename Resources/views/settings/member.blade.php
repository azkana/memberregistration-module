<div class="row">
    <div class="col-md-12">
        {!! Form::open([ 'route' => 'membership.setting.store', 'class' => 'form-horizontal', 'autocomplete' => 'off' ]) !!}
        {!! Form::hidden('tab', 'member') !!}
        <div class="box box-solid">
            <div class="box-body" style="min-height: 460px">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="membership_subscription" class="col-sm-5 control-label">Biaya Member</label>
                            <div class="col-sm-7">
                                {!! Form::text('membership_subscription', getOptions('membership_subscription'), [ 'class' => 'form-control number', 'id' => 'membership_subscription']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="membership_unique" class="col-sm-5 control-label">Use Unique Number</label>
                            <div class="col-sm-7">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('membership_unique', true, getOptions('membership_unique') == 1 ? true : false, ['id' => 'form_unique_no']) !!} Yes
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="is-unique {!! getOptions('membership_unique') == 1 ? '' : 'hide' !!}">
                            <div class="form-group">
                                <label for="membership_unique_type" class="col-sm-5 control-label">Unique Number Type</label>
                                <div class="col-sm-7">
                                    {!! Form::select('membership_unique_type', $uniqueNumberType, getOptions('membership_unique_type'), ['class' => 'form-control', 'id' => 'membership_unique_type'] ) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="membership_unique_digit" class="col-sm-5 control-label">Unique Digit</label>
                                <div class="col-sm-3">
                                    {!! Form::number('membership_unique_digit', getOptions('membership_unique_digit'), [ 'class' => 'form-control number', 'id' => 'form_unique_digit']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="membership_payment_due" class="col-sm-5 control-label">Payment Due</label>
                                <div class="col-sm-3">
                                    {!! Form::number('membership_payment_due', getOptions('membership_payment_due'), [ 'class' => 'form-control number', 'id' => 'form_payment_due']) !!}
                                </div>
                                <label class="col-sm-1 control-label small">day(s)</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="membership_form_published" class="col-sm-5 control-label">Form Registration</label>
                            <div class="col-sm-7">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('membership_form_published', true, getOptions('membership_form_published') == 1 ? true : false, ['id' => 'form_published']) !!} Published
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group {!! getOptions('membership_form_published') == 0 ? '' : 'hide' !!} form-unpublished-note">
                            <label for="membership_form_unpublished_note" class="col-sm-5 control-label">Note</label>
                            <div class="col-sm-7">
                                {!! Form::textarea('membership_form_unpublished_note', getOptions('membership_form_unpublished_note'), [ 'class' => 'form-control small', 'id' => 'membership_form_unpublished_note', 'maxlength' => 1000, 'style' => 'height:120px']) !!}
                            </div>
                        </div>
                        <div class="form-available {!! getOptions('membership_form_published') == 1 ? '' : 'hide' !!}">
                            <div class="form-group">
                                <label for="membership_form_available" class="col-sm-5 control-label">Form Available</label>
                                <div class="col-sm-7">
                                    {!! Form::select('membership_form_available', $formAvailable, getOptions('membership_form_available'), ['class' => 'form-control', 'id' => 'membership_form_available'] ) !!}
                                </div>
                            </div>
                            <div class="form-group form-available-weekdays {!! getOptions('membership_form_available') == 'everyday' ? '' : 'hide' !!}">
                                <label for="" class="col-sm-5 control-label"></label>
                                <div class="col-sm-7">
                                    @foreach ($weekdays as $item)
                                    <span class="label label-default">{!! $item !!}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group form-available-workingdays {!! getOptions('membership_form_available') == 'workingday' ? '' : 'hide' !!}">
                                <label for="" class="col-sm-5 control-label"></label>
                                <div class="col-sm-7">
                                    @foreach ($workingDays as $item)
                                    <span class="label label-default">{!! $item !!}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group form-available-time">
                                <label for="membership_form_available_time" class="col-sm-5 control-label">Form Available Time</label>
                                <div class="col-sm-7">
                                    {!! Form::select('membership_form_available_time', $formAvailableTime, getOptions('membership_form_available_time'), ['class' => 'form-control', 'id' => 'membership_form_available_time'] ) !!}
                                </div>
                            </div>
                            <div class="form-group form-available-time-range {!! getOptions('membership_form_available_time') == 'timerange' ? '' : 'hide' !!}">
                                <label for="membership_form_available_time_start" class="col-sm-5 control-label"></label>
                                <div class="col-sm-3">
                                    <div class="bootstrap-timepicker">
                                        <div class="input-group">
                                            {!! Form::text('membership_form_available_time_start', getOptions('membership_form_available_time_start'), [ 'class' => 'form-control input-sm timepicker', 'id' => 'membership_form_available_time_start']) !!}
                                            <div class="input-group-addon input-sm">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <label for="membership_form_available_time_end" class="col-sm-1 control-label">to</label>
                                <div class="col-sm-3">
                                    <div class="bootstrap-timepicker">
                                        <div class="input-group">
                                            {!! Form::text('membership_form_available_time_end', getOptions('membership_form_available_time_end'), [ 'class' => 'form-control input-sm timepicker', 'id' => 'membership_form_available_time_end']) !!}
                                            <div class="input-group-addon input-sm">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-max-day">
                                <label for="membership_form_max_day" class="col-sm-5 control-label">Max Registrant</label>
                                <div class="col-sm-3">
                                    {!! Form::number('membership_form_max_day', getOptions('membership_form_max_day'), ['class' => 'form-control number', 'id' => 'membership_form_max_day'] ) !!}
                                </div>
                                <label class="col-sm-1 control-label small">/day</label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Member Data</h4>
                        <div class="form-group">
                            <label for="membership_need_approval_subday" class="col-sm-5 control-label">Need Approval</label>
                            <div class="col-sm-3">
                                {!! Form::number('membership_need_approval_subday', getOptions('membership_need_approval_subday'), [ 'class' => 'form-control number']) !!}
                            </div>
                            <label class="col-sm-1 control-label small">day(s)</label>
                        </div>
                        <div class="form-group">
                            <label for="membership_active_member_subday" class="col-sm-5 control-label">Active Member</label>
                            <div class="col-sm-3">
                                {!! Form::number('membership_active_member_subday', getOptions('membership_active_member_subday'), [ 'class' => 'form-control number']) !!}
                            </div>
                            <label class="col-sm-1 control-label small">day(s)</label>
                        </div>
                        <div class="form-group">
                            <label for="membership_waiting_payment_subday" class="col-sm-5 control-label">Waiting Payment</label>
                            <div class="col-sm-3">
                                {!! Form::number('membership_waiting_payment_subday', getOptions('membership_waiting_payment_subday'), [ 'class' => 'form-control number']) !!}
                            </div>
                            <label class="col-sm-1 control-label small">day(s)</label>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>