<div class="row">
    <div class="col-md-12">
        {!! Form::open([ 'route' => 'membership.setting.store', 'class' => 'form-horizontal', 'autocomplete' => 'off' ]) !!}
        {!! Form::hidden('tab', 'iei') !!}
        <div class="box box-solid">
            <div class="box-body" style="min-height: 460px">
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Exports</h4>
                        <div class="form-group">
                            <label for="iei_export_template" class="col-sm-4 control-label">Template</label>
                            <div class="col-sm-5">
                                {!! Form::select('iei_export_template', $exportTemplate, getOptions('iei_export_template'), [ 'class' => 'form-control', 'id' => 'iei_export_template', 'placeholder' => 'Pilih']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>