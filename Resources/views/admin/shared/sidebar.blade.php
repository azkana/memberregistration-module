
<li class="treeview @if($adminActiveMenu == 'membership') active @endif">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Membership</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('member-list')
        <li class="@if($adminActiveSubMenu == 'members') active @endif">
            <a href="#"><i class="fa fa-circle-o"></i> Members
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="@if($adminActiveSubMenu == 'members' && empty($urlSegment4) && !isset($member_state)) active @endif">
                    <a href="{!! route('member.index') !!}">
                        <i class="fa fa-circle-o"></i> All Members
                        {{-- <span class="pull-right-container">
                            <span class="label label-danger pull-right">
                                {!! getMemberCount() !!}
                            </span>
                        </span> --}}
                    </a>
                </li>
                <li class="@if($urlSegment4 == 'active-member') active @endif">
                    <a href="{!! route('member.active-member') !!}">
                        <i class="fa fa-circle-o"></i> Active Member
                        {{-- <span class="pull-right-container">
                            <span class="label label-danger pull-right">
                                {!! getMemberCount('active-member') !!}
                            </span>
                        </span> --}}
                    </a>
                </li>
                <li class="@if($urlSegment4 == 'need-approval') active @endif">
                    <a href="{!! route('member.need-approval') !!}">
                        <i class="fa fa-circle-o"></i> Need Approval
                        {{-- <span class="pull-right-container">
                            <span class="label label-danger pull-right">
                                {!! getMemberCount('need-approval') !!}
                            </span>
                        </span> --}}
                    </a>
                </li>
                @can('member-waiting-payment')
                <li class="@if($urlSegment4 == 'waiting-payment') active @endif">
                    <a href="{!! route('member.waiting-payment') !!}">
                        <i class="fa fa-circle-o"></i> Waiting Payment
                        {{-- <span class="pull-right-container">
                            <span class="label label-danger pull-right">
                                {!! getMemberCount('waiting-payment') !!}
                            </span>
                        </span> --}}
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan
        @can('member-test')
        <li class="@if($adminActiveSubMenu == 'tests') active @endif">
            <a href="#"><i class="fa fa-circle-o"></i> Tests
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                @can('member-test-info')
                <li class="@if($urlSegment4 == 'info') active @endif">
                    <a href="{!! route('member.test.index') !!}">
                        <i class="fa fa-circle-o"></i> Info
                    </a>
                </li>
                @endcan
                @can('member-test-result')
                <li class="@if($urlSegment4 == 'result') active @endif">
                    <a href="{!! route('member.test.result.index') !!}">
                        <i class="fa fa-circle-o"></i> Result
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan
        @can('candidate-list')
        <li class="@if($adminActiveSubMenu == 'candidate') active @endif">
            <a href="{!! route('candidate.index') !!}">
                <i class="fa fa-circle-o"></i> Candidate
                {{-- <span class="pull-right-container">
                    <span class="label label-danger pull-right">
                        {!! getCandidateCount() !!}
                    </span>
                </span> --}}
            </a>
        </li>
        @endcan
        @can('member-reports')
        <li class="@if($adminActiveSubMenu == 'reports') active @endif"><a href="{!! route('report.index') !!}"><i class="fa fa-circle-o"></i> Report</a></li>
        @endcan
        @can('member-settings')
        <li class="@if($adminActiveSubMenu == 'settings') active @endif"><a href="{!! route('membership.setting.index') !!}"><i class="fa fa-circle-o"></i> Settings</a></li>
        @endcan
    </ul>
</li>
