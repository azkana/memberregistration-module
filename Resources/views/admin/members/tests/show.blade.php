@extends('memberregistration::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail</h3>
                    <div class="box-tools pull-right">
                        <a class="btn btn-sm btn-danger" href="{!! route('member.test.index') !!}">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <table class="table table-bordered table-hover small">
                                <tr>
                                    <th>Title</th>
                                    <td>{!! $data->name !!}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{!! $data->desc !!}</td>
                                </tr>
                                <tr>
                                    <th width="30%">Test Date</th>
                                    <td>{!! !is_null($data->test_date) ? date('d-m-Y H:i:s', strtotime($data->test_date)) : null !!}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-bordered table-hover small">
                                <tr width="30%">
                                    <th>Start Date</th>
                                    <td>{!! date('d-m-Y H:i:s', strtotime($data->start_date)) !!}</td>
                                </tr>
                                <tr>
                                    <th>End Date</th>
                                    <td>{!! date('d-m-Y H:i:s', strtotime($data->end_date)) !!}</td>
                                </tr>
                                <tr>
                                    <th>Active</th>
                                    <td>{!! $data->is_active == true ? 'Yes' : 'No' !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="grid-test" class="table table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr>
                                        {{-- <th></th> --}}
                                        <th class="text-center" style="width: 2%">#</th>
                                        <th class="text-center" style="width: 10%">No. Tes</th>
                                        <th class="text-center">No. Member</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama</th>
                                        {{-- <th class="text-center" style="width: 5%">
                                            <i class="fa fa-navicon"></i>
                                        </th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="modal-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="new-data">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div id="content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var batch = "{!! $data->batch !!}";
            var table = $("#grid-test").dataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                scrollX: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        text: '<i class="fa fa-cloud-upload"></i> Import',
                        className: "btn-sm",
                        attr: {
                            id: "btn-import"
                        }
                    },
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: {
                    url: "{!! route('member.test.data') !!}",
                    pages: 5,
                    data: {batch: batch}
                },
                columns: [
                    // {data: 'checkboxes', name: 'checkboxes', orderable: false, searchable: false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'test', name: 'test', className: "small text-center"},
                    {data: 'member', name: 'member', className: "small text-center"},
                    {data: 'nik', name: 'nik', className: "small text-center"},
                    {data: 'name', name: 'name', className: "small"},
                    // {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {
                        targets: 0,
                        className: 'select-checkbox'
                    },
                ],
                order: [
                    [2, 'asc'],
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>",
                    emptyTable: "Data tidak ditemukan"
                }
            });

            $('.dataTables_scrollBody').css('height', '300px');

            function getTime(d) {
                return new Date(d.split("-").reverse().join("-")).getTime()
            }

            $('#btn-import').on('click', function() {
                $.ajax({
                    type: "GET",
                    url: "{!! route('member.test.import') !!}",
                    data: {batch: batch},
                    success: function(data) {
                        $('#modal-box').modal('toggle');
                        $('#content').html(data);
                    }
                });
            });
        });
    </script>
@endsection
