<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="new-daily-form">Confirmation</h4>
</div>
<form id="form_test_batch_delete" class="form-horizontal" autocomplete="off">
@csrf
@method('delete')
{!! Form::hidden('id', $data->id) !!}
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            Apakah anda yakin akan menghapus <b>{!! $data->name !!}</b> ?
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-danger" id="submit"><i class="fa fa-trash"></i> Delete</button>
</div>
</form>

<script>
    $(document).ready(function() {
        $("#submit").on('click', function(e) {
            $("#form_test_batch_delete").validate({
                errorClass: 'errors small',
                submitHandler: function(form) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('member.test.destroy') }}",
                        data: $(form).serialize(),
                        success: function(data) {
                            $("#modal-box").modal('hide');
                            toastr.success(data.message);
                            $('#grid-member-test-batch').DataTable().ajax.reload(null, false);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });
</script>
