@extends('memberregistration::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title"></h3>
                        </div>
                        <div class="col-md-6">
                            <div class="box-tools pull-right" id="btn-table">
                                {{-- @can('member-test-info-delete')
                                <button class="btn btn-sm btn-danger hide" id="bulk_delete">
                                    <i class="fa fa-trash"></i>
                                    Delete
                                </button>
                                @endcan --}}
                                @can('member-test-info-create')
                                <a class="btn btn-sm btn-primary" id="new-data">
                                    <i class="fa fa-plus"></i> Baru
                                </a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-6"></div>
                        {{-- <div class="col-md-6">
                            <div class="row">
                                {!! Form::hidden('member_state', $member_state, ['id' => 'member_state']) !!}
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="start_date" class="small">Start Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('start_date', $startDate, [ 'class' => 'form-control datepicker input-sm', 'id' => 'start_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="end_date" class="small">End Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('end_date', $endDate, [ 'class' => 'form-control datepicker input-sm', 'id' => 'end_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="button" class="small"></label>
                                        <div class="input-group">
                                            <button class="btn btn-sm btn-success" style="margin-top: 4.5px" id="btn_search">
                                                <i class="fa fa-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="grid-member-test-batch" class="table table-bordered table-striped" style="width:100%;">
                                    <thead>
                                        <tr>
                                            {{-- <th></th> --}}
                                            <th class="text-center" style="width: 2%">#</th>
                                            <th class="text-center" style="width: 10%">No. Batch</th>
                                            <th class="text-center">Title</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center" style="width: 12%">Test Date</th>
                                            <th class="text-center" style="width: 12%">Start Date</th>
                                            <th class="text-center" style="width: 12%">End Date</th>
                                            <th class="text-center" style="width: 10%">Status</th>
                                            <th class="text-center" style="width: 5%">
                                                <i class="fa fa-navicon"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="modal fade modal-default" id="export" tabindex="-1" role="dialog" aria-labelledby="export">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open([ 'route' => ['member.exports'], 'method' => 'get', 'class' => 'form-horizontal' ]) !!}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">
                                    Export Data
                                    @if($member_state == 'active-member')
                                        <b>Active Member</b>
                                    @else
                                        <b>Member</b>
                                    @endif
                                </h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        {!! Form::hidden('export_state', $member_state, ['id' => 'export_state']) !!}
                                        <div class="form-group">
                                            <label for="tgl_from" class="col-sm-3 control-label">Tanggal <span class="required">*</span></label>
                                            <div class="col-sm-3">
                                                {!! Form::text('tgl_from', date('d-m-Y'), [ 'class' => 'form-control datepicker', 'id' => 'tgl_from', 'required']) !!}
                                            </div>
                                            <label for="tgl_to" class="col-sm-2 control-label">Sampai <span class="required">*</span></label>
                                            <div class="col-sm-3">
                                                {!! Form::text('tgl_to', date('d-m-Y'), [ 'class' => 'form-control datepicker', 'id' => 'tgl_to', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender" class="col-sm-3 control-label">Jenis Kelamin</label>
                                            <div class="col-sm-3">
                                                {!! Form::select('gender', $gender, null, [ 'class' => 'form-control text-uppercase small', 'placeholder' => 'Semua']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="file_type" class="col-sm-3 control-label">Jenis File <span class="required">*</span></label>
                                            <div class="col-sm-6">
                                                <div class="checkbox">
                                                    <label>
                                                        {!! Form::checkbox('file_type', 'xlsx', true, ['required']) !!} <i class="fa fa-file-excel-o"></i> Ms. Excel
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-cloud-download"></i>
                                    Export
                                </button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div> --}}

                <div id="modal-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="new-data">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div id="content"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .datepicker table tr td.disabled{
            color:red;
            cursor: not-allowed;
        }
        .datepicker table tr td.today{
            background: orange;
        }
        .datepicker table tr td.active{
            font-weight: bold;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var btnBulkDelete = $('#bulk_delete');
            var btnSearch = $('#btn_search');

            $('#new-data').on('click', function() {
                $.ajax({
                    type: "GET",
                    url: "{!! route('member.test.create') !!}",
                    success: function(data) {
                        $('#modal-box').modal('toggle');
                        $('#content').html(data);
                    }
                });
            });

            var table = $('#grid-member-test-batch').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                scrollX: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                buttons: [
                    // {
                    //     extend: 'selectAll',
                    //     text: 'Select All',
                    //     className: "btn-sm",
                    // },
                    // {
                    //     extend:'selectNone',
                    //     text: 'Select None',
                    //     className: "btn-sm",
                    // },
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: {
                    url: "{!! route('member.test.index.data') !!}",
                    pages: 10,
                    // data: function(d) {
                    //     d.start_date = startDate.val();
                    //     d.end_date = endDate.val();
                    //     d.member_state = member_state.val();
                    // }
                },
                columns: [
                    // {data: 'checkboxes', name: 'checkboxes', orderable: false, searchable: false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'batch', name: 'batch', className: "small"},
                    {data: 'name', name: 'name', className: "small"},
                    {data: 'desc', name: 'desc', className: "small"},
                    {data: 'test_date', name: 'test_date', className: "small"},
                    {data: 'start_date', name: 'start_date', className: "small"},
                    {data: 'end_date', name: 'end_date', className: "small text-right"},
                    {data: 'is_active', name: 'is_active', className: "small"},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    // {
                    //     targets: 0,
                    //     className: 'select-checkbox'
                    // },
                    {targets: 4, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )},
                    {targets: 5, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )},
                    {targets: 6, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )},
                ],
                order: [
                    [4, 'asc'],
                    [5, 'asc'],
                    [6, 'desc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>",
                    emptyTable: "Data tidak ditemukan"
                },
            });

            table.on( 'select', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count > 0 ) {
                    btnBulkDelete.removeClass('hide');
                }
            } );

            table.on( 'deselect', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count === 0 ) {
                    btnBulkDelete.addClass('hide');
                }
            } );

            btnBulkDelete.on('click', function(e) {
                e.preventDefault();
                var countRows = table.rows( { selected: true } ).data().count();
                swal({
                    title: 'Confirmation',
                    text: 'Are you sure you want to delete this ' + countRows + ' row(s)?',
                    buttons: ["Cancel", "Yes"],
                    dangerMode: true,
                    closeOnClickOutside: false,
                }).then(function(value) {
                    if(value) {
                        var selectedRows = table.rows( { selected: true } ).data().to$();
                        var selectedId = Array();
                        $.each(selectedRows, function(i, e) {
                            selectedId.push(e.id);
                        });

                        $.ajax({
                            url: "{!! route('member.destroy.bulk') !!}",
                            type: "post",
                            data: {
                                _method: "delete",
                                _token: "{!! csrf_token() !!}",
                                id: selectedId,
                            },
                            success: function(res, stat, xhr) {
                                swal({
                                    text: res.message,
                                    icon: 'success',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                                table.draw();
                                btnBulkDelete.addClass('hide');
                            },
                            error: function(err) {
                                swal({
                                    text: err,
                                    icon: 'error',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                            }
                        });
                    }
                });

            });

            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                endDate: '+1d',
                language: 'id'
            });

            btnSearch.on('click', function(e) {
                e.preventDefault();
                getData();
            });

            $('.dataTables_scrollBody').css('height', '350px');

            function getData() {
                $.ajax({
                    url: "{!! route('member.test.index.data') !!}",
                    type: "GET",
                    // data: {
                    //     start_date: startDate.val(),
                    //     end_date: endDate.val(),
                    //     member_state : member_state.val()
                    // },
                    success: function(data) {
                        table.ajax.reload();
                    }
                });
            }

            function getTime(d) {
                return new Date(d.split("-").reverse().join("-")).getTime()
            }

        });

        function deleteThis(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.test.delete') !!}",
                data: { id: id},
                success: function(data) {
                    $('#modal-box').modal('toggle');
                    $('#content').html(data);
                }
            });
        }

        function editThis(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('member.test.edit') !!}",
                data: { id: id},
                success: function(data) {
                    $('#modal-box').modal('toggle');
                    $('#content').html(data);
                }
            });
        }

    </script>
@endsection
