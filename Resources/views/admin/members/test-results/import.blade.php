<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="new-daily-form">New Test Batch</h4>
</div>
<form id="form_import_test_result" class="form-horizontal" autocomplete="off">
@csrf
{!! Form::hidden('batch', $batch) !!}
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label for="excel" class="col-sm-2 control-label">File <span class="required">*</span></label>
                <div class="col-sm-9">
                    {!! Form::file('excel', [ 'class' => 'form-control input-sm', 'id' => 'excel', 'accept' => '.xlsx,.xls,.csv', 'required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" id="submit"><i class="fa fa-cloud-upload"></i> Import</button>
</div>
</form>

<style>
    .errors {
        color:#FF0000;
        font-size: 10px;
        font-weight: 400;
    }
</style>

<script>
    $(document).ready(function() {
        $("#submit").on('click', function(e) {
            $("#form_import_test_result").validate({
                errorClass: 'errors small',
                rules: {
                    excel: {
                        required: true,
                        accept: "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,csv",
                    },
                },
                messages: {
                    excel: {
                        required: "harus diisi",
                        accept: "file tidak valid",
                    },
                },
                submitHandler: function(form) {
                    var formData = new FormData($(form)[0]);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('member.test.result.import.save') }}",
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        processData: false,
                        cache: false,
                        success: function(data) {
                            $("#modal-box").modal('hide');
                            toastr.success(data.message);
                            $('#grid-test-result').DataTable().ajax.reload(null, false);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });
</script>
