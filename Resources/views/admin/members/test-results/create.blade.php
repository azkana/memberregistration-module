<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="new-daily-form">New Test Result Batch</h4>
</div>
<form id="form_new_test_result_batch" class="form-horizontal" autocomplete="off">
@csrf
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label for="batch" class="col-sm-2 control-label">Batch No.</label>
                <div class="col-sm-4">
                    {!! Form::text('batch', $batch, [ 'class' => 'form-control input-sm', 'id' => 'batch', 'readonly']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name <span class="required">*</span></label>
                <div class="col-sm-9">
                    {!! Form::text('name', null, [ 'class' => 'form-control input-sm', 'id' => 'name', 'maxlength' => 100, 'required']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="desc" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-9">
                    {!! Form::textarea('desc', null, [ 'class' => 'form-control input-sm', 'id' => 'desc', 'maxlength' => 200, 'rows' => 3]) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="start_date" class="col-sm-2 control-label">Start Date <span class="required">*</span></label>
                <div class="col-sm-4">
                    {!! Form::text('start_date', null, [ 'class' => 'form-control input-sm datepicker', 'id' => 'start_date', 'required']) !!}
                </div>
                <label for="start_time" class="col-sm-1 control-label">Jam <span class="required">*</span></label>
                <div class="col-sm-4 bootstrap-timepicker">
                    {!! Form::text('start_time', null, [ 'class' => 'form-control input-sm timepicker', 'id' => 'start_time', 'required']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="end_date" class="col-sm-2 control-label">End Date <span class="required">*</span></label>
                <div class="col-sm-4">
                    {!! Form::text('end_date', null, [ 'class' => 'form-control input-sm datepicker', 'id' => 'end_date', 'required']) !!}
                </div>
                <label for="end_time" class="col-sm-1 control-label">Jam <span class="required">*</span></label>
                <div class="col-sm-4 bootstrap-timepicker">
                    {!! Form::text('end_time', null, [ 'class' => 'form-control input-sm timepicker', 'id' => 'end_time', 'required']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="is_active" class="col-sm-2 control-label">Active? </label>
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('is_active', true, true) !!} Yes
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" id="submit"><i class="fa fa-save"></i> Save</button>
</div>
</form>

<style>
    .errors {
        color:#FF0000;
        font-size: 10px;
        font-weight: 400;
    }
</style>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $(".timepicker").timepicker({
            showInputs: false
        });
        $("#submit").on('click', function(e) {
            $("#form_new_test_result_batch").validate({
                errorClass: 'errors small',
                rules: {
                    name: {
                        required: true,
                    },
                    test_date: {
                        required: true,
                    },
                    start_date: {
                        required: true,
                    },
                    end_date: {
                        required: true,
                    },
                    start_time: {
                        required: true,
                    },
                    end_time: {
                        required: true,
                    },
                },
                messages: {
                    name: {
                        required: "harus diisi",
                    },
                    test_date: {
                        required: "harus diisi",
                    },
                    start_date: {
                        required: "harus diisi",
                    },
                    end_date: {
                        required: "harus diisi",
                    },
                    start_time: {
                        required: "harus diisi",
                    },
                    end_time: {
                        required: "harus diisi",
                    },
                },
                submitHandler: function(form) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('member.test.result.store') }}",
                        data: $(form).serialize(),
                        success: function(data) {
                            $("#modal-box").modal('hide');
                            toastr.success(data.message);
                            $('#grid-member-test-result-batch').DataTable().ajax.reload(null, false);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });
</script>
