<!DOCTYPE html>
<html lang="{!! str_replace('_', '-', app()->getLocale()) !!}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
        <title>{!! $title !!} :: jobsdku.co.id </title>
        <link rel="shortcut icon" href="{!! config('filesystems.disks.s3.url') . '/core/settings/' . getOptions('favicon') !!}" />

        <link href="{{ asset('css/metronic.css') }}" rel="stylesheet">
        {{-- Global site tag (gtag.js) - Google Analytics --}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163105943-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-163105943-1');
        </script>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
        <div class="page-wrapper">
            <div class="page-container" style="margin-top: 0;">
                <div class="page-content-wrapper">
                    <div class="page-content" style="min-height: 730px">
                        <div class="text-center">
                            <img src="{!! Storage::disk('s3')->url('core/settings/'. getOptions('favicon')) !!}" style="max-width: 100px"/>
                        </div>
                        <h1 class="page-title text-center" style="margin-top: 10px"> {!! $title !!}</h1>

                        {{-- Alert --}}
                        @if (session('error_message'))
                        <div class="alert alert-dismissable bg-red font-white">
                            <button aria-hidden="true" data-dismiss="alert" class="close font-white" type="button">×</button>
                            <strong>Error!</strong> <br>{!! session('error_message') !!}
                        </div>
                        @endif

                        @yield('content')
                        
                    </div>
                </div>
            </div>
            <div class="page-footer">
                <div class="page-footer-inner">
                    {!! date('Y') !!} &copy; 
                    <a href="http://www.jobsdku.co.id" target="_blank" class="font-white"> PT Dhasnarindo Karya Utama </a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
        </div>
        <script src="{!! asset('js/metronic.js') !!}"></script>
        @yield('js')
        <script>
            $(document).ready(function()
            {
                $(".alert").delay(20000).slideUp("slow");
            });
        </script>
    </body>
</html>