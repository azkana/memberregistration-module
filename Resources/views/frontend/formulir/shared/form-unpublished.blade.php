@extends('memberregistration::frontend.formulir.shared.app')

@section('content')
    
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <div class="row" style="margin-top: 100px; margin-bottom: 100px">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="note note-warning">
                                <h4 class="block bold uppercase">Pengumuman</h4>
                                <p> {!! $messages !!} </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection