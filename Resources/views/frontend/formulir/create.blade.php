@extends('memberregistration::frontend.formulir.shared.app')

@section('content')
                        
    {!! Form::open([ 'route' => 'candidate.save', 'class' => 'form-horizontal', 'id' => 'frm_karyawan', 'files' => true, 'autocomplete' => 'off' ])!!}
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item bg-yellow-gold bg-font-yellow-gold"> <strong>Note:</strong> Mohon untuk menggunakan browser Google Chrome terbaru. </li>
            </ul>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <i class="fa fa-user font-green-haze"></i>
                        <span class="caption-subject bold uppercase"> Data Personal</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                        {{-- Data Personal --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="fullname">Nama Lengkap <br><small>(Sesuai KTP)</small> <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('fullname', null, [ 'class' => 'form-control text-uppercase', 'id' => 'fullname', 'required', 'autofocus']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="firstname">Nama Depan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('firstname', null, [ 'class' => 'form-control text-uppercase', 'id' => 'firstname', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="midname">Nama Tengah</label>
                                    <div class="col-md-8">
                                        {!! Form::text('midname', null, [ 'class' => 'form-control text-uppercase','id' => 'midname']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="lastname">Nama Belakang</label>
                                    <div class="col-md-8">
                                        {!! Form::text('lastname', null, [ 'class' => 'form-control text-uppercase', 'id' => 'lastname']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-radios">
                                    <label class="col-md-4 control-label" for="gender">Jenis Kelamin <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        <div class="md-radio-inline">
                                            <div class="md-radio">
                                                <input type="radio" id="radio53" name="gender" class="md-radiobtn" value="L" required>
                                                <label for="radio53">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Laki-laki
                                                </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="radio54" name="gender" class="md-radiobtn" value="P" required>
                                                <label for="radio54">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Perempuan
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="tmp_lahir">Tempat Lahir <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('tmp_lahir', null, [ 'class' => 'form-control text-uppercase', 'id' => 'tmp_lahir', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="tgl_lahir">Tanggal Lahir <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('tgl_lahir', null, [ 'class' => 'form-control', 'id' => 'tgl_lahir', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">31/04/2000</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="no_hp">No. HP <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('no_hp', null, [ 'class' => 'form-control', 'id' => 'no_hp', 'maxlength' => 30, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">085123456789</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="mother">Nama Ibu Kandung <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('mother', null, [ 'class' => 'form-control text-uppercase', 'id' => 'mother', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="ktp_type">Masa Berlaku KTP <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('ktp_type', $ktp_type, null, [ 'class' => 'form-control text-uppercase', 'id' => 'ktp_type', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        {{-- Kontak Darurat --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-map-marker font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Kontak Darurat</span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_name">Nama Lengkap <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('emr_name', null, [ 'class' => 'form-control text-uppercase', 'id' => 'emr_name', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_hubkel">Hubungan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('emr_hubkel', $hubKel, null, [ 'class' => 'form-control text-uppercase', 'id' => 'emr_hubkel', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_phone">No. Telpon <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('emr_phone', null, [ 'class' => 'form-control', 'id' => 'emr_phone', 'maxlength' => 30, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">085123456789</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_alamat">Alamat <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::textarea('emr_alamat', null, [ 'class' => 'form-control text-uppercase', 'id' => 'emr_alamat', 'rows' => 1, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Diisi alamat sesuai yang dipilih tanpa RT/RW</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_alamat_rt">RT/RW <span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><small>RT</small></span>
                                            {!! Form::text('emr_alamat_rt', null, [ 'class' => 'form-control', 'id' => 'emr_alamat_rt', 'maxlength' => 3, 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">001</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><small>RW</small></span>
                                            {!! Form::text('emr_alamat_rw', null, [ 'class' => 'form-control', 'id' => 'emr_alamat_rw', 'maxlength' => 3, 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">007</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_provinsi">Provinsi <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('emr_provinsi', $provinsi, null, [ 'class' => 'form-control text-uppercase', 'id' => 'emr_provinsi', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_kabupaten">Kota/Kabupaten <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('emr_kabupaten', [], null, [ 'class' => 'form-control text-uppercase', 'id' => 'emr_kabupaten', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_kecamatan">Kecamatan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('emr_kecamatan', [], null, [ 'class' => 'form-control text-uppercase', 'id' => 'emr_kecamatan', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_kelurahan">Kelurahan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('emr_kelurahan', null, [ 'class' => 'form-control text-uppercase', 'id' => 'emr_kelurahan', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="emr_kodepos">Kodepos <span class="required">*</span></label>
                                    <div class="col-md-4">
                                        {!! Form::text('emr_kodepos', null, [ 'class' => 'form-control', 'id' => 'emr_kodepos', 'maxlength' => 5, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        {{-- Alamat --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-map-marker font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Alamat (Sesuai KTP)</span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="alamat">Alamat <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::textarea('alamat', null, [ 'class' => 'form-control text-uppercase', 'id' => 'alamat', 'rows' => 2, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Diisi sesuai yang ada di KTP anda, tanpa RT/RW</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="alamat_rt">RT/RW <span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><small>RT</small></span>
                                            {!! Form::text('alamat_rt', null, [ 'class' => 'form-control', 'id' => 'alamat_rt', 'maxlength' => 3, 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">001</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><small>RW</small></span>
                                            {!! Form::text('alamat_rw', null, [ 'class' => 'form-control', 'id' => 'alamat_rw', 'maxlength' => 3, 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">007</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="provinsi">Provinsi <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('provinsi', $provinsi, null, [ 'class' => 'form-control text-uppercase', 'id' => 'provinsi', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kabupaten">Kota/Kabupaten <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('kabupaten', [], null, [ 'class' => 'form-control text-uppercase', 'id' => 'kabupaten', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kecamatan">Kecamatan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('kecamatan', [], null, [ 'class' => 'form-control text-uppercase', 'id' => 'kecamatan', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kelurahan">Kelurahan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('kelurahan', null, [ 'class' => 'form-control text-uppercase', 'id' => 'kelurahan', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kodepos">Kodepos <span class="required">*</span></label>
                                    <div class="col-md-4">
                                        {!! Form::text('kodepos', null, [ 'class' => 'form-control', 'id' => 'kodepos', 'maxlength' => 5]) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="phone">No. Telpon <span class="required">*</span></label>
                                    <div class="col-md-6">
                                        {!! Form::text('phone', null, [ 'class' => 'form-control', 'id' => 'phone', 'maxlength' => 30]) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Nomor telepon yang bisa di hubungi di alamat KTP</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-map-marker font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Alamat (Domisili)</span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-12 control-label" style="text-align: left">
                                        <small>Domisili selama bekerja di PT. IEI, Pelamar harus bersedia bertempat tinggal dalam wilayah kota/kab.Bekasi</small>
                                    </label>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="alamat_dom">Alamat <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::textarea('alamat_dom', null, [ 'class' => 'form-control text-uppercase', 'id' => 'alamat_dom', 'rows' => 2, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Diisi sesuai dengan alamat tinggal anda sekarang, tanpa RT/RW</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="alamat_rt_dom">RT/RW <span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><small>RT</small></span>
                                            {!! Form::text('alamat_rt_dom', null, [ 'class' => 'form-control', 'id' => 'alamat_rt_dom', 'maxlength' => 3, 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">001</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><small>RW</small></span>
                                            {!! Form::text('alamat_rw_dom', null, [ 'class' => 'form-control', 'id' => 'alamat_rw_dom', 'maxlength' => 3, 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">007</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="provinsi_dom">Provinsi <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('provinsi_dom', $provinsi, null, [ 'class' => 'form-control text-uppercase', 'id' => 'provinsi_dom', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kabupaten_dom">Kota/Kabupaten <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('kabupaten_dom', [], null, [ 'class' => 'form-control text-uppercase', 'id' => 'kabupaten_dom', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kecamatan_dom">Kecamatan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('kecamatan_dom', [], null, [ 'class' => 'form-control text-uppercase', 'id' => 'kecamatan_dom', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kelurahan_dom">Kelurahan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('kelurahan_dom', null, [ 'class' => 'form-control text-uppercase', 'id' => 'kelurahan_dom', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kodepos_dom">Kodepos <span class="required">*</span></label>
                                    <div class="col-md-4">
                                        {!! Form::text('kodepos_dom', null, [ 'class' => 'form-control', 'id' => 'kodepos_dom', 'maxlength' => 5]) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="phone_dom">No. Telpon <span class="required">*</span></label>
                                    <div class="col-md-6">
                                        {!! Form::text('phone_dom', null, [ 'class' => 'form-control', 'id' => 'phone_dom', 'maxlength' => 30]) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Nomor telepon yang bisa di hubungi tinggal sekarang</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        {{-- Pendidikan --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-university font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Pendidikan & Data Tambahan</span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="sekolah">Nama Sekolah <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('sekolah', null, [ 'class' => 'form-control text-uppercase','id' => 'sekolah',  'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="jurusan">Jurusan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('jurusan', null, [ 'class' => 'form-control text-uppercase','id' => 'jurusan',  'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="marital">Status Pernikahan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('marital', $marital, null, [ 'class' => 'form-control text-uppercase', 'id' => 'marital', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="agama">Agama <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('agama', $agama, null, [ 'class' => 'form-control text-uppercase', 'id' => 'agama', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="kk">No. KK <br><small>(Kartu Keluarga)</small> <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('kk', null, [ 'class' => 'form-control', 'id' => 'kk', 'maxlength' => 16, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">16 angka</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="nik">NIK <br><small>(No. KTP)</small> <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('nik', null, [ 'class' => 'form-control', 'id' => 'nik', 'maxlength' => 16, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">16 angka</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="bank_name">Bank</label>
                                    <div class="col-md-8">
                                        {!! Form::select('bank_name', $bank, null, [ 'class' => 'form-control text-uppercase', 'id' => 'bank_name', 'placeholder' => 'Pilih']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="bank_account_no">No. Rekening</label>
                                    <div class="col-md-8">
                                        {!! Form::text('bank_account_no', null, [ 'class' => 'form-control', 'id' => 'bank_account_no']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">BCA 10 angka, CIMB NIAGA 11 angka</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="npwp">No. NPWP</label>
                                    <div class="col-md-8">
                                        {!! Form::text('npwp', null, [ 'class' => 'form-control', 'id' => 'npwp', 'maxlength' => 15]) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Diisi tanpa tanda baca ( . ) titik dan ( - ) strip</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="bank_branch">Cabang Bank Pembuka</label>
                                    <div class="col-md-8">
                                        {!! Form::text('bank_branch', null, [ 'class' => 'form-control text-uppercase', 'id' => 'bank_branch']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Lihat dibuku tabungan, apabila BCA diawali KCP/KCU (contoh : KCP PONDOK TIMUR), apabila CIMB NIAGA diawali CIMB (contoh : CIMB JUANDA)</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="email">Email <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::email('email', null, [ 'class' => 'form-control text-lowercase', 'id' => 'email', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="tgl_tes">Tanggal Tes Tertulis <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('tgl_tes', null, [ 'class' => 'form-control', 'id' => 'tgl_tes', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">31/04/2000</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="skck_validity">Masa Berlaku SKCK <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('skck_validity', null, [ 'class' => 'form-control', 'id' => 'skck_validity', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">31/04/2000</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="jemputan">Rute Jemputan <span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::select('jemputan', $jemputan, null, [ 'class' => 'form-control text-uppercase', 'id' => 'jemputan', 'placeholder' => 'Pilih', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="father">Nama Ayah Anda<span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('father', null, [ 'class' => 'form-control text-uppercase', 'id' => 'father', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="sekolah_kab">Kota Tempat Anda Sekolah<span class="required">*</span></label>
                                    <div class="col-md-8">
                                        {!! Form::text('sekolah_kab', null, [ 'class' => 'form-control text-uppercase', 'id' => 'sekolah_kab', 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="thn_lulus">Tahun Lulus Sekolah Anda<span class="required">*</span></label>
                                    <div class="col-md-4">
                                        {!! Form::text('thn_lulus', null, [ 'class' => 'form-control', 'id' => 'thn_lulus', 'maxlength' => 4, 'required']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        {{-- Pengalaman Kerja --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-black-tie font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Pengalaman Kerja 1</span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_company_1">Nama Perusahaan</label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_company_1', null, [ 'class' => 'form-control text-uppercase', 'id' => 'exp_company_1']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_company_type_1">Jenis Perusahaan</label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_company_type_1', null, [ 'class' => 'form-control text-uppercase', 'id' => 'exp_company_type_1']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">CONTOH : JASA, OTOMOTIF, ELEKTRONIK, GARMENT, RETAIL, DLL</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_position_1">Posisi Terakhir <br><small>(Bagian)</small></label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_position_1', null, [ 'class' => 'form-control text-uppercase', 'id' => 'exp_position_1']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">CONTOH : ASSY, PACKING, QC, DLL</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_salary_1">Gaji Rata-Rata Per Bulan</label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_salary_1', null, [ 'class' => 'form-control', 'id' => 'exp_salary_1', 'placeholder' => '']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">CONTOH : 4500000 (dibulatkan)</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_start_1">Masa Kerja <br><small>(Bulan dan Tahun)</small></label>
                                    <div class="col-md-4">
                                        {!! Form::text('exp_start_1', null, [ 'class' => 'form-control', 'id' => 'exp_start_1']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Contoh : 01/2019 - 12/2019</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-black-tie font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Pengalaman Kerja 2</span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_company_2">Nama Perusahaan</label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_company_2', null, [ 'class' => 'form-control text-uppercase', 'id' => 'exp_company_2']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_company_type_2">Jenis Perusahaan</label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_company_type_2', null, [ 'class' => 'form-control text-uppercase', 'id' => 'exp_company_type_2']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">CONTOH : JASA, OTOMOTIF, ELEKTRONIK, GARMENT, RETAIL, DLL</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_position_2">Posisi Terakhir <br><small>(Bagian)</small></label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_position_2', null, [ 'class' => 'form-control text-uppercase', 'id' => 'exp_position_2']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">CONTOH : ASSY, PACKING, QC, DLL</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_salary_2">Gaji Rata-Rata Per Bulan</label>
                                    <div class="col-md-8">
                                        {!! Form::text('exp_salary_2', null, [ 'class' => 'form-control', 'id' => 'exp_salary_2', 'placeholder' => '']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">CONTOH : 4500000 (dibulatkan)</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="exp_start_2">Masa Kerja <br><small>(Bulan dan Tahun)</small></label>
                                    <div class="col-md-4">
                                        {!! Form::text('exp_start_2', null, [ 'class' => 'form-control', 'id' => 'exp_start_2']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Contoh : 01/2019 - 12/2019</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        {{-- Data Vaksin --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-image-o font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Data Vaksin Covid-19</span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="vac_type">Jenis Vaksin</label>
                                    <div class="col-md-8">
                                        {!! Form::select('vac_type', $vacType, null, [ 'class' => 'form-control text-uppercase', 'id' => 'vac_type', 'placeholder' => 'Pilih']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label">Vaksin ke-1</label>
                                    <div class="col-md-8">
                                        <div class="md-checkbox-list">
                                            <div class="md-checkbox">
                                                <input type="checkbox" name="vac_1" id="vac_1" class="md-check" value="1">
                                                <label for="vac_1">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> 
                                                    Sudah
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="vac_1_date">Tanggal Vaksin ke-1</label>
                                    <div class="col-md-8">
                                        {!! Form::text('vac_1_date', null, [ 'class' => 'form-control dateinput', 'id' => 'vac_1_date']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">10/09/2021</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="vac_1_loc">Lokasi Vaksin ke-1</label>
                                    <div class="col-md-8">
                                        {!! Form::text('vac_1_loc', null, [ 'class' => 'form-control text-uppercase', 'id' => 'vac_1_loc']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label">Vaksin ke-2</label>
                                    <div class="col-md-8">
                                        <div class="md-checkbox-list">
                                            <div class="md-checkbox">
                                                <input type="checkbox" name="vac_2" id="vac_2" class="md-check" value="1">
                                                <label for="vac_2">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> 
                                                    Sudah
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="vac_2_date">Tanggal Vaksin ke-2</label>
                                    <div class="col-md-8">
                                        {!! Form::text('vac_2_date', null, [ 'class' => 'form-control dateinput', 'id' => 'vac_2_date']) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">10/09/2021</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="vac_2_loc">Lokasi Vaksin ke-2</label>
                                    <div class="col-md-8">
                                        {!! Form::text('vac_2_loc', null, [ 'class' => 'form-control text-uppercase', 'id' => 'vac_2_loc']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        {{-- File --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-image-o font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Dokumen</span>
                                </div>
                                <div class="form-group">
                                    <label for="pas_foto" class="col-md-4 control-label">KTP</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                            <div>
                                                <span class="btn red btn-outline btn-file">
                                                    <span class="fileinput-new"> Pilih KTP</span>
                                                    <span class="fileinput-exists"> Ubah </span>
                                                    <input type="file" name="file_ktp" accept=".jpg,.png,.jpeg">
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                            </div>
                                        </div>
                                        <div class="clearfix margin-top-10 small">
                                            <span class="label label-sm label-warning">NOTE</span> 
                                            <span class="text-warning">Ukuran file harus kurang dari 4 MB</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file_kk" class="col-md-4 control-label">KK <small>(Kartu Keluarga)</small></label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                            <div>
                                                <span class="btn red btn-outline btn-file">
                                                    <span class="fileinput-new"> Pilih KK</span>
                                                    <span class="fileinput-exists"> Ubah </span>
                                                    <input type="file" name="file_kk" accept=".jpg,.png,.jpeg">
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                            </div>
                                        </div>
                                        <div class="clearfix margin-top-10 small">
                                            <span class="label label-sm label-warning">NOTE</span> 
                                            <span class="text-warning">Ukuran file harus kurang dari 4 MB</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file_vaksin" class="col-md-4 control-label">Sertifikat Vaksin</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                            <div>
                                                <span class="btn red btn-outline btn-file">
                                                    <span class="fileinput-new"> Pilih Sertifikat Vaksin</span>
                                                    <span class="fileinput-exists"> Ubah </span>
                                                    <input type="file" name="file_vaksin" accept=".jpg,.png,.jpeg">
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                            </div>
                                        </div>
                                        <div class="clearfix margin-top-10 small">
                                            <span class="label label-sm label-warning">NOTE</span> 
                                            <span class="text-warning">Ukuran file harus kurang dari 4 MB</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-image-o font-white"></i>
                                    <span class="caption-subject bold uppercase"> </span>
                                </div>
                                <div class="form-group">
                                    <label for="file_skck" class="col-md-4 control-label">SKCK</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                            <div>
                                                <span class="btn red btn-outline btn-file">
                                                    <span class="fileinput-new"> Pilih SKCK</span>
                                                    <span class="fileinput-exists"> Ubah </span>
                                                    <input type="file" name="file_skck" accept=".jpg,.png,.jpeg">
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                            </div>
                                        </div>
                                        <div class="clearfix margin-top-10 small">
                                            <span class="label label-sm label-warning">NOTE</span> 
                                            <span class="text-warning">Ukuran file harus kurang dari 4 MB</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file_tab" class="col-md-4 control-label">Buku Tabungan</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                            <div>
                                                <span class="btn red btn-outline btn-file">
                                                    <span class="fileinput-new"> Pilih Buku Tabungan</span>
                                                    <span class="fileinput-exists"> Ubah </span>
                                                    <input type="file" name="file_tab" accept=".jpg,.png,.jpeg">
                                                </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                            </div>
                                        </div>
                                        <div class="clearfix margin-top-10 small">
                                            <span class="label label-sm label-warning">NOTE</span> 
                                            <span class="text-warning">Ukuran file harus kurang dari 4 MB</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="submit" class="btn blue">SUBMIT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@section('js')
    <script src="{{ asset('js/metronic.js') }}"></script>
    <script src="{!! asset('modules/memberregistration/js/form-karyawan.validation.js') !!}"></script>
    <script>
        $(document).ready(function()
        {
            $(".alert").delay(20000).slideUp("slow");
            $('#tgl_lahir').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $('#tgl_tes').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $('#skck_validity').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $('.dateinput').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            var emr_provinsi = $('#emr_provinsi');
            var emr_kabupaten = $('#emr_kabupaten');
            var emr_kecamatan = $('#emr_kecamatan');
            var provinsi = $('#provinsi');
            var kabupaten = $('#kabupaten');
            var kecamatan = $('#kecamatan');
            var provinsi_dom = $('#provinsi_dom');
            var kabupaten_dom = $('#kabupaten_dom');
            var kecamatan_dom = $('#kecamatan_dom');
            let placeHolder = '<option value="">Pilih</option>';
            emr_provinsi.on('change', function(e) {
                e.preventDefault();
                let provId = e.target.value;
                if(provId !== '') {
                    $.ajax({
                        url: "{!! route('kabupaten.list') !!}",
                        type: "GET",
                        data: { prov_id: provId },
                        success: function (data) {
                            emr_kabupaten.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                emr_kabupaten.append('<option value="' + kab.code + '">' + kab.name + '</option>');
                            });
                            emr_kecamatan.empty().append(placeHolder);
                        }
                    });
                } else {
                    emr_kabupaten.empty().append(placeHolder);
                    emr_kecamatan.empty().append(placeHolder);
                }
            });
            provinsi.on('change', function(e) {
                e.preventDefault();
                let provId = e.target.value;
                if(provId !== '') {
                    $.ajax({
                        url: "{!! route('kabupaten.list') !!}",
                        type: "GET",
                        data: { prov_id: provId },
                        success: function (data) {
                            kabupaten.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                kabupaten.append('<option value="' + kab.code + '">' + kab.name + '</option>');
                            });
                            kecamatan.empty().append(placeHolder);
                        }
                    });
                } else {
                    kabupaten.empty().append(placeHolder);
                    kecamatan.empty().append(placeHolder);
                }
            });
            provinsi_dom.on('change', function(e) {
                e.preventDefault();
                let provId = e.target.value;
                if(provId !== '') {
                    $.ajax({
                        url: "{!! route('kabupaten.list') !!}",
                        type: "GET",
                        data: { prov_id: provId },
                        success: function (data) {
                            kabupaten_dom.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                kabupaten_dom.append('<option value="' + kab.code + '">' + kab.name + '</option>');
                            });
                            kecamatan_dom.empty().append(placeHolder);
                        }
                    });
                } else {
                    kabupaten_dom.empty().append(placeHolder);
                    kecamatan_dom.empty().append(placeHolder);
                }
            });
            emr_kabupaten.on('change', function(e) {
                e.preventDefault();
                let kabId = e.target.value;
                if(kabId !== '') {
                    $.ajax({
                        url: "{!! route('kecamatan.list') !!}",
                        type: "GET",
                        data: { kab_id: kabId },
                        success: function (data) {
                            emr_kecamatan.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                emr_kecamatan.append('<option value="' + kab.code + '">' + kab.name + '</option>');
                            });
                        }
                    });
                } else {
                    emr_kecamatan.empty().append(placeHolder);
                }
            });
            kabupaten.on('change', function(e) {
                e.preventDefault();
                let kabId = e.target.value;
                if(kabId !== '') {
                    $.ajax({
                        url: "{!! route('kecamatan.list') !!}",
                        type: "GET",
                        data: { kab_id: kabId },
                        success: function (data) {
                            kecamatan.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                kecamatan.append('<option value="' + kab.code + '">' + kab.name + '</option>');
                            });
                        }
                    });
                } else {
                    kecamatan.empty().append(placeHolder);
                }
            });
            kabupaten_dom.on('change', function(e) {
                e.preventDefault();
                let kabId = e.target.value;
                if(kabId !== '') {
                    $.ajax({
                        url: "{!! route('kecamatan.list') !!}",
                        type: "GET",
                        data: { kab_id: kabId },
                        success: function (data) {
                            kecamatan_dom.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                kecamatan_dom.append('<option value="' + kab.code + '">' + kab.name + '</option>');
                            });
                        }
                    });
                } else {
                    kecamatan_dom.empty().append(placeHolder);
                }
            });
        });
    </script>
    
@endsection