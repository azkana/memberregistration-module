@extends('memberregistration::frontend.formulir.shared.app')

@section('content')
                     
@if(isset($messages))
<div class="row" style="margin-top: 100px">
    <div class="col-md-6 col-md-offset-3">
        <div class="note note-warning">
            <h4 class="block bold uppercase">Pengumuman</h4>
            <p> {!! $messages !!} </p>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <ul class="list-group">
            <li class="list-group-item bg-yellow-gold bg-font-yellow-gold">
                <strong>Note:</strong><br>
                Form Pendaftaran pindah ke halaman lain. Halaman ini akan otomatis <i>redirect</i> ke beranda.<br>
                Kemudian klik tombol <b>DAFTAR</b>.
            </li>
        </ul>
    </div>
</div>
@endif

@endsection

@section('js')
<script src="{!! asset('modules/memberregistration/js/form-member.validation.js') !!}"></script>
<script>
    $(document).ready(function()
    {
        setTimeout(function(){
            window.location.href = '{!! route("cms.home") !!}';
        }, 5000);
    });
</script>

@endsection