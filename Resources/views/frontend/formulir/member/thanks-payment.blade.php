<!DOCTYPE html>
<html lang="{!! str_replace('_', '-', app()->getLocale()) !!}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <title>{!! $title !!}</title>
        <link rel="shortcut icon" href="{!! config('filesystems.disks.s3.url') . '/core/settings/' . getOptions('favicon') !!}" />

        <link href="{!! asset('css/metronic.css') !!}" rel="stylesheet">
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width" style="background-color: #ffffff">
        <div class="page-wrapper">
            <div class="page-container" style="margin-top: 0">
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="text-center">
                            <img src="{!! Storage::disk('s3')->url('core/settings/'. getOptions('favicon')) !!}" style="max-width: 100px"/>
                        </div>
                        <h1 class="page-title text-center" style="margin-top: -15px; font-size: 18px; margin-bottom: 100px"> PT Dhasnarindo Karya Utama </h1>
                        <div class="col-md-12 col-lg-6 col-lg-offset-3">
                            @if(! is_null($data) )
                            <div class="note note-success">
                                <h4 class="block">Selamat konfirmasi pembayaran anda berhasil!</h4>
                                <p>Terimakasih telah mendaftarkan diri Anda <b>{!! $data->full_name !!}</b>.</p>
                                <p>Silahkan periksa email anda secara berkala </p>
                                <p>Atau klik <a href="{!! route('member.register.confirm', $data->pub_id) !!}"><b>disini</b></a> untuk melihat status member anda.</p>
                            </div>
                            @else
                            <div class="note note-danger">
                                <h4 class="block">Data tidak ditemukan.</h4>
                            </div>
                            @endif
                            <div class="text-center" style="margin-top: 100px;">
                                {!! getOptions('app_year') !!} &copy; 
                                <a href="{!! route('cms.home') !!}" target="_blank" class=""> PT Dhasnarindo Karya Utama </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>