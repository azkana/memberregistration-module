@extends('memberregistration::frontend.formulir.shared.app')

@section('content')

@if(!isset($data->has_subs->has_confirmation))
    {!! Form::open([ 'route' => 'member.register.confirm.store', 'class' => 'form-horizontal', 'id' => 'frm_member_payment_confirmation', 'files' => true, 'autocomplete' => 'off'])!!}
    {!! Form::hidden('member_subscription_id', $data->has_subs->id) !!}
    {!! Form::hidden('pub_id', $data->has_subs->pub_id) !!}
        <div class="row">
            <div class="col-md-12 col-lg-offset-3 col-lg-6">
                <ul class="list-group">
                    <li class="list-group-item bg-yellow-gold bg-font-yellow-gold"> 
                        <strong>Note:</strong> Mohon untuk menggunakan browser Google Chrome terbaru. 
                    </li>
                </ul>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-haze">
                            <i class="fa fa-user font-green-haze"></i>
                            <span class="caption-subject bold uppercase"> Data Pembayaran</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="full_name">Nama Lengkap</label>
                                        <div class="col-md-7">
                                            {!! Form::text('full_name', $data->full_name, [ 'class' => 'form-control text-uppercase', 'id' => 'full_name', 'disabled']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="nik">NIK</label>
                                        <div class="col-md-7">
                                            {!! Form::text('nik', $data->nik, [ 'class' => 'form-control text-uppercase', 'id' => 'nik', 'disabled']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="subs_price">Jumlah Tagihan</label>
                                        <div class="col-md-7">
                                            {!! Form::text('subs_price', isset($data->has_subs->subs_price) ? $data->has_subs->subs_price + $data->has_subs->unique_no : '0', [ 'class' => 'form-control number', 'id' => 'subs_price', 'disabled']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="bank_account_id">Bank Tujuan <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            {!! Form::select('bank_account_id', $bankAccount, null, [ 'class' => 'form-control text-uppercase', 'id' => 'bank_account_id', 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="transfer_date">Tanggal Bayar <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            {!! Form::text('transfer_date', null, [ 'class' => 'form-control', 'id' => 'transfer_date','required']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="transfer_amount">Jumlah Bayar <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            {!! Form::text('transfer_amount', null, [ 'class' => 'form-control number', 'id' => 'transfer_amount', 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block small">Jumlah Bayar harus sama dengan Jumlah Tagihan</span>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="transfer_bank_id">Bank Asal <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            {!! Form::select('transfer_bank_id', $bank, null, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_id', 'placeholder' => 'Pilih', 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input hide group-bank-name">
                                        <label class="col-md-3 control-label" for="transfer_bank_name">Nama Bank Lain <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            {!! Form::text('transfer_bank_name', null, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_name', 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="transfer_bank_account">No. Rekening <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            {!! Form::text('transfer_bank_account', null, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_account', 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="transfer_bank_holder_name">Nama Pemilik Rekening <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            {!! Form::text('transfer_bank_holder_name', null, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_holder_name', 'required']) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="transfer_attachment" class="control-label col-md-3">Bukti Transfer <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Pilih Bukti Transfer</span>
                                                        <span class="fileinput-exists"> Ubah </span>
                                                        <input type="file" name="transfer_attachment" id="transfer_attachment" accept=".jpg,.png,.jpeg" required> 
                                                    </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10 small">
                                                <span class="label label-sm label-warning">NOTE</span><br>
                                                <span class="text-warning">Silahkan lengkapi bukti transfer.<br>Ukuran file harus kurang dari 4 MB</span><br>
                                                <span class="text-warning">File yang diupload harus berupa <strong>bukti transfer</strong> yang sudah berhasil.<br>Mohon jangan upload <strong>pas foto, foto layar mesin ATM dll.</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green-jungle">Konfirmasi</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@else
    @php
        $jmlTagihan     = $data->has_subs->subs_price + $data->has_subs->unique_no;
        $jmlTransfer    = $data->has_subs->has_confirmation->transfer_amount;
        if($jmlTagihan == $jmlTransfer) {
            $disabled = 'disabled';
            $required = '';
        } else {
            $disabled = '';
            $required = 'required';
        }
    @endphp
    @if(empty($data->has_subs->has_confirmation->transfer_attachment) OR $jmlTagihan <> $jmlTransfer)
    {!! Form::open([ 'route' => ['member.register.confirm.update', $data->has_subs->has_confirmation->id], 'method' => 'patch', 'class' => 'form-horizontal', 'id' => 'frm_member_payment_confirmation', 'files' => true])!!}
    {!! Form::hidden('pub_id', $data->has_subs->pub_id) !!}
    @endif
    <div class="row">
        <div class="col-md-12 col-lg-offset-3 col-lg-6">
            @if (session('success_message'))
            <div class="alert alert-dismissable bg-green-jungle font-white">
                <button aria-hidden="true" data-dismiss="alert" class="close font-white" type="button">×</button>
                <strong>Success!</strong> <br>{!! session('success_message') !!}
            </div>
            @endif
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <i class="fa fa-user font-green-haze"></i>
                        <span class="caption-subject bold uppercase"> Data Pembayaran</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="full_name">Nama Lengkap</label>
                                    <div class="col-md-7">
                                        {!! Form::text('full_name', $data->full_name, [ 'class' => 'form-control text-uppercase', 'id' => 'full_name', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="nik">NIK</label>
                                    <div class="col-md-7">
                                        {!! Form::text('nik', $data->nik, [ 'class' => 'form-control text-uppercase', 'id' => 'nik', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="subs_price">Jumlah Tagihan</label>
                                    <div class="col-md-7">
                                        {!! Form::text('subs_price', isset($data->has_subs->subs_price) ? $data->has_subs->subs_price + $data->has_subs->unique_no : '0', [ 'class' => 'form-control number', 'id' => 'subs_price', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="bank_account_id">Bank Tujuan</label>
                                    <div class="col-md-7">
                                        {!! Form::select('bank_account_id', $bankAccount, $data->has_subs->has_confirmation->bank_account_id, [ 'class' => 'form-control text-uppercase', 'id' => 'bank_account_id', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="transfer_date">Tanggal Bayar</label>
                                    <div class="col-md-7">
                                        {!! Form::text('transfer_date', date('d-m-Y', strtotime($data->has_subs->has_confirmation->transfer_date)), [ 'class' => 'form-control', 'id' => 'transfer_date','disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="transfer_amount">Jumlah Bayar</label>
                                    <div class="col-md-7">
                                        {!! Form::text('transfer_amount', $data->has_subs->has_confirmation->transfer_amount, [ 'class' => 'form-control number', 'id' => 'transfer_amount', $required, $disabled]) !!}
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block small">Jumlah Bayar harus sama dengan Jumlah Tagihan</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="transfer_bank_id">Bank Asal</label>
                                    <div class="col-md-7">
                                        {!! Form::select('transfer_bank_id', $bank, $data->has_subs->has_confirmation->transfer_bank_id, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_id', 'placeholder' => 'Pilih', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input @if($data->has_subs->has_confirmation->transfer_bank_id <> '999') hide @endif">
                                    <label class="col-md-3 control-label" for="transfer_bank_name">Nama Bank Lain</label>
                                    <div class="col-md-7">
                                        {!! Form::text('transfer_bank_name', $data->has_subs->has_confirmation->transfer_bank_name, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_name', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="transfer_bank_account">No. Rekening</label>
                                    <div class="col-md-7">
                                        {!! Form::text('transfer_bank_account', $data->has_subs->has_confirmation->transfer_bank_account, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_account', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="transfer_bank_holder_name">Nama Pemilik Rekening</label>
                                    <div class="col-md-7">
                                        {!! Form::text('transfer_bank_holder_name', $data->has_subs->has_confirmation->transfer_bank_holder_name, [ 'class' => 'form-control text-uppercase', 'id' => 'transfer_bank_holder_name', 'disabled']) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="payment_status">Status</label>
                                    <div class="col-md-7">
                                        <label class="label label-info">{!! getMemberStatus($data->has_subs->payment_status) !!}</label>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                @if(empty($data->has_subs->has_confirmation->transfer_attachment))
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Bukti Transfer <span class="required">*</span></label>
                                        <div class="col-md-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                <div>
                                                    <span class="btn red btn-outline btn-file">
                                                        <span class="fileinput-new"> Pilih Bukti Transfer </span>
                                                        <span class="fileinput-exists"> Ubah </span>
                                                        <input type="file" name="transfer_attachment" accept=".jpg,.png,.jpeg" required>
                                                    </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group form-md-line-input">
                                        <label class="control-label col-md-3">Bukti Transfer</label>
                                        <div class="col-md-7">
                                            @if(!empty($data->has_subs->has_confirmation->transfer_attachment))
                                                @if(config('memberregistration.disk') == 's3')
                                                    @if(Storage::disk('s3')->exists('members/'.$data->nik.'/'.$data->transfer_attachment))
                                                        <img src="{!! Storage::disk('s3')->url('members/'.$data->nik.'/'.$data->has_subs->has_confirmation->transfer_attachment) !!}" alt="" style="max-height:200px;max-width:250px;border:1px solid #ECF0F5;border-radius:10px">
                                                    @else
                                                        <img src="{!! Storage::disk('local')->url('members/'.$data->nik.'/'.$data->has_subs->has_confirmation->transfer_attachment) !!}" alt="" style="max-height:200px;max-width:250px;border:1px solid #ECF0F5;border-radius:10px">
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if(empty($data->has_subs->has_confirmation->transfer_attachment) OR $jmlTagihan <> $jmlTransfer)
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <button type="submit" class="btn green-jungle">Konfirmasi Pembayaran</button>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    @if(empty($data->has_subs->has_confirmation->transfer_attachment) OR $jmlTagihan <> $jmlTransfer)
    {!! Form::close() !!}
    @endif
@endif
@endsection

@section('js')

    <script src="{!! asset('modules/memberregistration/js/form-member-confirmation.validation.js') !!}"></script>
    <script>
        $(document).ready(function()
        {
            $('#transfer_date').inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $(".number").number(true, 0);

            $('#transfer_bank_id').on('change', function(e) {
                var bank_id = e.target.value;
                var group_bank_name = $('.group-bank-name');
                if(bank_id === '999') {
                    group_bank_name.removeClass('hide');
                } else {
                    group_bank_name.addClass('hide');
                }
            });
        });
    </script>

@endsection