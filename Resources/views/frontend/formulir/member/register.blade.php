@extends('memberregistration::frontend.formulir.shared.app')

@section('content')

{!! Form::open([ 'route' => 'member.register.save', 'class' => 'form-horizontal', 'id' => 'frm_member', 'files' => true, 'autocomplete' => 'off'])!!}

@if(isset($messages))
<div class="row" style="margin-top: 100px">
    <div class="col-md-6 col-md-offset-3">
        <div class="note note-warning">
            <h4 class="block bold uppercase">Pengumuman</h4>
            <p> {!! $messages !!} </p>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-12">
        <ul class="list-group">
            <li class="list-group-item bg-yellow-gold bg-font-yellow-gold">
                <strong>Note:</strong> Mohon untuk menggunakan browser Google Chrome terbaru.<br>
            </li>
        </ul>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Data Diri</span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="full_name">Nama Lengkap <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('full_name', null, [ 'class' => 'form-control text-uppercase', 'id' => 'full_name', 'required', 'autofocus']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="nik">NIK <small>(No. KTP)</small> <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('nik', null, [ 'class' => 'form-control', 'id' => 'nik', 'maxlength' => 16, 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="email">Email <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::email('email', null, [ 'class' => 'form-control text-lowercase', 'id' => 'email', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="no_hp">No. HP <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('no_hp', null, [ 'class' => 'form-control', 'id' => 'no_hp', 'maxlength' => 30, 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">085123456789</span>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="no_wa">No. WA <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('no_wa', null, [ 'class' => 'form-control', 'id' => 'no_wa', 'maxlength' => 30, 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">085123456789</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="tmp_lahir">Tempat Lahir <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('tmp_lahir', null, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="tgl_lahir">Tanggal Lahir <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('tgl_lahir', null, [ 'class' => 'form-control', 'id' => 'tgl_lahir','required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-radios">
                                <label class="col-md-4 control-label" for="gender">Jenis Kelamin <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <div class="md-radio-inline">
                                        <div class="md-radio">
                                            <input type="radio" id="radio53" name="gender" class="md-radiobtn" value="L">
                                            <label for="radio53">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Laki-laki
                                            </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="radio54" name="gender" class="md-radiobtn" value="P">
                                            <label for="radio54">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Perempuan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="tb">Tinggi Badan <span class="required">*</span></label>
                                <div class="col-md-4">
                                    {!! Form::text('tb', null, [ 'class' => 'form-control text-uppercase number', 'maxlength' => 3, 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">Tinggi Badan</span>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="bb">Berat Badan <span class="required">*</span></label>
                                <div class="col-md-4">
                                    {!! Form::text('bb', null, [ 'class' => 'form-control text-uppercase number', 'maxlength' => 3, 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">Berat Badan</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption font-green-haze">
                                <i class="fa fa-map-marker font-green-haze"></i>
                                <span class="caption-subject bold uppercase"> Alamat (Sesuai KTP)</span>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="alamat">Alamat <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::textarea('alamat', null, [ 'class' => 'form-control text-uppercase', 'rows' => 2, 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="kabupaten">Kota/Kab. <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('kabupaten', null, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="provinsi">Provinsi <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::select('provinsi', $provinsi, null, [ 'class' => 'form-control text-uppercase', 'placeholder' => 'Pilih', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="kodepos">Kodepos <span class="required">*</span></label>
                                <div class="col-md-4">
                                    {!! Form::text('kodepos', null, [ 'class' => 'form-control', 'maxlength' => 5, 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="caption font-green-haze">
                                <i class="fa fa-university font-green-haze"></i>
                                <span class="caption-subject bold uppercase"> Pendidikan Terakhir</span>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="jenjang">Jenjang <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::select('jenjang', $jenjang, null, [ 'class' => 'form-control', 'placeholder' => 'Pilih', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="sekolah">Sekolah / Universitas <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('sekolah', null, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="jurusan">Jurusan <span class="required">*</span></label>
                                <div class="col-md-8">
                                    {!! Form::text('jurusan', null, [ 'class' => 'form-control text-uppercase', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="thn_lulus">Tahun Lulus <span class="required">*</span></label>
                                <div class="col-md-4">
                                    {!! Form::selectYear('thn_lulus', date('Y'), 2000, null, [ 'class' => 'form-control text-uppercase', 'placeholder' => 'Pilih', 'required']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption font-green-haze">
                                <i class="fa fa-black-tie font-green-haze"></i>
                                <span class="caption-subject bold uppercase"> Pengalaman Kerja</span>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="exp_company_1">Perusahaan</label>
                                <div class="col-md-8">
                                    {!! Form::text('exp_company_1', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="exp_position_1">Jabatan</label>
                                <div class="col-md-8">
                                    {!! Form::text('exp_position_1', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="exp_duration_year_1">Lama Kerja</label>
                                <div class="col-md-4">
                                    {!! Form::select('exp_duration_year_1', $tahun_kerja, null, [ 'class' => 'form-control text-uppercase', 'placeholder' => 'Pilih']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::select('exp_duration_month_1', $bulan_kerja, null, [ 'class' => 'form-control text-uppercase', 'placeholder' => 'Pilih']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="exp_company_2">Perusahaan</label>
                                <div class="col-md-8">
                                    {!! Form::text('exp_company_2', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="exp_position_2">Jabatan</label>
                                <div class="col-md-8">
                                    {!! Form::text('exp_position_2', null, [ 'class' => 'form-control', 'maxlength' => 100]) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label" for="exp_duration_year_2">Lama Kerja</label>
                                <div class="col-md-4">
                                    {!! Form::select('exp_duration_year_2', $tahun_kerja, null, [ 'class' => 'form-control text-uppercase', 'placeholder' => 'Pilih']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::select('exp_duration_month_2', $bulan_kerja, null, [ 'class' => 'form-control text-uppercase', 'placeholder' => 'Pilih']) !!}
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="caption font-green-haze">
                                <i class="fa fa-tasks font-green-haze"></i>
                                <span class="caption-subject bold uppercase"> Keahlian</span>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-1 control-label" for=""></label>
                                <div class="col-md-10">
                                    <div class="md-checkbox-list">
                                        @foreach($skills as $skill)
                                        <div class="md-checkbox">
                                            <input type="checkbox" name="skill[]" id="checkbox{!! $idInput++ !!}" class="md-check" value="{!! $skill !!}">
                                            <label for="checkbox{!! $idLabel++ !!}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> {!! $skill !!}
                                            </label>
                                        </div>
                                        @endforeach
                                    </div>
                                    {!! Form::textarea('skill_lain', null, [ 'class' => 'form-control', 'rows' => 1, 'placeholder' => 'Lainnya']) !!}
                                    <div class="form-control-focus"> </div>
                                    <span class="help-block">Bahasa Jepang,Bahasa Arab</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption font-green-haze">
                                <i class="fa fa-file-image-o font-green-haze"></i>
                                <span class="caption-subject bold uppercase"> Dokumen</span>
                            </div>
                            <div class="form-group">
                                <label for="pas_foto" class="col-md-4 control-label">Pas Foto</label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                        <div>
                                            <span class="btn red btn-outline btn-file">
                                                <span class="fileinput-new"> Pilih Foto</span>
                                                <span class="fileinput-exists"> Ubah </span>
                                                <input type="file" name="pas_foto" accept=".jpg,.png,.jpeg">
                                            </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10 small">
                                        <span class="label label-sm label-warning">NOTE</span>
                                        <span class="text-warning">Ukuran file harus kurang dari 4 MB</span>
                                    </div>
                                    {{-- <img src="" id="img_foto" alt="pas-foto" style="max-height:200px;max-width:250px;border:1px solid #ECF0F5;border-radius:10px">
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-control">
                                            <input type="text" name="foto" id="foto" class="form-control" value="" readonly>
                                            <div class="form-control-focus"> </div>
                                        </div>
                                        <span class="input-group-btn btn-right">
                                            <a href="{!! route('document.uploader.create') !!}" id="btn_file_upload" class="btn green-jungle btn-outline btn-file" data-toggle="modal" data-target="#modalMedium">
                                                <i class='fa fa-upload'></i> Upload Foto
                                            </a>
                                            <button id="btn_file_trash" class="btn red-mint btn-outline btn-file"><i class='fa fa-trash'></i> Remove</button>
                                        </span>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            {{-- <div class="form-group form-md-line-input" style="margin-top: 20px">
                                <label class="control-label col-md-4">KTP</label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                        </div>
                                        <div>
                                            <span class="btn red btn-outline btn-file">
                                                <span class="fileinput-new"> Pilih Foto</span>
                                                <span class="fileinput-exists"> Ubah </span>
                                                <input type="file" name="ktp" accept=".jpg,.png,.jpeg">
                                            </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-checkbox">
                                <label class="col-md-4 control-label" for=""></label>
                                <div class="col-md-8">
                                    <div class="md-checkbox-list">
                                        <div class="md-checkbox">
                                            <input type="checkbox" name="tnc" id="tnc" class="md-check" value="tnc">
                                            <label for="tnc">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                    Setuju dengan
                                                    @if(!empty(getOptions('membership_tnc')))
                                                    <a href="#modal-tnc" data-toggle="modal">
                                                        Syarat dan Ketentuan
                                                    </a>
                                                    @else
                                                    Syarat dan Ketentuan
                                                    @endif
                                            </label>
                                        </div>
                                    </div>
                                    {!! Form::hidden('subs_price', getOptions('membership_subscription')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Syarat & Ketentuan --}}
                    @if(!empty(getOptions('membership_tnc')))
                    <div class="modal fade" id="modal-tnc" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Syarat & Ketentuan Member PT. Dhasnarindo Karya Utama (DKU)</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="portlet light">
                                        <div class="portlet-body">
                                            <div class="scroller" style="height:400px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                                                {!! getOptions('membership_tnc') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-md-checkbox">
                                                <label class="col-md-1 control-label"></label>
                                                <div class="col-md-8">
                                                    <div class="md-checkbox-list">
                                                        <div class="md-checkbox">
                                                            <input type="checkbox" name="pre_tnc" id="pre_tnc" class="md-check">
                                                            <label for="pre_tnc">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>
                                                                Saya setuju dengan Syarat dan ketentuan di atas.
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm blue btn-outline" id="btn_accept" data-dismiss="modal">DAFTAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn blue">Register</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade draggable-modal ui-draggable text-left" id="modalMedium" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <img src="{!! asset('img/loading-spinner-grey.gif') !!}" alt="" class="loading">
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
@endif
{!! Form::close() !!}

@endsection

@section('js')
{{-- <script src="{!! asset('modules/memberregistration/js/form-member.dropzone.js') !!}"></script> --}}
<script src="{!! asset('modules/memberregistration/js/form-member.validation.js') !!}"></script>
<script>
    $(document).ready(function()
    {
        $('#tgl_lahir').inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy"
        });
        $(".number").number(true,0);

        let modalTnc = $('#modal-tnc');
        let checkPreTnc = $('#pre_tnc');
        let btnAccept = $('#btn_accept');
        let checkTnc = $('#tnc');
        modalTnc.modal('toggle');
        btnAccept.prop('disabled', true);
        checkPreTnc.on('change', function(e) {
            if(e.target.checked) {
                btnAccept.prop('disabled', false);
            } else {
                btnAccept.prop('disabled', true);
            }
        });

        btnAccept.on('click', function(e) {
            checkTnc.prop('checked', true);
        });

        checkTnc.on('change', function(e) {
            if(!e.target.checked) {
                modalTnc.modal('toggle');
                checkPreTnc.prop('checked', false);
                btnAccept.prop('disabled', true);
            }
        });
        /*
        var fotoInput = $("#foto");
        var imgFoto = $("#img_foto");
        var btnFileUpload = $("#btn_file_upload");
        var btnFileTrash = $("#btn_file_trash");
        var localStorageFilename = localStorage.getItem("filename");
        if(localStorageFilename) {
            var imgUrl = "{!! Storage::disk('s3')->url('members/') !!}" + "{!! date('Ymd') !!}/" + localStorageFilename;
            fotoInput.val(localStorageFilename);
            btnFileUpload.addClass('hide');
            btnFileTrash.removeClass('hide');
            imgFoto.removeClass('hide').attr("src", imgUrl);
        } else {
            btnFileUpload.removeClass('hide');
            btnFileTrash.addClass('hide');
            imgFoto.addClass('hide');
        }

        btnFileTrash.on("click", function(e) {
            e.preventDefault();
            $.ajax({
                url: "{!! route('document.uploader.destroy') !!}",
                type: "POST",
                data: {
                    _method: "DELETE",
                    _token: "{!! csrf_token() !!}",
                    filename: localStorageFilename
                },
                success: function(data) {
                    btnFileUpload.removeClass('hide');
                    btnFileTrash.addClass('hide');
                    fotoInput.val('');
                    localStorage.removeItem("filename");
                }
            });
        });
        */
    });
</script>

@endsection
