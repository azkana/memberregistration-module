@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title">
            Hi, <strong>{!! $data['full_name'] !!}</strong>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify">
            Terima kasih telah mendaftarkan diri anda.<br>
            Berikut nomor member Anda:<br>
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td class="title" style="font-weight: bold">
            {!! $data['no_member'] !!}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            <small><i>*Harap nomor member dicatat / disimpan.</i></small>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify">
            <strong>Ketentuan Member:</strong>
            <small>
                <ol style="padding-left: -20px">
                    <li>Member berlaku sampai mendapatkan pekerjaan.</li>
                    <li>Informasi lowongan kerja akan kami kirimkan melalui SMS/Telepon/Email.</li>
                    <li>Jika ada pertanyaan, silahkan hubungi nomor telepon kantor DKU 021-8242-4255 pada hari dan jam kerja.</li>
                </ol>
            </small>
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop