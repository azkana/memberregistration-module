@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title">
            Hi, <strong>{!! $data['full_name'] !!}</strong>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify; border: 0.8px solid red; padding: 10px; font-size: 11px; background-color: red; color: white">
            <p style="text-decoration: underline; text-align: center; font-size: 14px"><strong>PERHATIAN !!</strong></p>
            <ol>
                <li style="margin-left: -30px">DKU <strong>TIDAK BEKERJASAMA DENGAN PIHAK MANAPUN</strong> DALAM PROSES PEREKRUTAN.</li>
                <li style="margin-left: -30px">HARAP <strong>BERHATI-HATI</strong> DALAM MENERIMA INFORMASI YANG MENGATASNAMAKAN PT. DKU.</li>
                <li style="margin-left: -30px"><strong>PT. DKU TIDAK BERTANGGUNG JAWAB</strong> ATAS INFORMASI YANG DISEBARLUASKAN DI LUAR MANAJEMEN.</li>
                <li style="margin-left: -30px">JIKA ADA YANG INGIN DITANYAKAN HARAP MENGHUBUNGI <strong>CALL CENTER MEMBER</strong> KAMI 
                    PADA HARI DAN JAM KERJA DI <strong>NO. TELP. (021) 8242 4255</strong></li>
            </ol>
        </td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify">
            <p>Terima kasih telah mendaftarkan diri Anda.</p><br>
            <p>Secara otomatis anda terdaftar dalam membership dengan biaya registrasi yang dikirimkan ke nomor rekening:</p><br>
            <p style="font-size: 20px; font-weight: bold;">CIMB NIAGA : 800132891100</p><br>
            <p>a/n. <b>PT. Dhasnarindo Karya Utama</b></p>
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td class="paragraph">
            <table style="font-size: 14px; text-align: left">
                <tr>
                    <th style="width: 250px">Biaya Registrasi Membership</th>
                    <th style="width: 250px">Batas Waktu Pembayaran</th>
                </tr>
                <tr>
                    <td style="font-size: 20px; font-weight: bold; color: #a30f0f">
                        Rp. {!! numberFormat($data['subs_price'] + $data['unique_no']) !!},-
                    </td>
                    <td>{!! date('d-m-Y H:i', strtotime($data['payment_due'])) !!} WIB.</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify; padding: 10px">
            <p style="font-weight: bold;">Keuntungan Membership bagi Anda:</p>
            <p>1. Membership berlaku sampai mendapatkan pekerjaan.</p>
            <p>2. Mendapatkan informasi lowongan kerja perusahaan melalui SMS/Telepon/Email.</p>
            <p>3. Mendapatkan kesempatan mengikuti tes di perusahaan yang bekerjasama selama belum mendapatkan pekerjaan.</p>
            <p>4. Dapat sewaktu-waktu melakukan konfirmasi melalui nomor telepon kantor DKU 021-8242-4255 pada hari dan jam kerja.</p>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify; border: 0.5px solid grey; font-style: italic; padding: 10px">
            <p style="font-weight: bold; text-decoration: underline;">Note:</p>
            <p>- Transfer harus sesuai dengan biaya registrasi diatas, kesalahan jumlah transfer bisa menghambat proses verifikasi.</p>
            <p>- Data akan terhapus otomatis jika dalam Batas Waktu Pembayaran tidak ada konfirmasi pembayaran.</p>
            <p>- Jika sudah melakukan transfer, segera lakukan konfirmasi pembayaran dengan klik tombol di bawah ini.</p>
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'Konfirmasi Sekarang', 'link' => route('member.register.confirm', $data['pub_id'])])
            <img src="{!! route('email.read', 'id='.$data['log_id']) !!}" width="1" height="1" />
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop