@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title">
            Hi, <strong>{!! $data['full_name'] !!}</strong>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify">
            Selamat konfirmasi pembayaran anda berhasil!
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td class="title" style="text-decoration: underline">
            Detail Konfirmasi Pembayaran
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            <table style="font-size: 14px; text-align: left">
                <tr>
                    <th style="width: 200px">Bank Tujuan</th>
                    <td>
                        @if($data['bank_account_id'] == 'DKUCMB')
                            CIMB NIAGA - 800132891100
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Jumlah Transfer</th>
                    <td>Rp. {!! numberFormat($data['transfer_amount']) !!}</td>
                </tr>
                <tr>
                    <th>Tanggal Transfer</th>
                    <td>{!! date('d-m-Y', strtotime($data['transfer_date'])) !!}</td>
                </tr>
                <tr>
                    <th>Dari</th>
                    <td>{!! $data['transfer_bank_name'] !!}</td>
                </tr>
                <tr>
                    <th>No. Rekening</th>
                    <td>{!! $data['transfer_bank_account'] !!}</td>
                </tr>
                <tr>
                    <th>Nama Pemegang Rekening</th>
                    <td>{!! $data['transfer_bank_holder_name'] !!}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify">
            Silahkan menunggu, kami akan melakukan proses verifikasi.
            Nomor member akan dikirimkan melalui email anda.
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop