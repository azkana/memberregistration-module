<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
@php $style = 'text-align: center; background-color: #92D050; font-weight: bold; vertical-align: middle;'; @endphp
<table>
    <thead>
        <tr>
            <th rowspan="3" style="{!! $style !!}">NO.</th>
            <th rowspan="3" style="{!! $style !!}">NAMA<br>(Sesuai KTP)</th>
            <th rowspan="3" style="{!! $style !!}">NAMA DEPAN</th>
            <th rowspan="3" style="{!! $style !!}">NAMA TENGAH</th>
            <th rowspan="3" style="{!! $style !!}">NAMA BELAKANG</th>
            <th rowspan="3" style="{!! $style !!}">JENIS<br>KELAMIN</th>
            <th rowspan="3" style="{!! $style !!}">TEMPAT LAHIR</th>
            <th rowspan="3" style="{!! $style !!}">TANGGAL<br>LAHIR<br>DD/MM/YYYY</th>
            <th rowspan="2" colspan="2" style="{!! $style !!}">USIA</th>
            <th rowspan="3" style="{!! $style !!}">TELEPON</th>
            <th rowspan="3" style="{!! $style !!}">DEPARTEMEN</th>
            <th rowspan="3" style="{!! $style !!}">NAMA IBU KANDUNG</th>
            <th rowspan="3" style="{!! $style !!}">MASA BERLAKU KTP</th>
            <th colspan="9" style="{!! $style !!}">ALAMATKONTAKEMERGENSI(ALAMATASAL)--->(ORANGTUA/KAKAK/ADIK)</th>
            <th colspan="7" style="{!! $style !!}">ALAMAT KTP</th>
            <th colspan="7" style="{!! $style !!}">ALAMAT SEKARANG</th>
            <th rowspan="3" style="{!! $style !!}">NAMA SEKOLAH</th>
            <th rowspan="3" style="{!! $style !!}">JURUSAN</th>
            <th rowspan="3" style="{!! $style !!}">MARITAL STATUS</th>
            <th rowspan="3" style="{!! $style !!}">AGAMA</th>
            <th rowspan="3" style="{!! $style !!}">NO. KARTU KELUARGA</th>
            <th rowspan="3" style="{!! $style !!}">NO. KTP</th>
            <th rowspan="3" style="{!! $style !!}">BANK</th>
            <th rowspan="3" style="{!! $style !!}">NO. REK BANK</th>
            <th rowspan="3" style="{!! $style !!}">NO. NPWP</th>
            <th rowspan="3" style="{!! $style !!}">VENDOR</th>
            <th rowspan="3" style="{!! $style !!}">SUMBER KANDIDAT</th>
            <th rowspan="3" style="{!! $style !!}">HUBUNGAN KELUARGA DENGAN<br>PEKERJA IEI</th>
            <th rowspan="3" style="{!! $style !!}">CABANG BANK PEMBUKA</th>
            <th rowspan="3" style="{!! $style !!}">TANGGAL TES</th>
            <th rowspan="3" style="{!! $style !!}">RUTE JEMPUTAN</th>
            <th rowspan="2" style="{!! $style !!}">PERSONAL EMAIL</th>
        </tr>
        <tr>
            <th rowspan="2" style="{!! $style !!}">NAMA</th>
            <th rowspan="2" style="{!! $style !!}">HUBUNGAN</th>
            <th rowspan="2" style="{!! $style !!}">TELEPON</th>
            <th colspan="5" style="{!! $style !!}">ALAMAT EMERGENSI</th>
            <th rowspan="2" style="{!! $style !!}">ALAMAT</th>
            <th rowspan="2" style="{!! $style !!}">KELURAHAN</th>
            <th rowspan="2" style="{!! $style !!}">KECAMATAN</th>
            <th rowspan="2" style="{!! $style !!}">KOTA/KABUPATEN</th>
            <th rowspan="2" style="{!! $style !!}">PROVINSI</th>
            <th rowspan="2" style="{!! $style !!}">KODEPOS</th>
            <th rowspan="2" style="{!! $style !!}">TELEPON</th>
            <th rowspan="2" style="{!! $style !!}">ALAMAT</th>
            <th rowspan="2" style="{!! $style !!}">KELURAHAN</th>
            <th rowspan="2" style="{!! $style !!}">KECAMATAN</th>
            <th rowspan="2" style="{!! $style !!}">KOTA/KABUPATEN</th>
            <th rowspan="2" style="{!! $style !!}">PROVINSI</th>
            <th rowspan="2" style="{!! $style !!}">KODEPOS</th>
            <th rowspan="2" style="{!! $style !!}">TELEPON</th>
        </tr>
        <tr>
            <th style="{!! $style !!}">TAHUN</th>
            <th style="{!! $style !!}">BULAN</th>
            <th style="{!! $style !!}">ALAMAT</th>
            <th style="{!! $style !!}">KELURAHAN</th>
            <th style="{!! $style !!}">KECAMATAN</th>
            <th style="{!! $style !!}">KOTA/KABUPATEN</th>
            <th style="{!! $style !!}">PROVINSI</th>
            <th style="{!! $style !!}">KODEPOS</th>
            <th style="{!! $style !!}">(EMAIL AKTIF)</th>
        </tr>
    </thead>
    <tbody>
        @php $no = 1; @endphp
        @foreach ($karyawan as $row)
            <tr>
                <td style="text-align: right">{!! $no++ !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->fullname) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->firstname) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->midname) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->lastname) !!}</td>
                <td>
                    @if($row->gender == 'L')
                    LAKI-LAKI
                    @elseif($row->gender == 'P')
                    PEREMPUAN
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->tmp_lahir) !!}</td>
                <td>{!! !empty($row->tgl_lahir) ? date('d/m/Y', strtotime($row->tgl_lahir)) : null !!}</td>
                <td style="text-align: right">{!! Carbon::parse($row->tgl_lahir)->diff(Carbon::now())->format('%y') !!}</td>
                <td style="text-align: right">{!! Carbon::parse($row->tgl_lahir)->diff(Carbon::now())->format('%m') !!}</td>
                <td>{!! $row->no_hp !!}</td>
                <td></td>
                <td>{!! str_replace('&', '&amp;', $row->mother) !!}</td>
                <td>
                    @if($row->ktp_type == 1)
                    SEUMUR HIDUP
                    @elseif($row->ktp_type == 2)
                    RESI/SUKET
                    @endif
                </td>
                {{-- EMERGENCY CONTACT --}}
                <td>{!! str_replace('&', '&amp;', $row->emr_name) !!}</td>
                <td>
                    {!! getHubKelForEpsonDetail($row->emr_hubkel) !!}
                </td>
                <td>{!! $row->emr_phone !!}</td>
                <td>
                    {!! str_replace('&', '&amp;', $row->emr_alamat) !!}
                    @if(!empty($row->emr_alamat_rt) && !empty($row->emr_alamat_rw))
                    RT/RW. {!! $row->emr_alamat_rt . '/' . $row->emr_alamat_rw !!}
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->emr_kelurahan) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kec_emr->name) ? $row->rel_kec_emr->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kab_emr->name) ? $row->rel_kab_emr->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_provinsi_emr->name) ? $row->rel_provinsi_emr->name : null) !!}</td>
                <td>{!! $row->emr_kodepos !!}</td>
                {{-- ALAMAT KTP --}}
                <td>
                    {!! str_replace('&', '&amp;', $row->alamat) !!}
                    @if(!empty($row->alamat_rt) && !empty($row->alamat_rw))
                    RT/RW. {!! $row->alamat_rt . '/' . $row->alamat_rw !!}
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->kelurahan) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kec_ktp->name) ? $row->rel_kec_ktp->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kab_ktp->name) ? $row->rel_kab_ktp->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_provinsi_ktp->name) ? $row->rel_provinsi_ktp->name : null) !!}</td>
                <td>{!! $row->kodepos !!}</td>
                <td>{!! $row->phone !!}</td>
                {{-- ALAMAT DOMISILI --}}
                <td>
                    {!! str_replace('&', '&amp;', $row->alamat_dom) !!}
                    @if(!empty($row->alamat_rt_dom) && !empty($row->alamat_rw_dom))
                    RT/RW. {!! $row->alamat_rt_dom . '/' . $row->alamat_rw_dom !!}
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->kelurahan_dom) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kec_dom->name) ? $row->rel_kec_dom->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kab_dom->name) ? $row->rel_kab_dom->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_provinsi_dom->name) ? $row->rel_provinsi_dom->name : null) !!}</td>
                <td>{!! $row->kodepos_dom !!}</td>
                <td>{!! $row->phone_dom !!}</td>
                {{-- PENDIDIKAN --}}
                <td>{!! str_replace('&', '&amp;', $row->sekolah) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->jurusan) !!}</td>
                <td>{!! getMaritalStatusForEpsonDetail($row->marital) !!}</td>
                <td>{!! isset($row->religion->name) ? strtoupper($row->religion->name) : null !!}</td>
                <td>{!! $row->kk !!}</td>
                <td>{!! $row->nik !!}</td>
                <td>
                    {!! isset($row->bankAccount->name) ? strtoupper($row->bankAccount->name) : null !!}
                </td>
                <td>{!! $row->bank_account_no !!}</td>
                <td>{!! $row->npwp !!}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{!! str_replace('&', '&amp;', $row->bank_branch) !!}</td>
                <td>{!! !empty($row->tgl_tes) ? date('d/m/Y', strtotime($row->tgl_tes)) : null !!}</td>
                <td>{!! getRuteJemputanDetail($row->jemputan) !!}</td>
                <td>{!! $row->email !!}</td>
            </tr>
        @endforeach
    </tbody>
</table>