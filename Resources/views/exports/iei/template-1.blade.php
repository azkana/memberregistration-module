<table>
    <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th colspan="9">EMERGENCY CONTACT ( KONTAK DARURAT )</th>
            <th colspan="7">ALAMAT KTP</th>
            <th colspan="7">ALAMAT DOMISILI (ALAMAT SEKARANG)</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th colspan="5">PENGALAMAN KERJA 1</th>
            <th colspan="5">PENGALAMAN KERJA 2</th>
            <th></th>
            <th colspan="7">DATA VAKSIN</th>
            <th colspan="5">DOKUMEN</th>
        </tr>
        <tr>
            <th>SUBMISSION DATE</th>
            <th>NAMA LENGKAP</th>
            <th>NAMA DEPAN</th>
            <th>NAMA TENGAH</th>
            <th>NAMA BELAKANG</th>
            <th>JENIS KELAMIN</th>
            <th>TEMPAT LAHIR</th>
            <th>TANGGAL LAHIR</th>
            <th>USIA (TAHUN)</th>
            <th>USIA (BULAN)</th>
            <th>NO. HANDPHONE</th>
            <th>NAMA IBU KANDUNG</th>
            <th>MASA BERLAKU KTP</th>
            {{-- EMERGENCY CONTACT --}}
            <th>NAMA</th>
            <th>HUBUNGAN</th>
            <th>TELEPON</th>
            <th>ALAMAT</th>
            <th>KELURAHAN</th>
            <th>KECAMATAN</th>
            <th>KOTA/KABUPATEN</th>
            <th>PROVINSI</th>
            <th>KODEPOS</th>
            {{-- ALAMAT KTP --}}
            <th>ALAMAT</th>
            <th>KELURAHAN</th>
            <th>KECAMATAN</th>
            <th>KOTA/KABUPATEN</th>
            <th>PROVINSI</th>
            <th>KODEPOS</th>
            <th>TELEPON</th>
            {{-- ALAMAT DOMISILI --}}
            <th>ALAMAT</th>
            <th>KELURAHAN</th>
            <th>KECAMATAN</th>
            <th>KOTA/KABUPATEN</th>
            <th>PROVINSI</th>
            <th>KODEPOS</th>
            <th>TELEPON</th>
            {{-- PENDIDIKAN --}}
            <th>NAMA SEKOLAH</th>
            <th>JURUSAN</th>
            <th>STATUS PERKAWINAN</th>
            <th>AGAMA</th>
            <th>NO. KARTU KELUARGA</th>
            <th>NO. KTP</th>
            <th>BANK</th>
            <th>NO. REKENING</th>
            <th>NO. NPWP</th>
            <th>CABANG BANK PEMBUKA</th>
            <th>EMAIL</th>
            <th>TANGGAL TES TERTULIS</th>
            <th>RUTE JEMPUTAN</th>
            <th>NAMA AYAH ANDA</th>
            <th>KOTA TEMPAT ANDA SEKOLAH</th>
            <th>TAHUN LULUS SEKOLAH ANDA</th>
            {{-- PENGALAMAN KERJA 1 --}}
            <th>NAMA PERUSAHAAN</th>
            <th>JENIS PERUSAHAAN</th>
            <th>POSISI TERAKHIR (BAGIAN)</th>
            <th>GAJI RATA-RATA / BULAN</th>
            <th>MASA KERJA (BULAN DAN TAHUN)</th>
            {{-- PENGALAMAN KERJA 2 --}}
            <th>NAMA PERUSAHAAN</th>
            <th>JENIS PERUSAHAAN</th>
            <th>POSISI TERAKHIR (BAGIAN)</th>
            <th>GAJI RATA-RATA / BULAN</th>
            <th>MASA KERJA (BULAN DAN TAHUN)</th>
            <th>MASA BERLAKU SKCK</th>
            {{-- Data Vaksin --}}
            <th>JENIS VAKSIN</th>
            <th>VAKSIN I</th>
            <th>TGL. VAKSIN I</th>
            <th>LOKASI VAKSIN I</th>
            <th>VAKSIN II</th>
            <th>TGL. VAKSIN II</th>
            <th>LOKASI VAKSIN II</th>
            {{-- DOKUMEN --}}
            <th>KTP</th>
            <th>KK</th>
            <th>SKCK</th>
            <th>BUKU TABUNGAN</th>
            <th>SERTIFIKAT VAKSIN</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($karyawan as $row)
            <tr>
                <td>{!! date('d/m/Y H:i', strtotime($row->created_at)) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->fullname) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->firstname) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->midname) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->lastname) !!}</td>
                <td>
                    @if($row->gender == 'L')
                    LAKI-LAKI
                    @elseif($row->gender == 'P')
                    PEREMPUAN
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->tmp_lahir) !!}</td>
                <td>{!! !empty($row->tgl_lahir) ? date('d/m/Y', strtotime($row->tgl_lahir)) : null !!}</td>
                <td></td>
                <td></td>
                <td>{!! $row->no_hp !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->mother) !!}</td>
                <td>
                    @if($row->ktp_type == 1)
                    SEUMUR HIDUP
                    @elseif($row->ktp_type == 2)
                    RESI/SUKET
                    @endif
                </td>
                {{-- EMERGENCY CONTACT --}}
                <td>{!! str_replace('&', '&amp;', $row->emr_name) !!}</td>
                <td>
                    {!! getHubKelForEpsonDetail($row->emr_hubkel) !!}
                </td>
                <td>{!! $row->emr_phone !!}</td>
                <td>
                    {!! str_replace('&', '&amp;', $row->emr_alamat) !!}
                    @if(!empty($row->emr_alamat_rt) && !empty($row->emr_alamat_rw))
                    RT/RW. {!! $row->emr_alamat_rt . '/' . $row->emr_alamat_rw !!}
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->emr_kelurahan) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kec_emr->name) ? $row->rel_kec_emr->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kab_emr->name) ? $row->rel_kab_emr->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_provinsi_emr->name) ? $row->rel_provinsi_emr->name : null) !!}</td>
                <td>{!! $row->emr_kodepos !!}</td>
                {{-- ALAMAT KTP --}}
                <td>
                    {!! str_replace('&', '&amp;', $row->alamat) !!}
                    @if(!empty($row->alamat_rt) && !empty($row->alamat_rw))
                    RT/RW. {!! $row->alamat_rt . '/' . $row->alamat_rw !!}
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->kelurahan) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kec_ktp->name) ? $row->rel_kec_ktp->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kab_ktp->name) ? $row->rel_kab_ktp->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_provinsi_ktp->name) ? $row->rel_provinsi_ktp->name : null) !!}</td>
                <td>{!! $row->kodepos !!}</td>
                <td>{!! $row->phone !!}</td>
                {{-- ALAMAT DOMISILI --}}
                <td>
                    {!! str_replace('&', '&amp;', $row->alamat_dom) !!}
                    @if(!empty($row->alamat_rt_dom) && !empty($row->alamat_rw_dom))
                    RT/RW. {!! $row->alamat_rt_dom . '/' . $row->alamat_rw_dom !!}
                    @endif
                </td>
                <td>{!! str_replace('&', '&amp;', $row->kelurahan_dom) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kec_dom->name) ? $row->rel_kec_dom->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_kab_dom->name) ? $row->rel_kab_dom->name : null) !!}</td>
                <td>{!! str_replace('&', '&amp;', isset($row->rel_provinsi_dom->name) ? $row->rel_provinsi_dom->name : null) !!}</td>
                <td>{!! $row->kodepos_dom !!}</td>
                <td>{!! $row->phone_dom !!}</td>
                {{-- PENDIDIKAN --}}
                <td>{!! str_replace('&', '&amp;', $row->sekolah) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->jurusan) !!}</td>
                <td>{!! getMaritalStatusForEpsonDetail($row->marital) !!}</td>
                <td>{!! isset($row->religion->name) ? strtoupper($row->religion->name) : null !!}</td>
                <td>{!! $row->kk !!}</td>
                <td>{!! $row->nik !!}</td>
                <td>
                    {!! isset($row->bankAccount->name) ? strtoupper($row->bankAccount->name) : null !!}
                </td>
                <td>{!! $row->bank_account_no !!}</td>
                <td>{!! $row->npwp !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->bank_branch) !!}</td>
                <td>{!! $row->email !!}</td>
                <td>{!! !empty($row->tgl_tes) ? date('d/m/Y', strtotime($row->tgl_tes)) : null !!}</td>
                <td>{!! getRuteJemputanDetail($row->jemputan) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->father) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->sekolah_kab) !!}</td>
                <td>{!! $row->thn_lulus !!}</td>
                {{-- PENGALAMAN KERJA 1 --}}
                <td>{!! str_replace('&', '&amp;', $row->exp_company_1) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->exp_company_type_1) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->exp_position_1) !!}</td>
                <td>{!! $row->exp_salary_1 !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->exp_start_1) !!}</td>
                {{-- PENGALAMAN KERJA 2 --}}
                <td>{!! str_replace('&', '&amp;', $row->exp_company_2) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->exp_company_type_2) !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->exp_position_2) !!}</td>
                <td>{!! $row->exp_salary_2 !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->exp_start_2) !!}</td>
                <td>{!! !empty($row->skck_validity) ? date('d/m/Y', strtotime($row->skck_validity)) : null !!}</td>
                {{-- DATA VAKSIN --}}
                <td>{!! $row->vac_type !!}</td>
                <td>{!! $row->vac_1 == 1 ? 'sudah' : 'belum' !!}</td>
                <td>{!! !empty($row->vac_1_date) ? date('d/m/Y', strtotime($row->vac_1_date)) : null !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->vac_1_loc) !!}</td>
                <td>{!! $row->vac_2 == 1 ? 'sudah' : 'belum' !!}</td>
                <td>{!! !empty($row->vac_2_date) ? date('d/m/Y', strtotime($row->vac_2_date)) : null !!}</td>
                <td>{!! str_replace('&', '&amp;', $row->vac_2_loc) !!}</td>
                {{-- DOKUMEN --}}
                <td>
                    @if(!empty($row->file_ktp))
                        {!! Storage::disk('s3')->url('candidate/iei/'.$row->nik.'/'.$row->file_ktp) !!}
                    @endif
                </td>
                <td>
                    @if(!empty($row->file_kk))
                        {!! Storage::disk('s3')->url('candidate/iei/'.$row->nik.'/'.$row->file_kk) !!}
                    @endif
                </td>
                <td>
                    @if(!empty($row->file_skck))
                        {!! Storage::disk('s3')->url('candidate/iei/'.$row->nik.'/'.$row->file_skck) !!}
                    @endif
                </td>
                <td>
                    @if(!empty($row->file_tab))
                        {!! Storage::disk('s3')->url('candidate/iei/'.$row->nik.'/'.$row->file_tab) !!}
                    @endif
                </td>
                <td>
                    @if(!empty($row->file_vaksin))
                        {!! Storage::disk('s3')->url('candidate/iei/'.$row->nik.'/'.$row->file_vaksin) !!}
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>