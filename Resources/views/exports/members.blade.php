<table>
    <thead>
        <tr>
            <th rowspan="2">TANGGAL DAFTAR</th>
            <th rowspan="2">NO MEMBER</th>
            <th rowspan="2">STATUS</th>
            <th colspan="14">DATA PERSONAL</th>
            <th colspan="4">PENDIDIKAN</th>
            <th colspan="4">PENGALAMAN KERJA 1</th>
            <th colspan="4">PENGALAMAN KERJA 2</th>
            <th rowspan="2">SKILLS</th>
            <th colspan="2">APPROVAL</th>
        </tr>
        <tr>
            <th>NAMA LENGKAP</th>
            <th>NO KTP</th>
            <th>EMAIL</th>
            <th>NO HANDPHONE</th>
            <th>NO WA</th>
            <th>TEMPAT LAHIR</th>
            <th>TANGGAL LAHIR</th>
            <th>JENIS KELAMIN</th>
            <th>TINGGI BADAN</th>
            <th>BERAT BADAN</th>
            <th>ALAMAT</th>
            <th>KOTA/KABUPATEN</th>
            <th>PROVINSI</th>
            <th>KODEPOS</th>
            {{-- PENDIDIKAN --}}
            <th>JENJANG</th>
            <th>NAMA SEKOLAH</th>
            <th>JURUSAN</th>
            <th>TAHUN LULUS</th>
            {{-- PENGALAMAN KERJA 1 --}}
            <th>NAMA PERUSAHAAN</th>
            <th>POSISI (BAGIAN)</th>
            <th>LAMA KERJA (TAHUN)</th>
            <th>LAMA KERJA (BULAN)</th>
            {{-- PENGALAMAN KERJA 2 --}}
            <th>NAMA PERUSAHAAN</th>
            <th>POSISI (BAGIAN)</th>
            <th>LAMA KERJA (TAHUN)</th>
            <th>LAMA KERJA (BULAN)</th>
            {{-- APPROVAL --}}
            <th>TANGGAL</th>
            <th>OLEH</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
            <tr>
                <td>{!! date('d/m/Y', strtotime($row->reg_date)) !!}</td>
                <td>{!! $row->id_member !!}</td>
                <td>{!! strtoupper($row->status) !!}</td>
                <td>{!! cleanTextForExport($row->full_name) !!}</td>
                <td>{!! $row->nik !!}</td>
                <td>{!! $row->email !!}</td>
                <td>{!! cleanTextForExport($row->no_hp) !!}</td>
                <td>{!! cleanTextForExport($row->no_wa) !!}</td>
                <td>{!! cleanTextForExport($row->tmp_lahir) !!}</td>
                <td>{!! date('d/m/Y', strtotime($row->tgl_lahir)) !!}</td>
                <td>
                    @if($row->gender == 'L')
                    LAKI-LAKI
                    @elseif($row->gender == 'P')
                    PEREMPUAN
                    @endif
                </td>
                <td>{!! $row->tb !!}</td>
                <td>{!! $row->bb !!}</td>
                <td>{!! cleanTextForExport($row->alamat) !!}</td>
                <td>{!! cleanTextForExport($row->kabupaten) !!}</td>
                <td>
                    @if(isset($row->rel_provinsi->name))
                        {!! cleanTextForExport($row->rel_provinsi->name) !!}
                    @else
                        {!! cleanTextForExport($row->provinsi) !!}
                    @endif
                </td>
                <td>{!! $row->kodepos !!}</td>
                {{-- PENDIDIKAN --}}
                <td>{!! $row->jenjang !!}</td>
                <td>{!! cleanTextForExport($row->sekolah) !!}</td>
                <td>{!! cleanTextForExport($row->jurusan) !!}</td>
                <td>{!! $row->thn_lulus !!}</td>
                {{-- PENGALAMAN KERJA 1 --}}
                <td>{!! cleanTextForExport($row->exp_company_1) !!}</td>
                <td>{!! cleanTextForExport($row->exp_position_1) !!}</td>
                <td>{!! cleanTextForExport($row->exp_duration_year_1) !!}</td>
                <td>{!! cleanTextForExport($row->exp_duration_month_1) !!}</td>
                {{-- PENGALAMAN KERJA 2 --}}
                <td>{!! cleanTextForExport($row->exp_company_2) !!}</td>
                <td>{!! cleanTextForExport($row->exp_position_2) !!}</td>
                <td>{!! cleanTextForExport($row->exp_duration_year_2) !!}</td>
                <td>{!! cleanTextForExport($row->exp_duration_month_2) !!}</td>
                {{-- SKILLS --}}
                <td>{!! cleanTextForExport($row->skills) !!}</td>
                <td>{!! !empty($row->approved_date) ? date('d/m/Y', strtotime($row->approved_date)) : null !!}</td>
                <td>{!! isset($row->rel_user->name) ? $row->rel_user->name : null !!}</td>
            </tr>
        @endforeach
    </tbody>
</table>