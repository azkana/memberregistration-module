<?php
/**
 * // TODO: 
 * [x] Condition if confirmation was exist and only upload transfer attachment
 * [x] Add status expired transfer due date at index > waiting payment grid
 * [] Payment Approval change to ajax process 
 * [] Create Menu Search from all members data with ajax process
 * [] Refresh count number at Sidebar Menu
 */
namespace Modules\MemberRegistration\Http\Controllers;

use App\Models\Logs\Email;
use App\Models\Master\Bank;
use Auth;
use Excel;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Master\Provinsi;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\MemberRegistration\Entities\Member;
use Modules\MemberRegistration\Entities\MemberActive;
use Modules\MemberRegistration\Exports\MemberExport;
use Modules\MemberRegistration\Jobs\MemberRegisterJob;
use Modules\MemberRegistration\Entities\MemberNeedApproval;
use Modules\MemberRegistration\Entities\MemberSubscription;
use Modules\MemberRegistration\Entities\MemberSubscriptionConfirmation;
use Modules\MemberRegistration\Entities\MemberWaitingPayment;
use Modules\MemberRegistration\Jobs\MemberRegisterApprovalJob;

class MemberRegistrationController extends Controller
{
    protected $pageTitle = 'Members';
    protected $disk;

    public function __construct()
    {
        $this->disk = config('memberregistration.disk');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['gender']       = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];
        $params['dateType']     = array(
            'approvedate' => 'Tgl. Approve',
            'regdate' => 'Tgl. Daftar'
        );
        $params['startDate']    = Carbon::now()->subDays(0)->format('d-m-Y');
        $params['endDate']      = Carbon::now()->format('d-m-Y');
        $params['member_state'] = $memberState = $request->member_state;
        if($memberState == 'active-member') {
            $view = 'memberregistration::members.index.active-member';
        } else {
            $view = 'memberregistration::members.index';
        }
        return view($view, $params);
    }

    public function indexData(Request $request) {
        $state  = $request->member_state;
        if($request->ajax()) {
            $data = new Member();
            if(isset($request->start_date)) {
                $data = $data->whereDate('reg_date', '>=', dateFormatYmd($request->start_date));
            } else {
                $data = $data->whereDate('reg_date', '>=', Carbon::now()->subDays(3));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('reg_date', '<=', dateFormatYmd($request->end_date));
            } else {
                $data = $data->whereDate('reg_date', '<=', Carbon::now());
            }
            if(isset($state)) {
                if($state == 'active-member') {
                    $data = $data->whereHas('has_subs', function ($q) {
                        return $q->where('payment_status', 'AM');
                    });
                }
                if($state == 'member-no') {
                    $data = $data->where('mail_to_member', false)
                        ->where('id_member', '!=', '');
                }
            }
            $data = $data->latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $btnResendInfo = '';
                    if(Auth::user()->can('member-resend-info')) {
                        if(!empty($row->id_member)) {
                            $btnResendInfo = '
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#send-email-'.$row->id.'"><i class="fa fa-check-square-o"></i>Kirim Email</a>
                                </li>
                            ';
                        }
                    }
                    $btn = 
                        '
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Action</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li>
                                        <a href="#" onclick="getDetail('.$row->id.');return false;"><i class="fa fa-eye"></i>Detail</a>
                                    </li>
                                    <li>
                                        <a href="'.route("member.edit", $row->id).'"><i class="fa fa-edit"></i>Edit</a>
                                    </li>
                                    '.$btnResendInfo.'
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="modal fade" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form method="POST" action="'.route("member.destroy", $row->id).'">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input name="_token" type="hidden" value="'.csrf_token().'">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                        aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin akan menghapus data member <b>'.$row->full_name.'</b>?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="send-email-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="send-email">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form method="POST" action="'.route("member.email", $row->id).'">
                                            <input name="_token" type="hidden" value="'.csrf_token().'">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                        aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Konfirmasi Kirim Email No. Member</h4>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin akan mengirim informasi sebagai berikut?<br>
                                                <table>
                                                    <tr>
                                                        <td>No. Member</td>
                                                        <th>'.$row->id_member.'</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama Lengkap</td>
                                                        <th>'.$row->full_name.'</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <th>'.$row->email.'</th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Kirim</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ';

                    return $btn;
                })
                ->addColumn('payment_status', function($row) {
                    if(isset($row->has_subs->payment_status)) {
                        $result = getMemberStatus($row->has_subs->payment_status);
                    } else {
                        $result = 'N/A';
                    }
                    return $result;
                })
                ->addColumn('mail_to_member', function($row) {
                    if($row->mail_to_member == true) {
                        $result = 'Yes';
                    } else {
                        $result = 'No';
                    }
                    return $result;
                })
                ->addColumn('checkboxes', function($row) {
                    return null;
                })
                ->addColumn('transfer_amount', function($row) {
                    $result = isset($row->has_subs->has_confirmation->transfer_amount) ? $row->has_subs->has_confirmation->transfer_amount : null;
                    return $result;
                })
                ->addColumn('transfer_date', function($row) {
                    $result = isset($row->has_subs->has_confirmation->transfer_date) ? $row->has_subs->has_confirmation->transfer_date : null;
                    return $result;
                })
                ->addColumn('confirm_date', function($row) {
                    $result = isset($row->has_subs->has_confirmation->created_at) ? $row->has_subs->has_confirmation->created_at : null;
                    return $result;
                })
                ->rawColumns([
                    'action', 
                    'checkboxes', 
                    'payment_status', 
                    'mail_to_member',
                    'transfer_amount',
                    'transfer_date',
                    'confirm_date'
                ])
                ->make(true);
        }
    }

    public function activeMember() {
        $params['pageTitle']    = $this->pageTitle;
        $params['startDate']    = Carbon::now()->subDays(getOptions('membership_active_member_subday'))->format('d-m-Y');
        $params['endDate']      = Carbon::now()->format('d-m-Y');
        $params['dateType']     = array(
            'approvedate' => 'Tgl. Approve',
            'regdate' => 'Tgl. Daftar'
        );
        $params['gender']       = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];
        return view('memberregistration::members.index.active-member', $params);
    }

    public function activeMemberData(Request $request) {
        if($request->ajax()) {
            $data = new MemberActive();
            if(isset($request->start_date)) {
                $data = $data->whereDate('created_at', '>=', dateFormatYmd($request->start_date));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('created_at', '<=', dateFormatYmd($request->end_date));
            }
            $data = $data->latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('checkboxes', function($row) {
                    return null;
                })
                ->addColumn('approved_at', function($row) {
                    $approved_date = $row->approved_date;
                    if(empty($row->approved_date)) {
                        $approved_date = $row->created_at;
                    }
                    return $approved_date;
                })
                ->addColumn('action', function($row) {
                    $btnResendInfo = '';
                    if(Auth::user()->can('member-resend-info')) {
                        if(!empty($row->id_member)) {
                            $btnResendInfo = '
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#send-email-'.$row->id.'"><i class="fa fa-check-square-o"></i>Kirim Email</a>
                                </li>
                            ';
                        }
                    }
                    $btn = 
                        '
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Action</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li>
                                        <a href="#" onclick="getDetail('.$row->id.');return false;"><i class="fa fa-eye"></i>Detail</a>
                                    </li>
                                    '.$btnResendInfo.'
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="modal fade" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form method="POST" action="'.route("member.destroy", $row->id).'">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input name="_token" type="hidden" value="'.csrf_token().'">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                        aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin akan menghapus data member <b>'.$row->full_name.'</b>?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="send-email-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="send-email">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form method="POST" action="'.route("member.email", $row->id).'">
                                            <input name="_token" type="hidden" value="'.csrf_token().'">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                        aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Konfirmasi Kirim Email No. Member</h4>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin akan mengirim informasi sebagai berikut?<br>
                                                <table>
                                                    <tr>
                                                        <td>No. Member</td>
                                                        <th>'.$row->id_member.'</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama Lengkap</td>
                                                        <th>'.$row->full_name.'</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <th>'.$row->email.'</th>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Kirim</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ';

                    return $btn;
                })
                ->rawColumns([
                    'action', 'checkboxes', 'approved_at'
                ])
                ->make(true);
        }
    }

    public function needApproval() {
        $params['pageTitle']    = $this->pageTitle;
        $params['startDate']    = Carbon::now()->subDays(getOptions('membership_need_approval_subday'))->format('d-m-Y');
        $params['endDate']      = Carbon::now()->format('d-m-Y');
        return view('memberregistration::members.index.need-approval', $params);
    }

    public function needApprovalData(Request $request) {
        if($request->ajax()) {
            $data = new MemberNeedApproval();
            if(isset($request->start_date)) {
                $data = $data->whereDate('created_at', '>=', dateFormatYmd($request->start_date));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('created_at', '<=', dateFormatYmd($request->end_date));
            }
            $data = $data->latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $btn = 
                        '
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Action</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li>
                                        <a href="#" onclick="getDetail('.$row->id.');return false;"><i class="fa fa-eye"></i>Detail</a>
                                    </li>
                                    <li class="">
                                        <a href="#" onclick="paymentApprovalConfirmation('.$row->id.');return false;"><i class="fa fa-check-square-o"></i>Approve Payment</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="modal fade" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form method="POST" action="'.route("member.destroy", $row->id).'">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input name="_token" type="hidden" value="'.csrf_token().'">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                        aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin akan menghapus data member <b>'.$row->full_name.'</b>?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ';

                    return $btn;
                })
                ->rawColumns([
                    'action'
                ])
                ->make(true);
        }
    }

    public function waitingPayment() {
        $params['pageTitle']    = $this->pageTitle;
        $params['startDate']    = Carbon::now()->subDays(getOptions('membership_waiting_payment_subday'))->format('d-m-Y');
        $params['endDate']      = Carbon::now()->format('d-m-Y');
        return view('memberregistration::members.index.waiting-payment', $params);
    }

    public function waitingPaymentData(Request $request) {
        if($request->ajax()) {
            $data = new MemberWaitingPayment();
            if(isset($request->start_date)) {
                $data = $data->whereDate('reg_date', '>=', dateFormatYmd($request->start_date));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('reg_date', '<=', dateFormatYmd($request->end_date));
            }
            $data = $data->orderBy('reg_date', 'DESC')->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('expired', function($row) {
                    if($row->payment_due_date < Carbon::now()) {
                        $status = '<span class="text-danger">Expired</span>';
                    } else {
                        $status = '<span class="text-success">Waiting</span>';
                    }
                    return $status;
                })
                ->addColumn('action', function($row) {
                    $btn = 
                        '
                            <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Action</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li>
                                        <a href="#" onclick="getDetail('.$row->id.');return false;"><i class="fa fa-eye"></i>Detail</a>
                                    </li>
                                    <li class="">
                                        <a href="#" onclick="editEmail('.$row->id.');return false;"><i class="fa fa-envelope"></i>Edit Email</a>
                                    </li>
                                    <li class="">
                                        <a href="#" onclick="resendEmail('.$row->id.');return false;"><i class="fa fa-send"></i>Resend Email Registration</a>
                                    </li>
                                    <li class="">
                                        <a href="#" onclick="uploadBuktiTransfer('.$row->id.');return false;"><i class="fa fa-cloud-upload"></i>Upload Bukti Transfer</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="modal fade" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form method="POST" action="'.route("member.destroy", $row->id).'">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input name="_token" type="hidden" value="'.csrf_token().'">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                        aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin akan menghapus data member <b>'.$row->full_name.'</b>?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ';

                    return $btn;
                })
                ->rawColumns([
                    'action', 'expired'
                ])
                ->make(true);
        }
    }

    public function editEmailForm(Request $request)
    {
        $id = $request->id;
        $params['data'] = MemberWaitingPayment::findOrFail($id);
        return view('memberregistration::members.modals.edit-email', $params);
    }

    public function editEmailSave(Request $request, $id)
    {
        try {
            $data   = Member::findOrFail($id);
            $email  = $request->new_email;
            $data->update([
                'email' => $email
            ]);
            return array(
                'message' => 'Email ' . $email . ' berhasil disimpan'
            );
        } catch (\Exception $e) {
            return array(
                'message' => 'Email ' . $email . ' gagal disimpan'
            );
        }
    }

    public function resendEmailRegistrationForm(Request $request)
    {
        $id = $request->id;
        $params['data'] = Member::findOrFail($id);
        return view('memberregistration::members.modals.resend-email-registration', $params);
    }

    public function resendEmailRegistrationSave(Request $request, $id)
    {
        try {
            $id = $request->id;
            $data = Member::findOrFail($id);
            $dataEmail['pub_id']        = $data->pub_id;
            $dataEmail['full_name']     = $data->full_name;
            $dataEmail['email']         = $data->email;
            $dataEmail['subs_price']    = $data->has_subs->subs_price;
            $dataEmail['unique_no']     = $data->has_subs->unique_no;
            $dataEmail['payment_due']   = $data->has_subs->payment_due_date;
            $dataEmail['subject']       = 'Jobsdku - Pendaftaran Member Berhasil';

            $logEmail = Email::create([
                'email_from'    => config('mail.from.address'),
                'email_to'      => $dataEmail['email'],
                'email_subject' => $dataEmail['subject'],
                'email_status'  => 'Q',
                'module'        => 'memberregistration',
                'queue_at'      => date('Y-m-d H:i:s')
            ]);
            $dataEmail['log_id']  = $logEmail->id;

            MemberRegisterJob::dispatch($dataEmail)
                ->delay(now()->addMinutes(1));
            
            return array(
                'message' => 'Pengiriman Email ke ' . $data->email . ' sedang dalam antrian.'
            );
        } catch (\Exception $e) {
            return array(
                'message' => 'Pengiriman Email ke ' . $data->email . ' gagal.'
            );
        }
    }

    public function uploadBuktiTransferForm(Request $request)
    {
        $id = $request->id;
        $params['data']     = Member::findOrFail($id);
        $params['bankAccount']  = [
            'DKUCMB' => 'CIMB NIAGA - 800132891100'
        ];
        $params['bank']     = Bank::pluck('name', 'code');
        return view('memberregistration::members.modals.upload-bukti-transfer', $params);
    }

    public function uploadBuktiTransferSave(Request $request, $id)
    {
        try {
            $member     = Member::findOrFail($id);
            $data['member_subscription_id']     = $member->has_subs->id;
            $data['bank_account_id']            = $request->bank_account_id;
            $data['transfer_amount']            = cleanNominal($request->transfer_amount);
            $data['transfer_date']              = Carbon::createFromFormat('d-m-Y', $request->transfer_date)->format('Y-m-d');
            $data['transfer_bank_id']           = $request->transfer_bank_id;
            $data['transfer_bank_name']         = $request->transfer_bank_name;
            $data['transfer_bank_account']      = trim($request->transfer_bank_account);
            $data['transfer_bank_holder_name']  = strtoupper($request->transfer_bank_holder_name);
            if($request->hasFile('transfer_attachment')) {
                $fileExt    = strtolower($request->file('transfer_attachment')->getClientOriginalExtension());
                $fileName   = 'transfer_attachment-' . md5($member->has_subs->id.time()) . '.'  . $fileExt;
                $data['transfer_attachment']    = $fileName;
                if($this->disk == 's3') {
                    Storage::disk($this->disk)->put(
                        'members/' . $member->nik . '/' . $fileName,
                        fopen($request->file('transfer_attachment'), 'r+'),
                        'public'
                    );
                }
            }
            if(!isset($member->has_subs->has_confirmation)) {
                MemberSubscriptionConfirmation::create($data);
            } else {
                MemberSubscriptionConfirmation::where('member_subscription_id', $member->has_subs->id)
                    ->update($data);
            }
            MemberSubscription::findOrFail($member->has_subs->id)
                ->update([
                    'payment_status' => 'CP'
                ]);

            return array(
                'message' => 'Bukti transaksi ' . $member->full_name . ' berhasil disimpan.'
            );
        } catch (\Exception $e) {
            return array(
                'message' => 'Bukti transaksi ' . $member->full_name . ' gagal disimpan.'
            );
        }
    }

    public function uploadBuktiTransferDeleteForm(Request $request)
    {
        $id = $request->id;
        $params['data'] = Member::findOrFail($id);
        return view('memberregistration::members.modals.delete-file', $params);
    }

    public function uploadBuktiTransferDeleteSave(Request $request)
    {
        try {
            $id = $request->id;
            $member = Member::findOrFail($id);
            if(isset($member->has_subs->has_confirmation->transfer_attachment)) {
                Storage::disk($this->disk)->delete('members/' . $member->nik . '/' . $member->has_subs->has_confirmation->transfer_attachment);
                MemberSubscriptionConfirmation::where('member_subscription_id', $member->has_subs->id)
                    ->update([
                        'transfer_attachment' => null
                    ]);
            }
            return array(
                'message' => 'Bukti transaksi ' . $member->full_name . ' berhasil dihapus.'
            );
        } catch (\Exception $e) {
            return array(
                'message' => 'Bukti transaksi ' . $member->full_name . ' gagal dihapus.'
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Create';
        $params['skills']       = [
            'Forklift',
            'Ms. Office (Excel dan Word)',
            'Maintenance Elektrik',
            'Maintenance Mesin',
            'Bahasa Inggris'
        ];
        $params['provinsi'] = Provinsi::orderBy('name')->pluck('name', 'code');
        $params['jenjang']  = $this->getJenjang();
        return view('memberregistration::members.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['pub_id']     = base64_encode($request->nik.date('Y-m-d H:i:s'));
            $data['id_member']  = $memberID = strtoupper($request->id_member);
            $data['full_name']  = strtoupper($request->full_name);
            $data['nik']        = $request->nik;
            $data['email']      = strtolower($request->email);
            $data['no_hp']      = $request->no_hp;
            $data['tmp_lahir']  = strtoupper($request->tmp_lahir);
            $data['tgl_lahir']  = Carbon::createFromFormat('d/m/Y', $request->tgl_lahir)->format('Y-m-d');
            $data['gender']     = $request->gender;
            $data['tb']         = $request->tb;
            $data['bb']         = $request->bb;
            $data['alamat']     = strtoupper($request->alamat);
            $data['kabupaten']  = strtoupper($request->kabupaten);
            $data['provinsi']   = strtoupper($request->provinsi);
            $data['kodepos']    = $request->kodepos;
            $data['jenjang']    = strtoupper($request->jenjang);
            $data['sekolah']    = strtoupper($request->sekolah);
            $data['jurusan']    = strtoupper($request->jurusan);
            $data['thn_lulus']  = $request->thn_lulus;
            $data['exp_company_1']          = strtoupper($request->exp_company_1);
            $data['exp_position_1']         = strtoupper($request->exp_position_1);
            $data['exp_duration_year_1']    = $request->exp_duration_year_1;
            $data['exp_duration_month_1']   = $request->exp_duration_month_1;
            $data['exp_company_2']          = strtoupper($request->exp_company_2);
            $data['exp_position_2']         = strtoupper($request->exp_position_2);
            $data['exp_duration_year_2']    = $request->exp_duration_year_2;
            $data['exp_duration_month_2']   = $request->exp_duration_month_2;
            $data['reg_date']   = Carbon::today()->format('Y-m-d');
            $data['status']     = 'Pemanggilan';
            
            if(isset($request->skill)) {
                $skills = [];
                
                if($request->skill) {
                    $skills         = implode(',', $request->skill);
                }
                if(!empty($request->skill_lain)) {
                    $skillLain  = explode(',', $request->skill_lain);
                    $allSkills  = array_merge(explode(',',$skills), $skillLain);
                    $skills     = implode(',', $allSkills);
                }
                $data['skills'] = $skills;
            }

            if($request->hasFile('pas_foto')) {
                $fileExt    = strtolower($request->file('pas_foto')->getClientOriginalExtension());
                $fileName   = 'pas-foto-' . md5($request->nik.time()) . '.' . $fileExt;
                $data['pas_foto']   = $fileName;
                Storage::disk('public')->putFileAs(
                    'members/' . $request->nik,
                    $request->file('pas_foto'),
                    $fileName
                );
            }

            if($request->hasFile('ktp')) {
                $fileExt    = strtolower($request->file('ktp')->getClientOriginalExtension());
                $fileName   = 'ktp-' . md5($request->nik.time()) . '.'  . $fileExt;
                $data['id_card']= $fileName;
                Storage::disk('public')->putFileAs(
                    'members/' . $request->nik,
                    $request->file('ktp'),
                    $fileName
                );
            }

            Member::create($data);
            return redirect(route('member.index'))
				->with('success_message', trans('message.success_save'));
        } catch(\Exception $e) {
            return redirect(route('member.index'))
				->with('error_message', trans('message.error_save'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Detail';
        $params['data']         = Member::findOrFail($id);
        return view('memberregistration::members.show', $params);
    }

    public function showModal(Request $request)
    {
        $id = $request->id;
        $params['data']         = Member::findOrFail($id);
        return view('memberregistration::members.show-modal', $params);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Edit';
        $params['data']         = $data = Member::findOrFail($id);
        $params['selectedSkills']= explode(',', $data->skills);
        $params['skills']       = [
            'Forklift',
            'Ms. Office (Excel dan Word)',
            'Maintenance Elektrik',
            'Maintenance Mesin',
            'Bahasa Inggris'
        ];
        $params['provinsi'] = Provinsi::orderBy('name')->pluck('name', 'code');
        $params['jenjang']  = $this->getJenjang();
        $params['status']       = [
            'Pemanggilan' => 'Pemanggilan',
            'Seleksi Tes' => 'Seleksi Tes',
            'Sudah Bekerja' => 'Sudah Bekerja'
        ];
        return view('memberregistration::members.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $member = Member::findOrFail($id);
            $data['id_member']  = $memberID = strtoupper($request->id_member);
            $data['full_name']  = strtoupper($request->full_name);
            $data['nik']        = $request->nik;
            $data['email']      = $request->email;
            $data['no_hp']      = $request->no_hp;
            $data['tmp_lahir']  = strtoupper($request->tmp_lahir);
            $data['tgl_lahir']  = Carbon::createFromFormat('d/m/Y', $request->tgl_lahir)->format('Y-m-d');
            $data['gender']     = $request->gender;
            $data['tb']         = $request->tb;
            $data['bb']         = $request->bb;
            $data['alamat']     = strtoupper($request->alamat);
            $data['kabupaten']  = strtoupper($request->kabupaten);
            $data['provinsi']   = strtoupper($request->provinsi);
            $data['kodepos']    = $request->kodepos;
            $data['jenjang']    = strtoupper($request->jenjang);
            $data['sekolah']    = strtoupper($request->sekolah);
            $data['jurusan']    = strtoupper($request->jurusan);
            $data['thn_lulus']  = $request->thn_lulus;
            $data['exp_company_1']          = strtoupper($request->exp_company_1);
            $data['exp_position_1']         = strtoupper($request->exp_position_1);
            $data['exp_duration_year_1']    = $request->exp_duration_year_1;
            $data['exp_duration_month_1']   = $request->exp_duration_month_1;
            $data['exp_company_2']          = strtoupper($request->exp_company_2);
            $data['exp_position_2']         = strtoupper($request->exp_position_2);
            $data['exp_duration_year_2']    = $request->exp_duration_year_2;
            $data['exp_duration_month_2']   = $request->exp_duration_month_2;
            $data['status']     = $request->status;
            
            if(isset($request->skill)) {
                $skills = [];
                
                if($request->skill) {
                    $skills         = implode(',', $request->skill);
                }
                if(!empty($request->skill_lain)) {
                    $skillLain  = explode(',', $request->skill_lain);
                    $allSkills  = array_merge(explode(',',$skills), $skillLain);
                    $skills     = implode(',', $allSkills);
                }
                $data['skills'] = $skills;
            }

            if($request->hasFile('pas_foto')) {
                $fileExt    = strtolower($request->file('pas_foto')->getClientOriginalExtension());
                $fileName   = 'pas-foto-' . md5($memberID.time()) . '.' . $fileExt;
                $data['pas_foto']   = $fileName;
                Storage::disk('public')->putFileAs(
                    'members/' . $memberID,
                    $request->file('pas_foto'),
                    $fileName
                );
            }

            if($request->hasFile('ktp')) {
                $fileExt    = strtolower($request->file('ktp')->getClientOriginalExtension());
                $fileName   = 'ktp-' . md5($memberID.time()) . '.'  . $fileExt;
                $data['id_card']= $fileName;
                Storage::disk('public')->putFileAs(
                    'members/' . $memberID,
                    $request->file('ktp'),
                    $fileName
                );
            }

            $member->update($data);
            return redirect(route('member.index'))
				->with('success_message', trans('message.success_edit'));
        } catch(\Exception $e) {
            return redirect(route('member.index'))
				->with('error_message', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $data = Member::findOrFail($id);
            if(!empty($data->pas_foto) || !empty($data->ktp)) {
                Storage::disk($this->disk)->delete('members/'.$data->nik.'/'.$data->pas_foto);
                Storage::disk($this->disk)->delete('members/'.$data->nik.'/'.$data->id_card);
            }
            if(isset($data->has_subs->has_confirmation)) {
                Storage::disk($this->disk)->delete('members/'.$data->nik.'/'.$data->transfer_attachment);
            }
            $data->delete();
            return redirect(route('member.index'))
				->with('success_message', trans('message.success_delete'));
        } catch(\Exception $e) {
            return redirect(route('member.index'))
				->with('error_message', trans('message.error_delete'));
        }
    }

    public function destroyBulk(Request $request) 
    {
        try {
            $data = $request->id;
            foreach($data as $id) {
                $member = Member::findOrFail($id);
                if(!empty($member->pas_foto) || !empty($member->ktp)) {
                    Storage::disk($this->disk)->delete('members/'.$member->nik.'/'.$member->pas_foto);
                    Storage::disk($this->disk)->delete('members/'.$member->nik.'/'.$member->id_card);
                }

                if(isset($data->has_subs->has_confirmation)) {
                    Storage::disk($this->disk)->delete('members/'.$data->nik.'/'.$data->transfer_attachment);
                }

                $member->delete();
            }
            return response()->json([
                'message' => 'Data deleted successfully!'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'message' => $e
            ]);
        }
    }

    /**
     * Print PDF
     * @return Response
     */
    public function print($id)
    {
        $params['data'] = Member::findOrFail($id);
        return view('memberregistration::members.print', $params);
    }

    public function printPdf($id)
    {
        setLocale(LC_TIME, 'id_ID');
        $params['data'] = $data = Member::findOrFail($id);
        $params['date'] = Carbon::parse($data->reg_date);
        $pdf = \PDF::loadView('memberregistration::members.pdf', $params)
            ->setPaper('a4', 'portrait');
        // return $pdf->stream();
        return $pdf->download('member-'.$data->nik.'.pdf');
    }

    public function exports(Request $request)
    {
        $data['date_type']  = $request->date_type;
        $data['date_from']  = date('Y-m-d', strtotime($request->tgl_from));
        $data['date_to']    = date('Y-m-d', strtotime($request->tgl_to));
        $data['gender']     = $request->gender;
        $data['state']      = $request->export_state;
        if(isset($request->export_state)) {
            $filename = $request->export_state.'_'.date('Y-m-d').'.xlsx';
        } else {
            $filename = 'members_'.date('Y-m-d').'.xlsx';
        }
        $fileType = $request->file_type;
        if($fileType == 'xlsx') {
            return $this->exportExcel($data, $filename);
        }
    }

    public function exportExcel($data, $filename) {
        return Excel::download(new MemberExport($data), $filename);
    }

    public function paymentApprovalConfirmation(Request $request) {
        $id = $request->id;
        $params['data']         = Member::findOrFail($id);
        return view('memberregistration::members.modals.payment-approval', $params);
    }

    public function paymentApprovalConfirmationSave(Request $request, $id) {
        try {
            $memberNo   = $request->member_no;
            $member     = Member::findOrFail($id);
            
            $member->update(['id_member' => $memberNo]);
            $member->has_subs->update(['payment_status' => 'AM']);

            $data['full_name']  = $member->full_name;
            $data['email']      = $member->email;
            $data['no_member']  = $member->id_member;
            
            $sendMail = MemberRegisterApprovalJob::dispatch($data)
                ->delay(now()->addMinutes(1));
            if(!$sendMail) {
                return redirect(route('member.index', 'member_state=need-approval'))
				    ->with('error', 'Email tidak berhasil diirim');
            }
            $member->update([
                'mail_to_member' => true,
                'is_active' => true,
                'approved_date' => dateFormatYmdHis($request->approve_date . ' ' . date('H:i:s')),
                'approved_by' => Auth::user()->id
            ]);
            return array(
                'message' => 'Member ' . $member->full_name . ' berhasil disetujui.'
            ); 
        } catch (\Exception $e) {
            return array(
                'message' => 'Member ' . $member->full_name . ' gagal disetujui.'
            );
        }
    }

    private function getJenjang() {
        $data = [
            'SMA' => 'SMA',
            'SMK' => 'SMK',
            'D3' => 'D3',
            'S1' => 'S1'
        ];
        return $data;
    }

    public function sendEmailMember($id)
    {
        try {
            $member = Member::findOrFail($id);
            $data['full_name']  = $member->full_name;
            $data['email']      = $member->email;
            $data['no_member']  = $member->id_member;

            $sendMail = MemberRegisterApprovalJob::dispatch($data)
                ->delay(now()->addMinutes(4));
            
            if(!$sendMail) {
                return redirect(route('member.active-member'))
                    ->with('error', 'Email tidak berhasil diirim');
            }

            $member->update(['mail_to_member' => true]);

            return redirect(route('member.active-member'))
				->with('success', 'Email berhasil dikirim');
        } catch (\Exception $e) {
            return redirect(route('member.active-member'))
				->with('error', trans('message.error_save'));
        }
    }

    public function checkNoMember(Request $request)
    {
        $noMember = $request->member_no;
        $memberNo   = Member::where('id_member', $noMember)->get();
        if($memberNo->count() > 0) {
            return "false";
        } else {
            return "true";
        }
    }
}