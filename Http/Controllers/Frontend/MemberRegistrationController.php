<?php

namespace Modules\MemberRegistration\Http\Controllers\Frontend;

use Image;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use App\Models\Logs\Email;
use App\Models\Master\Bank;
use Illuminate\Http\Request;
use App\Models\Master\Provinsi;
use App\Models\Configs\Sequence;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Modules\MemberRegistration\Entities\Member;
use Modules\MemberRegistration\Jobs\MemberRegisterJob;
use Modules\MemberRegistration\Entities\MemberSubscription;
use Modules\MemberRegistration\Entities\MemberSubscriptionConfirmation;
use Modules\MemberRegistration\Notifications\MemberRegisterNotification;

class MemberRegistrationController extends Controller
{
    protected $disk;

    public function __construct()
    {
        $this->disk = config('memberregistration.disk');
    }

    public function registrationForm() {
        $title      = 'Member Registration Form';
        $toDay      = Carbon::now();
        $timeStart  = Carbon::createFromTimeString(getOptions('membership_form_available_time_start'));
        $timeEnd    = Carbon::createFromTimeString(getOptions('membership_form_available_time_end'));
        $messageDefault = 'Formulir pendaftaran member bisa diakses Senin-Jumat Jam '.
                            getOptions('membership_form_available_time_start').' - '.
                            getOptions('membership_form_available_time_end').' WIB.';
        if(getOptions('membership_form_published') == true) {
            if(!empty(getOptions('membership_form_max_day'))) {
                $memberCount = Member::where('reg_date', date('Y-m-d'))->get();
                if($memberCount->count() >= getOptions('membership_form_max_day')) {
                    $message = '<p>Mohon maaf kuota pendaftaran online harian hanya ' . getOptions('membership_form_max_day') . ' member / hari. Silahkan kembali lagi nanti.<br>'.
                                $messageDefault;
                    return $this->showMemberForm($title, $message);
                } else {
                    if(getOptions('membership_form_available') == 'everyday') {
                        if(getOptions('membership_form_available_time') == 'allday') {
                            return $this->showMemberForm($title);
                        } elseif(getOptions('membership_form_available_time') == 'timerange') {
                            if($toDay >= $timeStart && $toDay <= $timeEnd) {
                                return $this->showMemberForm($title);
                            } else {
                                return $this->showMemberForm($title, $messageDefault);
                            }
                        }
                    } elseif(getOptions('membership_form_available') == 'workingday') {
                        if($toDay->isWeekday()) {
                            if(getOptions('membership_form_available_time') == 'allday') {
                                return $this->showMemberForm($title);
                            } elseif(getOptions('membership_form_available_time') == 'timerange') {
                                if($toDay >= $timeStart && $toDay <= $timeEnd) {
                                    return $this->showMemberForm($title);
                                } else {
                                    return $this->showMemberForm($title, $messageDefault);
                                }
                            }
                        } else {
                            return $this->showMemberForm($title, $messageDefault);
                        }
                    }
                }
            }
        } else {
            $params['title']    = $title;
            $params['messages'] = getOptions('membership_form_unpublished_note');
            return view('memberregistration::frontend.formulir.shared.form-unpublished', $params);
        }
    }
    public function registrationFormOld() {
        $params['title']    = 'Member Registration Form';
        return view('memberregistration::frontend.formulir.member.register-old', $params);
    }

    public function registrationFormDemo() {
        $title  = 'Member Registration Form';
        return $this->showMemberForm($title);
    }

    private function showMemberForm($title = null, $message = null)
    {
        $params['title']    = $title;
        $params['idInput']  = 1;
        $params['idLabel']  = 1;
        $params['skills']   = [
            'Forklift',
            'Ms. Office (Excel dan Word)',
            'Maintenance Elektrik',
            'Maintenance Mesin',
            'Bahasa Inggris'
        ];
        $params['tahun_kerja']  = array(
            1 => '1 Tahun',
            2 => '2 Tahun',
            3 => '3 Tahun',
            4 => '4 Tahun',
            5 => '5 Tahun',
            6 => '6 Tahun',
            7 => '7 Tahun',
            8 => '8 Tahun',
            9 => '9 Tahun',
            10 => '10 Tahun',
        );
        $params['bulan_kerja']  = array(
            1 => '1 Bulan',
            2 => '2 Bulan',
            3 => '3 Bulan',
            4 => '4 Bulan',
            5 => '5 Bulan',
            6 => '6 Bulan',
            7 => '7 Bulan',
            8 => '8 Bulan',
            9 => '9 Bulan',
            10 => '10 Bulan',
            11 => '11 Bulan',
        );
        $params['provinsi'] = Provinsi::orderBy('name')->pluck('name', 'code');
        $params['jenjang']  = $this->getJenjang();
        $params['messages'] = $message;
        return view('memberregistration::frontend.formulir.member.register', $params);
    }

    public function registrationSave(Request $request) {
        try {

            $memberCount = Member::where('reg_date', date('Y-m-d'))->get();
            if(!empty(getOptions('membership_form_max_day'))) {
                if($memberCount->count() >= getOptions('membership_form_max_day')) {
                    $message = '<p>Mohon maaf kuota pendaftaran online harian hanya '.numberFormat(getOptions('membership_form_max_day')).' member / hari. Silahkan dicoba esok hari.<br>
                        Jam pendaftaran online :<br>
                        Senin - Jum\'at jam 08.00 - 16.00</p>';
                    return redirect(route('member.register.form'))
                        ->with('error_message', $message);
                }
            }

            $data['pub_id']     = $pub_id = base64_encode($request->nik.date('Y-m-d H:i:s'));
            $data['full_name']  = strtoupper($request->full_name);
            $data['nik']        = $request->nik;
            $data['email']      = strtolower($request->email);
            $data['no_hp']      = $request->no_hp;
            $data['no_wa']      = $request->no_wa;
            $data['tmp_lahir']  = strtoupper($request->tmp_lahir);
            $data['tgl_lahir']  = Carbon::createFromFormat('d/m/Y', $request->tgl_lahir)->format('Y-m-d');
            $data['gender']     = $request->gender;
            $data['alamat']     = strtoupper($request->alamat);
            $data['kabupaten']  = strtoupper($request->kabupaten);
            $data['provinsi']   = $request->provinsi;
            $data['kodepos']    = $request->kodepos;
            $data['jenjang']    = strtoupper($request->jenjang);
            $data['sekolah']    = strtoupper($request->sekolah);
            $data['jurusan']    = strtoupper($request->jurusan);
            $data['thn_lulus']  = $request->thn_lulus;
            $data['exp_company_1']          = strtoupper($request->exp_company_1);
            $data['exp_position_1']         = strtoupper($request->exp_position_1);
            $data['exp_duration_year_1']    = $request->exp_duration_year_1;
            $data['exp_duration_month_1']   = $request->exp_duration_month_1;
            $data['exp_company_2']          = strtoupper($request->exp_company_2);
            $data['exp_position_2']         = strtoupper($request->exp_position_2);
            $data['exp_duration_year_2']    = $request->exp_duration_year_2;
            $data['exp_duration_month_2']   = $request->exp_duration_month_2;
            $data['reg_date']   = Carbon::today()->format('Y-m-d');
            $data['status']     = 'Pemanggilan';
            $data['tb']         = $request->tb;
            $data['bb']         = $request->bb;
            
            if(isset($request->skill)) {

                $skills = [];
                if($request->skill) {
                    $skills         = implode(',', $request->skill);
                }
                if(!empty($request->skill_lain)) {
                    $skillLain  = explode(',', $request->skill_lain);
                    $allSkills  = array_merge(explode(',',$skills), $skillLain);
                    $skills     = implode(',', $allSkills);
                }
                $data['skills'] = $skills;

            }

            if($request->hasFile('pas_foto')) {
                ini_set('memory_limit','256M');
                $fileExt    = strtolower($request->file('pas_foto')->getClientOriginalExtension());
                $fileName   = 'pas-foto-' . Uuid::uuid4() . '.' . $fileExt;
                $data['pas_foto']   = $fileName;
                $fileFoto = Image::make($request->file('pas_foto'))
                    ->resize(1000, 1000, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                Storage::disk($this->disk)->put(
                    'members/' . $request->nik . '/' . $fileName,
                    $fileFoto->stream(),
                    'public'
                );
            }
            /*
            if($request->hasFile('ktp')) {
                ini_set('memory_limit','256M');
                $fileExt    = strtolower($request->file('ktp')->getClientOriginalExtension());
                $fileName   = 'ktp-' . Uuid::uuid4() . '.'  . $fileExt;
                $data['id_card']= $fileName;
                $fileKtp = Image::make($request->file('ktp'))
                    ->resize(800, 800, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                Storage::disk($this->disk)->put(
                    'members/' . $request->nik . '/' . $fileName,
                    $fileKtp->stream(),
                    'public'
                );
            }
            */
            $nik    = Member::where('nik', $request->nik)->get();
            $email  = Member::where('email', $request->email)->get();

            if(count($nik) > 0) {
                return redirect(route('member.register.form'))
				    ->with('error_message', 'NIK (No. KTP) ' . $request->nik . ' sudah terdaftar.');
            } elseif (count($email) > 0) {
                return redirect(route('member.register.form'))
				    ->with('error_message', 'alamat email ' . $request->email . ' sudah terdaftar.');
            } else {

                $member = Member::create($data);

                if(!empty(getOptions('membership_subscription')) OR getOptions('membership_subscription') > 0) {
                    
                    if(getOptions('membership_unique') == 1) {
                        if(getOptions('membership_unique_type') == 'random') {
                            $uniqueNoLength         = getOptions('membership_unique_digit');
                            $dataSubs['unique_no']  = generateUniqueNumber($uniqueNoLength);
                        }
                        if(getOptions('membership_unique_type') == 'sequence') {
                            $seqIsExist = Sequence::where('module_id', 'memberregistration')
                                ->where('menu_id', 'member')->first();
                            if(isset($seqIsExist)) {
                                $dataSubs['unique_no']  = $nextVal = $seqIsExist->last_value + 1;
                                $seqIsExist->update(['last_val' => $nextVal]);
                            }
                        }
                    }
        
                    if(!empty(getOptions('membership_payment_due'))) {
                        $paymentDueDate = Carbon::now()->addDays(getOptions('membership_payment_due'));
                    } else {
                        $paymentDueDate = Carbon::now()->addDays(1);
                    }
    
                    $dataSubs['pub_id']             = $member->pub_id;
                    $dataSubs['subs_price']         = !empty($request->subs_price) ? $request->subs_price : getOptions('membership_subscription');
                    $dataSubs['payment_due_date']   = $paymentDueDate;
                    $dataSubs['payment_status']     = 'WP';
                    MemberSubscription::create($dataSubs);

                }

                $dataEmail['pub_id']        = $pub_id;
                $dataEmail['full_name']     = $data['full_name'];
                $dataEmail['email']         = $data['email'];
                $dataEmail['subs_price']    = $dataSubs['subs_price'];
                $dataEmail['unique_no']     = $dataSubs['unique_no'];
                $dataEmail['payment_due']   = $dataSubs['payment_due_date'];
                $dataEmail['subject']       = 'Jobsdku - Pendaftaran Member Berhasil';

                $logEmail = Email::create([
                    'email_from'    => config('mail.from.address'),
                    'email_to'      => $data['email'],
                    'email_subject' => $dataEmail['subject'],
                    'email_status'  => 'Q',
                    'module'        => 'memberregistration',
                    'queue_at'      => date('Y-m-d H:i:s')
                ]);
                $dataEmail['log_id']  = $logEmail->id;

                MemberRegisterJob::dispatch($dataEmail)
                    ->delay(now()->addMinutes(1));

                $member->notify(new MemberRegisterNotification());
            }

            return redirect(route('member.register.thanks', $pub_id));
        } catch(\Exception $e) {
            Log::error($e);
            return redirect(route('member.register.index'))
				->with('error_message', trans('message.error_save'));
        }
    }

    public function registrationThanks($pub_id) {
        $params['title']    = 'Selamat pendaftaran berhasil!';
        $params['data']     = Member::select('full_name')->where('pub_id', $pub_id)->first();
        return view('memberregistration::frontend.formulir.member.thanks', $params);
    }

    /* Payment Confirmation */
    public function paymentConfirmation($id) {
        $params['title']    = 'Member Payment Confirmation';
        $params['data']     = Member::where('pub_id', $id)->firstOrFail();
        $params['bankAccount']  = [
            'DKUCMB' => 'CIMB NIAGA - 800132891100'
        ];
        $params['bank']     = Bank::pluck('name', 'code');
        return view('memberregistration::frontend.formulir.member.confirmation', $params);
    }

    public function paymentConfirmationStore(Request $request) {
        try {
            $member = Member::where('pub_id', $request->pub_id)->firstOrFail();

            $memberConfirmation = $member->has_subs->has_confirmation;

            if($memberConfirmation != null) {
                return redirect(route('member.register.confirm', $request->pub_id))
                    ->with('success_message', __('Hai ' . $member->full_name . ', konfirmasi pembayaran kamu sudah berhasil dikirim.'));
            } else {
            
                $data['member_subscription_id']     = $request->member_subscription_id;
                $data['bank_account_id']            = $request->bank_account_id;
                $data['transfer_amount']            = cleanNominal($request->transfer_amount);
                $data['transfer_date']              = Carbon::createFromFormat('d/m/Y', $request->transfer_date)->format('Y-m-d');
                $data['transfer_bank_id']           = $request->transfer_bank_id;
                $data['transfer_bank_name']         = $request->transfer_bank_name;
                $data['transfer_bank_account']      = trim($request->transfer_bank_account);
                $data['transfer_bank_holder_name']  = strtoupper($request->transfer_bank_holder_name);

                if($request->hasFile('transfer_attachment')) {
                    ini_set('memory_limit','256M');
                    $fileExt    = strtolower($request->file('transfer_attachment')->getClientOriginalExtension());
                    $fileName   = 'transfer_attachment-' . Uuid::uuid4() . '.'  . $fileExt;
                    $data['transfer_attachment']    = $fileName;
                    $fileAttc = Image::make($request->file('transfer_attachment'))
                        ->resize(800, 800, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    Storage::disk($this->disk)->put(
                        'members/' . $member->nik . '/' . $fileName,
                        $fileAttc->stream(),
                        'public'
                    );
                }

                /*$memberSubsConfirm =*/ MemberSubscriptionConfirmation::create($data);
                MemberSubscription::findOrFail($request->member_subscription_id)->update(['payment_status' => 'CP']);
                /*
                $data['transfer_bank_name']         = Bank::where('code', $request->transfer_bank_id)->first()->name;
                $data['full_name']                  = $member->full_name;
                $data['transfer_attachment_file']   = 'app/public/members/'. $member->nik .'/'.$memberSubsConfirm->transfer_attachment;
                $data['email']                      = 'choirulma.jr@gmail.com';
                MemberRegisterPaymentConfirmationJob::dispatch($data)
                        ->delay(now()->addMinutes(5));
                */
                return redirect(route('member.register.confirm.thanks', $request->pub_id));
            }
        } catch (\Exception $e) {
            Log::error($e);
            return redirect(route('member.register.confirm', $request->pub_id))
                ->with('error_message', trans('message.error_save'));
        }
    }

    public function paymentConfirmationUpdate(Request $request, $id) {
        try {
            $memberConfirmation = MemberSubscriptionConfirmation::findOrFail($id);
            $nik = $memberConfirmation->member_subscription->member->nik;
            $tagihan    = $memberConfirmation->member_subscription->subs_price + $memberConfirmation->member_subscription->unique_no;
            $transfer   = $memberConfirmation->transfer_amount;

            if($tagihan <> $transfer) {
                $data['transfer_amount'] = cleanNominal($request->transfer_amount);
            }

            if(empty($memberConfirmation->transfer_attachment)) {
                if($request->hasFile('transfer_attachment')) {
                    $fileExt    = strtolower($request->file('transfer_attachment')->getClientOriginalExtension());
                    $fileName   = 'transfer_attachment-' . Uuid::uuid4() . '.'  . $fileExt;
                    $data['transfer_attachment']    = $fileName;
                    $fileAttc = Image::make($request->file('transfer_attachment'))
                        ->resize(800, 800, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    Storage::disk($this->disk)->put(
                        'members/' . $nik . '/' . $fileName,
                        $fileAttc->stream(),
                        'public'
                    );
                }
            }
            $memberConfirmation->update($data);
            return redirect(route('member.register.confirm.thanks', $request->pub_id));
        } catch (\Exception $e) {
            return redirect(route('member.register.confirm', $request->pub_id))
                ->with('error_message', trans('message.error_save'. $e));
        }
    }

    public function paymentConfirmationThanks($pubid) {
        $params['title']    = 'Selamat Konfirmasi Pembayaran Berhasil dikirim.';
        $params['data']     = Member::where('pub_id', $pubid)->first();
        return view('memberregistration::frontend.formulir.member.thanks-payment', $params);
    }

    public function checkNIK(Request $request)
    {
        $nik  = $request->nik;
        $member = Member::where('nik', $nik)->get();
        if($member->count() > 0) {
            $msg = "false";
        } else {
            $msg = "true";
        }
        return $msg;
    }

    public function checkEmail(Request $request)
    {
        $email  = $request->email;
        $member = Member::where('email', $email)->get();
        if($member->count() > 0) {
            $msg = "false";
        } else {
            $msg = "true";
        }
        return $msg;
    }

    private function getJenjang() {
        $data = [
            'SMA' => 'SMA',
            'SMK' => 'SMK',
            'D3' => 'D3',
            'S1' => 'S1'
        ];
        return $data;
    }
}
