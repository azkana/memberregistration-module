<?php

namespace Modules\MemberRegistration\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Configs\GeneralController;

class SettingsController extends Controller
{
    protected $pageTitle;
    protected $generalOptions;
    protected $weekdays;

    public function __construct()
    {
        $this->pageTitle = 'Membership Settings';
        $this->generalOptions = new GeneralController();
        $this->weekdays = array(
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        );
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['formAvailable']= array(
            'everyday'      => __('Everyday'),
            'workingday'    => __('Working Day'),
            // 'selectedday'   => __('Selected Day'),
        );
        $params['formAvailableTime']= array(
            'allday'        => __('Allday'),
            // 'workinghours'  => __('Working Hour'),
            'timerange'     => __('Time Range'),
        );
        $params['uniqueNumberType'] = array(
            // 'sequence'  => __('Sequence'),
            'random'    => __('Random')
        );
        $params['workingDays']  = explode(',', getOptions('working_days'));
        $params['weekdays']     = $this->weekdays;
        $params['exportTemplate'] = array(
            '1' => 'Template 1',
            '2' => 'Template 2'
        );
        return view('memberregistration::settings.index', $params);
    }

    public function store(Request $request)
    {
        try {
            $tabPane = $request->tab;

            if($tabPane == 'member') {
                $data['membership_subscription']    = cleanNominal($request->membership_subscription);
                $data['membership_unique']          = $request->membership_unique == 1 ? 1 : 0;
                $data['membership_unique_type']     = $request->membership_unique_type;
                $data['membership_unique_digit']    = $request->membership_unique_digit;
                $data['membership_payment_due']     = $request->membership_payment_due;

                $data['membership_form_published']  = $request->membership_form_published == 1 ? 1 : 0;
                $data['membership_form_available']  = $request->membership_form_available;
                $data['membership_form_available_time']         = $request->membership_form_available_time;
                $data['membership_form_available_time_start']   = $request->membership_form_available_time_start;
                $data['membership_form_available_time_end']     = $request->membership_form_available_time_end;
                $data['membership_form_max_day']            = cleanNominal($request->membership_form_max_day);
                $data['membership_form_unpublished_note']   = trim($request->membership_form_unpublished_note);

                $data['membership_need_approval_subday']   = cleanNominal($request->membership_need_approval_subday);
                $data['membership_active_member_subday']   = cleanNominal($request->membership_active_member_subday);
                $data['membership_waiting_payment_subday']   = cleanNominal($request->membership_waiting_payment_subday);
            }

            if($tabPane == 'tnc') {
                $data['membership_tnc']    = trim($request->membership_tnc);
            }

            if($tabPane == 'iei') {
                $data['iei_export_template'] = $request->iei_export_template;
            }
            
            $this->generalOptions->saveOptions($data);

            return redirect()->back()
				->with('success', trans('message.success_save'));
        } catch (Exception $e) {
            return redirect()->back()
				->with('error', trans('message.error_save'));
        }
    }

}
