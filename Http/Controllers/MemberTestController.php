<?php

namespace Modules\MemberRegistration\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Modules\MemberRegistration\Entities\MemberTest;
use Modules\MemberRegistration\Imports\MemberTestImport;
use Modules\MemberRegistration\Entities\MemberTestBatch;

class MemberTestController extends Controller
{
    protected $pageTitle;
    protected $disk;

    public function __construct()
    {
        $this->pageTitle = 'Member Test Info';
        $this->disk = config('memberregistration.disk');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle'] = $this->pageTitle;
        return view('memberregistration::admin.members.tests.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new MemberTestBatch();
            $data = $data->latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('checkboxes', function($row) {
                    return null;
                })
                ->addColumn('is_active', function($row) {
                    return $row->is_active == true ? 'Active' : 'Not Active';
                })
                ->addColumn('action', function($row) {
                    $btnEdit = Auth::user()->hasPermissionTo('member-test-info-edit') ?
                        '
                        <li>
                            <a href="#" onclick="editThis(\''.$row->id.'\');return false;"><i class="fa fa-pencil"></i>Edit</a>
                        </li>
                        ' : null
                    ;
                    $btnDelete = Auth::user()->hasPermissionTo('member-test-info-delete') ?
                        '
                        <li>
                            <a href="#" onclick="deleteThis(\''.$row->id.'\');return false;"><i class="fa fa-trash"></i>Delete</a>
                        </li>
                        ' : null
                    ;
                    $btn =
                    '
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Action</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                                    <a href="' . route('member.test.show', $row->id) . '"><i class="fa fa-eye"></i>Detail</a>
                                </li>
                                '. $btnEdit .'
                                '. $btnDelete .'
                            </ul>
                        </div>
                    ';

                    return $btn;
                })
                ->rawColumns([
                    'checkboxes', 'is_active', 'action'
                ])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['batch']    = MemberTestBatch::_generateBatch();
        return view('memberregistration::admin.members.tests.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {

            $data['batch']  = $request->batch;
            $data['name']   = $request->name;
            $data['desc']   = $request->desc;
            $data['test_date']  = Carbon::parse($request->test_date)->format('Y-m-d H:i:s');
            $data['start_date'] = Carbon::parse($request->start_date . $request->start_time)->format('Y-m-d H:i:s');
            $data['end_date']   = Carbon::parse($request->end_date . $request->end_time)->format('Y-m-d H:i:s');
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;
            $data['created_by'] = Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;

            MemberTestBatch::create($data);

            return response()->json([
                'message' => 'Data berhasil disimpan.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $params['pageTitle'] = $this->pageTitle;
        $params['data'] = MemberTestBatch::find($id);
        return view('memberregistration::admin.members.tests.show', $params);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $id = $request->id;
        $params['data'] = MemberTestBatch::find($id);
        return view('memberregistration::admin.members.tests.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        try {

            $id = $request->id;
            $data['name']   = $request->name;
            $data['desc']   = $request->desc;
            $data['test_date']  = Carbon::parse($request->test_date)->format('Y-m-d H:i:s');
            $data['start_date'] = Carbon::parse($request->start_date . $request->start_time)->format('Y-m-d H:i:s');
            $data['end_date']   = Carbon::parse($request->end_date . $request->end_time)->format('Y-m-d H:i:s');
            $data['is_active']  = $request->is_active == 1 ? 1 : 0;
            $data['updated_by'] = Auth::user()->id;

            $test = MemberTestBatch::findOrFail($id);

            $test->update($data);

            return response()->json([
                'message' => 'Data berhasil diubah.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $params['data'] = MemberTestBatch::find($id);
        return view('memberregistration::admin.members.tests.delete', $params);
    }
    public function destroy(Request $request)
    {
        try {
            $id = $request->id;
            $data = MemberTestBatch::findOrFail($id);
            $data->delete();

            return response()->json([
                'message' => 'Data berhasil dihapus.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Data gagal dihapus.'
            ]);
        }
    }

    public function importForm(Request $request)
    {
        $params['batch'] = $request->batch;
        return view('memberregistration::admin.members.tests.import', $params);
    }

    public function importSave(Request $request)
    {
        try {
            $testBatch          = MemberTestBatch::where('batch', $request->batch)->first();
            $data['batch']      = $testBatch->batch;
            $data['testDate']   = $testBatch->test_date;
            $data['startDate']  = $testBatch->start_date;
            $data['endDate']    = $testBatch->end_date;
            $data['author']     = Auth::user()->id;

            if($request->hasFile('excel')) {
                $file = $request->file('excel');
                Excel::import(new MemberTestImport($data), $file);
            }

            return response()->json([
                'message' => 'Data berhasil diimport.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }

    }

    public function testData(Request $request)
    {
        if($request->ajax()) {
            $data = new MemberTest();

            if(isset($request->batch)) {
                $data = $data->where('batch_id', $request->batch);
            }

            $data = $data->latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                // ->addColumn('checkboxes', function($row) {
                //     return null;
                // })
                ->addColumn('test', function($row) {
                    return str_pad($row->test, 5, '0', STR_PAD_LEFT);
                })
                ->addColumn('action', function($row) {
                    return null;
                })
                ->make(true);
        }
    }

    public function testInfo(Request $request)
    {
        try {
            $type = $request->get('type');
            $query = $request->get('query');

            $data = new MemberTest();

            if($type == 'nik') {
                $data = $data->where('nik', $query);
            }

            if($type == 'member') {
                $data = $data->where('member', $query);
            }

            $data = $data->where('is_active', true);
            $data = $data->where('start_date', '<=', Carbon::now()->format('Y-m-d H:i:s'));
            $data = $data->where('end_date', '>=', Carbon::now()->format('Y-m-d H:i:s'));
            $data = $data->orderBy('created_at', 'DESC');
            $data = $data->first();

            return response()->json([
                'code' => 200,
                'message' => 'Data tersedia.',
                'data'  => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                // 'message' => $e->getMessage()
                'message' => 'Terjadi kesalahan.'
            ]);
        }


    }
}
