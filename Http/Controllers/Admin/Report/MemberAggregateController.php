<?php

namespace Modules\MemberRegistration\Http\Controllers\Admin\Report;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\MemberRegistration\Entities\Reports\AggregateMemberActive;

class MemberAggregateController extends Controller
{

    public function memberAggActiveInitialDate()
    {
        try {

            $q = AggregateMemberActive::_generateDate();

            return $q;

        } catch (\Exception $e) {
            return $e;
        }
    }

    public function memberAggActiveUpdate()
    {
        try {
            $date = Carbon::now()->subDay()->format('Y-m-d');
            $data = AggregateMemberActive::_getAggregate($date);
            $q = AggregateMemberActive::_updateAggregate($data);

            return $q;

        } catch (\Exception $e) {
            return $e;
        }
    }
}
