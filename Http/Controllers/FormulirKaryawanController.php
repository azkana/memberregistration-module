<?php

namespace Modules\MemberRegistration\Http\Controllers;

use Excel;
use Image;
use Storage;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Master\Agama;
use App\Models\Master\Bank;
use App\Models\Master\Provinsi;
use Modules\MemberRegistration\Entities\FormulirKaryawan;
use Modules\MemberRegistration\Exports\FormulirKaryawanExport;
use Ramsey\Uuid\Uuid;

class FormulirKaryawanController extends Controller
{
    protected $pageTitle = 'Kandidat';

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['startDate']    = Carbon::today()->subDays(2)->format('d-m-Y');
        $params['endDate']      = Carbon::today()->format('d-m-Y');
        return view('memberregistration::karyawan.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new FormulirKaryawan();
            if(isset($request->start_date)) {
                $data = $data->whereDate('created_at', '>=', dateFormatYmd($request->start_date));
            } else {
                $data = $data->whereDate('created_at', '>=', Carbon::today()->subDays(3)->format('Y-m-d'));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('created_at', '<=', dateFormatYmd($request->end_date));
            } else {
                $data = $data->whereDate('created_at', '<=', Carbon::today());
            }
            $data = $data->latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('provinsi', function($row){
                    $provinsiName = isset($row->rel_provinsi_ktp->name) ? strtoupper($row->rel_provinsi_ktp->name) : null;
                    return $provinsiName;
                })
                ->addColumn('kabupaten', function($row){
                    $kabupatenName = isset($row->rel_kab_ktp->name) ? strtoupper($row->rel_kab_ktp->name) : null;
                    return $kabupatenName;
                })
                ->addColumn('action', function($row) {
                    $btn = 
                        '
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Action</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                                    <a href="'.route("candidate.show", $row->pub_id).'"><i class="fa fa-eye"></i>Detail</a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                                </li>
                            </ul>
                        </div>
                        <div class="modal fade modal-danger" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="POST" action="'.route("candidate.destroy", $row->pub_id).'">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus data member <b>'.$row->fullname.'</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline">Hapus</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        ';
                    return $btn;
                })
                ->addColumn('checkboxes', function($row) {
                    return null;
                })
                ->rawColumns(['action', 'checkboxes', 'provinsi', 'kabupaten'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['title']    = 'Formulir Karyawan Baru';
        $params['agama']    = Agama::where('is_active', 1)->pluck('name', 'id');
        $params['bank']     = Bank::where('is_active', 1)->pluck('name', 'id');
        $params['jenjang']  = $this->getJenjang();
        $params['marital']  = getMaritalStatusForEpson();
        $params['ktp_type'] = [
            1 => "SEUMUR HIDUP",
            2 => "RESI/SUKET"
        ];
        $params['jemputan'] = [
            1 => 'CIKARANG',
            2 => 'PILAR',
            3 => 'LEMAHABANG',
            4 => 'CIKARANG BARU',
            5 => 'CIBARUSAH',
            6 => 'LIPPO'
        ];
        $params['provinsi'] = Provinsi::where('is_active', true)
            ->orderBy('name')->pluck('name', 'code');
        $params['hubKel']   = getHubKelForEpson();
        $params['vacType']  = [
            'sinovac' => 'Sinovac',
            'astrazeneca' => 'Astra Zeneca',
            'pfizer' => 'Pfizer',
            'moderna' => 'Moderna',
            'biofarma' => 'Bio Farma',
            'sputnikv' => 'Sputnik V',
            'janssen' => 'Janssen',
            'convidecia' => 'Convidecia'
        ];
        return view('memberregistration::frontend.formulir.create', $params);
    }

    public function createOld()
    {
        $params['title']    = 'Formulir Karyawan Baru';
        $params['agama']    = Agama::pluck('name', 'id');
        $params['bank']     = Bank::pluck('name', 'id');
        $params['jenjang']  = $this->getJenjang();
        $params['marital']  = getMaritalStatusList();
        $params['ktp_type'] = [
            1 => "EKTP",
            2 => "RESI/SUKET (Surat Keterangan)"
        ];
        $params['jemputan'] = [
            1 => 'CIKARANG',
            2 => 'PILAR',
            3 => 'LEMAHABANG',
            4 => 'CIKARANG BARU',
            5 => 'CIBARUSAH',
            6 => 'LIPPO'
        ];
        $params['hubKel']   = getHubKel();
        return view('memberregistration::frontend.formulir.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['pub_id']     = $pubID = base64_encode($request->nik.date('Y-m-d H:i:s'));
            $data['fullname']   = strtoupper($request->fullname);
            $data['firstname']  = strtoupper($request->firstname);
            $data['midname']    = strtoupper($request->midname);
            $data['lastname']   = strtoupper($request->lastname);
            $data['tmp_lahir']  = strtoupper($request->tmp_lahir);
            $data['tgl_lahir']  = Carbon::createFromFormat('d/m/Y', $request->tgl_lahir)->format('Y-m-d');
            $data['gender']     = $request->gender;
            $data['agama']      = $request->agama;
            $data['nik']        = $request->nik;
            $data['ktp_type']   = $request->ktp_type;
            $data['marital']    = $request->marital;
            $data['email']      = strtolower($request->email);
            $data['no_hp']      = $request->no_hp;
            $data['npwp']       = $request->npwp;
            $data['kk']         = $request->kk;
            $data['mother']     = strtoupper($request->mother);
            $data['father']     = strtoupper($request->father);
            // Alamat Sesuai KTP
            $data['alamat']     = strtoupper($request->alamat);
            $data['alamat_rt']  = $request->alamat_rt;
            $data['alamat_rw']  = $request->alamat_rw;
            $data['kelurahan']  = strtoupper($request->kelurahan);
            $data['kecamatan']  = $request->kecamatan;
            $data['kabupaten']  = $request->kabupaten;
            $data['provinsi']   = $request->provinsi;
            $data['kodepos']    = $request->kodepos;
            $data['phone']      = $request->phone;
            // Alamat Domisili
            $data['alamat_dom']     = strtoupper($request->alamat_dom);
            $data['alamat_rt_dom']  = $request->alamat_rt_dom;
            $data['alamat_rw_dom']  = $request->alamat_rw_dom;
            $data['kelurahan_dom']  = strtoupper($request->kelurahan_dom);
            $data['kecamatan_dom']  = $request->kecamatan_dom;
            $data['kabupaten_dom']  = $request->kabupaten_dom;
            $data['provinsi_dom']   = $request->provinsi_dom;
            $data['kodepos_dom']    = $request->kodepos_dom;
            $data['phone_dom']      = $request->phone_dom;
            // Kontak Darurat
            $data['emr_name']       = strtoupper($request->emr_name);
            $data['emr_hubkel']     = $request->emr_hubkel;
            $data['emr_phone']      = $request->emr_phone;
            $data['emr_alamat']     = strtoupper($request->emr_alamat);
            $data['emr_alamat_rt']  = $request->emr_alamat_rt;
            $data['emr_alamat_rw']  = $request->emr_alamat_rw;
            $data['emr_kelurahan']  = strtoupper($request->emr_kelurahan);
            $data['emr_kecamatan']  = $request->emr_kecamatan;
            $data['emr_kabupaten']  = $request->emr_kabupaten;
            $data['emr_provinsi']   = $request->emr_provinsi;
            $data['emr_kodepos']    = $request->emr_kodepos;
            // Pendidikan
            $data['sekolah']        = strtoupper($request->sekolah);
            $data['jurusan']        = strtoupper($request->jurusan);
            $data['sekolah_kab']    = strtoupper($request->sekolah_kab);
            $data['thn_lulus']      = $request->thn_lulus;
            // Data Bank
            $data['bank_name']          = $request->bank_name;
            $data['bank_account_no']    = $request->bank_account_no;
            $data['bank_branch']        = strtoupper($request->bank_branch);
            $data['jemputan']           = $request->jemputan;
            $data['tgl_tes']            = Carbon::createFromFormat('d/m/Y', $request->tgl_tes)->format('Y-m-d');
            $data['skck_validity']      = Carbon::createFromFormat('d/m/Y', $request->skck_validity)->format('Y-m-d');
            // Pengalaman Kerja
            $data['exp_company_1']      = strtoupper($request->exp_company_1);
            $data['exp_company_type_1'] = strtoupper($request->exp_company_type_1);
            $data['exp_position_1']     = strtoupper($request->exp_position_1);
            $data['exp_salary_1']       = $request->exp_salary_1;
            $data['exp_start_1']        = $request->exp_start_1;
            $data['exp_company_2']      = strtoupper($request->exp_company_2);
            $data['exp_company_type_2'] = strtoupper($request->exp_company_type_2);
            $data['exp_position_2']     = strtoupper($request->exp_position_2);
            $data['exp_salary_2']       = $request->exp_salary_2;
            $data['exp_start_2']        = $request->exp_start_2;
            /* Data Vaksin */
            $data['vac_type']           = $request->vac_type;
            $data['vac_1']              = $request->vac_1 == 1 ? 1 : 0;
            $data['vac_1_date']         = !empty($request->vac_1_date) ? Carbon::createFromFormat('d/m/Y', $request->vac_1_date)->format('Y-m-d') : null;
            $data['vac_1_loc']          = $request->vac_1_loc;
            $data['vac_2']              = $request->vac_2 == 1 ? 1 : 0;
            $data['vac_2_date']         = !empty($request->vac_2_date) ? Carbon::createFromFormat('d/m/Y', $request->vac_2_date)->format('Y-m-d') : null;
            $data['vac_2_loc']          = $request->vac_2_loc;

            $nik    = FormulirKaryawan::where('nik', $request->nik)->get();
            $email  = FormulirKaryawan::where('email', $request->email)->get();

            if(count($nik) > 0) {
                return redirect(route('candidate.form'))
				    ->with('error_message', 'NIK (No. KTP) ' . $request->nik . ' sudah terdaftar.');
            } elseif (count($email) > 0) {
                return redirect(route('candidate.form'))
				    ->with('error_message', 'alamat email ' . $request->email . ' sudah terdaftar.');
            } else {
            
                if($request->hasFile('file_ktp')) {
                    ini_set('memory_limit','256M');
                    $ktpFile = Image::make($request->file('file_ktp'))
                        ->resize(1000, 1000, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    $fileExt = strtolower($request->file('file_ktp')->getClientOriginalExtension());
                    $ktpFileName = 'ktp-' . Uuid::uuid4() . '.' . $fileExt;
                    $data['file_ktp']   = $ktpFileName;
                    Storage::disk('s3')->put(
                        'candidate/iei/' . $request->nik . '/' . $ktpFileName,
                        $ktpFile->stream(), 'public'
                    );
                }
                if($request->hasFile('file_kk')) {
                    ini_set('memory_limit','256M');
                    $kkFile = Image::make($request->file('file_kk'))
                        ->resize(1000, 1000, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    $fileExt = strtolower($request->file('file_kk')->getClientOriginalExtension());
                    $kkFileName = 'kk-' . Uuid::uuid4() . '.' . $fileExt;
                    $data['file_kk']   = $kkFileName;
                    Storage::disk('s3')->put(
                        'candidate/iei/' . $request->nik . '/' . $kkFileName,
                        $kkFile->stream(), 'public'
                    );
                }
                if($request->hasFile('file_skck')) {
                    ini_set('memory_limit','256M');
                    $skckFile = Image::make($request->file('file_skck'))
                        ->resize(1000, 1000, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    $fileExt = strtolower($request->file('file_skck')->getClientOriginalExtension());
                    $skckFileName = 'skck-' . Uuid::uuid4() . '.' . $fileExt;
                    $data['file_skck']   = $skckFileName;
                    Storage::disk('s3')->put(
                        'candidate/iei/' . $request->nik . '/' . $skckFileName,
                        $skckFile->stream(), 'public'
                    );
                }
                if($request->hasFile('file_tab')) {
                    ini_set('memory_limit','256M');
                    $tabFile = Image::make($request->file('file_tab'))
                        ->resize(1000, 1000, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    $fileExt = strtolower($request->file('file_tab')->getClientOriginalExtension());
                    $tabFileName = 'tab-' . Uuid::uuid4() . '.' . $fileExt;
                    $data['file_tab']   = $tabFileName;
                    Storage::disk('s3')->put(
                        'candidate/iei/' . $request->nik . '/' . $tabFileName,
                        $tabFile->stream(), 'public'
                    );
                }
                if($request->hasFile('file_vaksin')) {
                    ini_set('memory_limit','256M');
                    $tabFile = Image::make($request->file('file_vaksin'))
                        ->resize(1000, 1000, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    $fileExt = strtolower($request->file('file_vaksin')->getClientOriginalExtension());
                    $fileName = 'vaksin-' . Uuid::uuid4() . '.' . $fileExt;
                    $data['file_vaksin']   = $fileName;
                    Storage::disk('s3')->put(
                        'candidate/iei/' . $request->nik . '/' . $fileName,
                        $tabFile->stream(), 'public'
                    );
                }
                FormulirKaryawan::create($data);
            }
           
            return redirect(route('candidate.thanks', $pubID));
        } catch (\Exception $ex) {
            return redirect(route('candidate.form'))
				->with('error_message', trans('message.error_save'));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request, $pub_id)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['pageSubTitle'] = 'Detail';
        $params['data']         = FormulirKaryawan::where('pub_id', $pub_id)->first();
        return view('memberregistration::karyawan.show', $params);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($pub_id)
    {
        try {
            $data = FormulirKaryawan::where('pub_id', $pub_id)->firstOrFail();
            $data->delete();
            return redirect(route('candidate.index'))
				->with('success_message', trans('message.success_delete'));
        } catch(\Exception $e) {
            return redirect(route('candidate.index'))
				->with('error_message', trans('message.error_delete'));
        }
    }

    public function destroyBulk(Request $request) 
    {
        try {
            $data = $request->id;
            foreach($data as $id) {
                $member = FormulirKaryawan::findOrFail($id);
                $member->delete();
            }
            return response()->json([
                'message' => 'Data deleted successfully!'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'message' => $e
            ]);
        }
    }

    private function getJenjang() {
        $data = [
            'SMA' => 'SMA',
            'SMK' => 'SMK',
            'D3' => 'D3',
            'S1' => 'S1'
        ];
        return $data;
    }

    public function checkNIK(Request $request)
    {
        $nik  = $request->nik;
        $member = FormulirKaryawan::where('nik', $nik)->get();
        if($member->count() > 0) {
            $msg = "false";
        } else {
            $msg = "true";
        }
        return $msg;
    }

    public function checkEmail(Request $request)
    {
        $email  = $request->email;
        $member = FormulirKaryawan::where('email', $email)->get();
        if($member->count() > 0) {
            $msg = "false";
        } else {
            $msg = "true";
        }
        return $msg;
    }

    public function registrationThanks($pub_id) 
    {
        $params['title']    = 'Selamat pendaftaran berhasil!';
        $params['data']     = FormulirKaryawan::where('pub_id', $pub_id)->first();
        return view('memberregistration::frontend.formulir.thanks', $params);
    }

    public function exportExcel() 
    {
        $data['orderBy']    = 'created_at';
        $data['date']       = date('Y-m-d');
        return Excel::download(new FormulirKaryawanExport($data), 'candidates_'.date('YmdHis').'.xlsx');
    }
}
