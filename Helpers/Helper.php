<?php

use Modules\MemberRegistration\Entities\FormulirKaryawan;
use Modules\MemberRegistration\Entities\Member;

if(!function_exists('getMemberCount')) {
    function getMemberCount($state = null) {
        $member = new member();
        if($state == null) {
            $member = $member;
        }

        if($state == 'active-member') {
            $member = $member->whereHas('has_subs', function ($q) {
                return $q->where('payment_status', 'AM');
            });
        }

        if($state == 'need-approval') {
            $member = $member->whereHas('has_subs', function ($q) {
                return $q->where('payment_status', 'CP');
            });
        }

        if($state == 'waiting-payment') {
            $member = $member->whereHas('has_subs', function ($q) {
                return $q->where('payment_status', 'WP')
                    ->where('id_member', null);
            });
        }

        $member = $member->count();

        if($member > 10000) {
            $member = '10.000+';
        } else {
            $member = numberFormat($member);
        }
        return $member;
    }
}

if(!function_exists('getCandidateCount')) {
    function getCandidateCount() {
        $candidates = new FormulirKaryawan();
        $candidateCount = $candidates->count();
        return numberFormat($candidateCount);
    }
}
