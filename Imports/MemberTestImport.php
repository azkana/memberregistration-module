<?php

namespace Modules\MemberRegistration\Imports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Modules\MemberRegistration\Entities\MemberTest;

class MemberTestImport implements ToCollection, WithHeadingRow, WithBatchInserts, WithChunkReading, SkipsOnFailure
{
    use SkipsFailures;

    protected $data;

    public function __construct($data = array())
    {
        $this->data = $data;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        // Clear data
        $checkData = MemberTest::where('batch_id', $this->data['batch']);
        if($checkData) {
            $checkData->delete();
        }

        $testDate = Carbon::parse($this->data['testDate'])->format('Y-m-d');
        $jam = '00:00:00';

        // Insert Data
        foreach ($rows as $row) {

            if(isset($row['jam'])) {
                $jam = !is_null($row['jam']) || !empty($row['jam']) || $row['jam'] == '' ? Carbon::parse($row['jam'])->format('H:i:s') : '00:00:00';
            }

            $data = [
                'batch_id'  => $this->data['batch'],
                'nik'       => $row['nik'],
                'member'    => $row['member'],
                'test'      => $row['test'],
                'name'      => $row['name'],
                'test_date' => Carbon::parse($testDate . $jam)->format('Y-m-d H:i:s'),
                'start_date'=> $this->data['startDate'],
                'end_date'  => $this->data['endDate'],
                'created_by'=> $this->data['author'],
                'updated_by'=> $this->data['author'],
            ];

            MemberTest::create($data);

        }
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
