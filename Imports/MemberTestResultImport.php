<?php

namespace Modules\MemberRegistration\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Modules\MemberRegistration\Entities\MemberTestResult;

class MemberTestResultImport implements ToCollection, WithHeadingRow, WithBatchInserts, WithChunkReading, SkipsOnFailure
{
    use SkipsFailures;

    protected $data;

    public function __construct($data = array())
    {
        $this->data = $data;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        // Clear data
        $checkData = MemberTestResult::where('batch_id', $this->data['batch']);
        if($checkData) {
            $checkData->delete();
        }

        // Insert Data
        foreach ($rows as $row) {
            $data = [
                'batch_id'  => $this->data['batch'],
                'nik'       => $row['nik'],
                'member'    => $row['member'],
                'test'      => $row['test'],
                'name'      => $row['name'],
                'keterangan'=> $row['keterangan'],
                'start_date'=> $this->data['startDate'],
                'end_date'  => $this->data['endDate'],
                'created_by'=> $this->data['author'],
                'updated_by'=> $this->data['author'],
            ];

            MemberTestResult::create($data);
        }
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
