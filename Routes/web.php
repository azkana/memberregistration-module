<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {

    Route::prefix('manage')->group(function() {

        Route::prefix('membership')->group(function() {

            Route::name('member.')->group(function() {
                Route::get('members', 'MemberRegistrationController@index')->name('index');
                Route::get('members/data', 'MemberRegistrationController@indexData')->name('index.data');
                Route::get('members/create', 'MemberRegistrationController@create')->name('create');
                Route::post('members/store', 'MemberRegistrationController@store')->name('store');
                Route::get('members/{id}/show', 'MemberRegistrationController@show')->name('show');
                Route::get('members/{id}/edit', 'MemberRegistrationController@edit')->name('edit');
                Route::patch('members/{id}', 'MemberRegistrationController@update')->name('update');
                Route::delete('members/bulk', 'MemberRegistrationController@destroyBulk')->name('destroy.bulk');
                Route::delete('members/{id}', 'MemberRegistrationController@destroy')->name('destroy');
                Route::get('members/{id}/print', 'MemberRegistrationController@print')->name('print');
                Route::get('members/{id}/print/pdf', 'MemberRegistrationController@printPdf')->name('print.pdf');
                Route::get('members/excel', 'MemberRegistrationController@exports')->name('exports');

                Route::get('members/show', 'MemberRegistrationController@showModal')->name('show.modal');

                /* Member Active */
                Route::get('members/active-member', 'MemberRegistrationController@activeMember')->name('active-member');
                Route::get('members/active-member/data', 'MemberRegistrationController@activeMemberData')->name('active-member.data');
                /* Member Need Approval */
                Route::get('members/need-approval', 'MemberRegistrationController@needApproval')->name('need-approval');
                Route::get('members/need-approval/data', 'MemberRegistrationController@needApprovalData')->name('need-approval.data');
                /* Member Waiting Payment */
                Route::get('members/waiting-payment', 'MemberRegistrationController@waitingPayment')->name('waiting-payment')->middleware('permission:member-waiting-payment');
                Route::get('members/waiting-payment/data', 'MemberRegistrationController@waitingPaymentData')->name('waiting-payment.data')->middleware('permission:member-waiting-payment');
                Route::get('members/edit/email', 'MemberRegistrationController@editEmailForm')->name('edit-email');
                Route::patch('members/edit/email/{id}', 'MemberRegistrationController@editEmailSave')->name('edit-email.save');
                Route::get('members/resend/email-registration', 'MemberRegistrationController@resendEmailRegistrationForm')->name('resend-email');
                Route::patch('members/resend/email-registration/{id}', 'MemberRegistrationController@resendEmailRegistrationSave')->name('resend-email.save');
                Route::get('members/upload-bukti-transfer', 'MemberRegistrationController@uploadBuktiTransferForm')->name('upload-bukti-transfer');
                Route::patch('members/upload-bukti-transfer/{id}', 'MemberRegistrationController@uploadBuktiTransferSave')->name('upload-bukti-transfer.save');
                Route::delete('members/upload-bukti-transfer/delete-file', 'MemberRegistrationController@uploadBuktiTransferDeleteSave')->name('upload-bukti-transfer.delete-file.save');
                Route::get('members/upload-bukti-transfer/delete-file', 'MemberRegistrationController@uploadBuktiTransferDeleteForm')->name('upload-bukti-transfer.delete-file');

                /* approve payment confirmation */
                Route::get('members/payment/approve/confirmation', 'MemberRegistrationController@paymentApprovalConfirmation')->name('payment.approve.confirmation');
                Route::patch('members/payment/approve/confirmation/{id}', 'MemberRegistrationController@paymentApprovalConfirmationSave')->name('payment.approve.confirmation.save');
                /* Send No. Member Information to Email */
                Route::post('members/{id}/email', 'MemberRegistrationController@sendEmailMember')->name('email');
                /* Check Nomor Member */
                Route::get('members/check/nomember', 'MemberRegistrationController@checkNoMember')->name('check.nomember');
            });

            Route::prefix('tests')->group(function() {

                Route::prefix('info')->group(function() {

                    Route::name('member.test.')->group(function() {

                        Route::get('/', 'MemberTestController@index')->name('index');
                        Route::get('data', 'MemberTestController@indexData')->name('index.data');
                        Route::get('create', 'MemberTestController@create')->name('create');
                        Route::post('create', 'MemberTestController@store')->name('store');
                        Route::get('edit', 'MemberTestController@edit')->name('edit');
                        Route::patch('edit', 'MemberTestController@update')->name('update');
                        Route::get('{id}/show', 'MemberTestController@show')->name('show');
                        Route::get('delete', 'MemberTestController@delete')->name('delete');
                        Route::delete('destroy', 'MemberTestController@destroy')->name('destroy');
                        Route::get('import', 'MemberTestController@importForm')->name('import');
                        Route::post('import', 'MemberTestController@importSave')->name('import.save');

                        Route::get('data-detail', 'MemberTestController@testData')->name('data');

                    });

                });

                Route::prefix('result')->group(function() {

                    Route::name('member.test.result.')->group(function() {

                        Route::get('/', 'MemberTestResultController@index')->name('index');
                        Route::get('data', 'MemberTestResultController@indexData')->name('index.data');
                        Route::get('create', 'MemberTestResultController@create')->name('create');
                        Route::post('create', 'MemberTestResultController@store')->name('store');
                        Route::get('edit', 'MemberTestResultController@edit')->name('edit');
                        Route::patch('edit', 'MemberTestResultController@update')->name('update');
                        Route::get('{id}/show', 'MemberTestResultController@show')->name('show');
                        Route::get('delete', 'MemberTestResultController@delete')->name('delete');
                        Route::delete('destroy', 'MemberTestResultController@destroy')->name('destroy');
                        Route::get('import', 'MemberTestResultController@importForm')->name('import');
                        Route::post('import', 'MemberTestResultController@importSave')->name('import.save');

                        Route::get('data-detail', 'MemberTestResultController@testData')->name('data');

                    });

                });

            });

            Route::name('candidate.')->group(function() {
                Route::get('candidate', 'FormulirKaryawanController@index')->name('index');
                Route::get('candidate/data', 'FormulirKaryawanController@indexData')->name('index.data');
                Route::get('candidate/show/{pub_id}', 'FormulirKaryawanController@show')->name('show');
                Route::delete('candidate/bulk', 'FormulirKaryawanController@destroyBulk')->name('destroy.bulk');
                Route::delete('candidate/{pub_id}', 'FormulirKaryawanController@destroy')->name('destroy');
                Route::get('candidate/excel', 'FormulirKaryawanController@exportExcel')->name('excel');
            });

            Route::name('report.')->group(function() {
                Route::get('reports', 'MemberReportController@index')->name('index');
                Route::get('reports/member-per-day', 'MemberReportController@memberPerDayChart')->name('chart');
            });

            Route::name('membership.setting.')->group(function() {
                Route::get('settings', 'SettingsController@index')->name('index');
                Route::post('settings', 'SettingsController@store')->name('store');
            });
        });
    });
});

    /* Member Registration Process at Frontend */
    Route::get('member/form', 'Frontend\MemberRegistrationController@registrationFormOld')->name('member.register.form');
    Route::get('member/register', 'Frontend\MemberRegistrationController@registrationForm')->name('member.register.index');
    Route::post('member/form/upload', 'Frontend\MemberRegistrationController@formUpload')->name('member.register.form.upload');
    Route::post('member/save', 'Frontend\MemberRegistrationController@registrationSave')->name('member.register.save');
    Route::get('member/check/nik', 'Frontend\MemberRegistrationController@checkNIK')->name('member.register.check.nik');
    Route::get('member/check/email', 'Frontend\MemberRegistrationController@checkEmail')->name('member.register.check');
    Route::get('member/thanks/{pub_id}', 'Frontend\MemberRegistrationController@registrationThanks')->name('member.register.thanks');
    /* Member Test */
    Route::get('member/test', 'MemberTestController@testInfo')->name('member.test.info');
    Route::get('member/test-result', 'MemberTestResultController@testInfo')->name('member.test.result.info');
    /* Member Registration Payment Confirmation */
    Route::post('member/registration/confirmation', 'Frontend\MemberRegistrationController@paymentConfirmationStore')->name('member.register.confirm.store');
    Route::get('member/registration/confirmation/{id}', 'Frontend\MemberRegistrationController@paymentConfirmation')->name('member.register.confirm');
    Route::patch('member/registration/confirmation/update/{id}', 'Frontend\MemberRegistrationController@paymentConfirmationUpdate')->name('member.register.confirm.update');
    Route::get('member/registration/thanks/{pubid}', 'Frontend\MemberRegistrationController@paymentConfirmationThanks')->name('member.register.confirm.thanks');
    /* File Uploader */
    Route::get('document/uploader', 'Frontend\DocumentUploaderController@create')->name('document.uploader.create');
    Route::post('document/uploader', 'Frontend\DocumentUploaderController@store')->name('document.uploader.store');
    Route::delete('document/uploader', 'Frontend\DocumentUploaderController@destroy')->name('document.uploader.destroy');

    Route::prefix('candidate')->group(function() {
        Route::name('candidate.')->group(function() {
            Route::get('iei/form', 'FormulirKaryawanController@create')->name('form');
            Route::post('iei/save', 'FormulirKaryawanController@store')->name('save');
            Route::get('iei/check/nik', 'FormulirKaryawanController@checkNIK')->name('check.nik');
            Route::get('iei/check/email', 'FormulirKaryawanController@checkEmail')->name('check.email');
            Route::get('iei/thanks/{pub_id}', 'FormulirKaryawanController@registrationThanks')->name('thanks');
        });
    });

    Route::prefix('demo')->group(function() {
        Route::get('member/form', 'Frontend\MemberRegistrationController@registrationFormDemo');
        Route::get('member/check/nik', 'MemberRegistrationController@checkNIK');
        Route::get('member/check/email', 'MemberRegistrationController@checkEmail');
    });

    Route::get('agg/member/active', 'Admin\Report\MemberAggregateController@memberAggActiveInitialDate');
    Route::get('agg/member/active/update', 'Admin\Report\MemberAggregateController@memberAggActiveUpdate');
