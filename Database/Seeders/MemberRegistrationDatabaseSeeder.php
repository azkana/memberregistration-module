<?php

namespace Modules\MemberRegistration\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MemberRegistrationDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("SeedMemberSequencesTableSeeder");
        $this->call(PermissionTableSeeder::class);
    }
}
