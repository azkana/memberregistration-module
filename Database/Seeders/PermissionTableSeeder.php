<?php

namespace Modules\MemberRegistration\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            'member-list',
            'member-create',
            'member-edit',
            'member-delete',
            'member-export',

            'member-test',
            'member-test-info',
            'member-test-info-list',
            'member-test-info-create',
            'member-test-info-edit',
            'member-test-info-delete',
            'member-test-info-import',

            'member-test-result',
            'member-test-result-list',
            'member-test-result-create',
            'member-test-result-edit',
            'member-test-result-delete',
            'member-test-result-import',
        ];

        foreach ($permissions as $row) {

            $permission = Permission::where('name', $row)->first();

            if($permission) {
                $this->command->info('Permission ' .$row. ' already exists.');
            } else {
                Permission::create(['name' => $row]);
                $this->command->info('Permission ' .$row. ' created successfully.');
            }

        }
    }
}
