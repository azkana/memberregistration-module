<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFormulirKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rec_formulir_karyawan', function (Blueprint $table) {
            $table->string('vac_2_loc', 150)->nullable()->after('file_vaksin');
            $table->date('vac_2_date')->nullable()->after('file_vaksin');
            $table->boolean('vac_2')->default(0)->after('file_vaksin');
            $table->string('vac_1_loc', 150)->nullable()->after('file_vaksin');
            $table->date('vac_1_date')->nullable()->after('file_vaksin');
            $table->boolean('vac_1')->default(0)->after('file_vaksin');
            $table->string('vac_type', 50)->nullable()->after('file_vaksin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rec_formulir_karyawan', function (Blueprint $table) {
            $table->dropColumn('vac_type');
            $table->dropColumn('vac_1');
            $table->dropColumn('vac_1_date');
            $table->dropColumn('vac_1_loc');
            $table->dropColumn('vac_2');
            $table->dropColumn('vac_2_date');
            $table->dropColumn('vac_2_loc');
        });
    }
}
