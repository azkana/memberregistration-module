<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateMemberWaitingPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_member_waiting_payment
            AS
            SELECT 
                m.id, m.pub_id, m.nik, m.full_name, m.email, m.reg_date,
                m.gender,
                ms.subs_price + ms.unique_no as invoice_amount, ms.payment_due_date
            FROM members m
            INNER JOIN member_subscriptions ms ON m.pub_id = ms.pub_id
            WHERE
                m.id_member IS NULL 
                AND ms.payment_status = 'WP'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS vw_member_waiting_payment;
        ");
    }
}
