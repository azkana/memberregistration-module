<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_test_results', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('batch_id', 14)->nullable();
            $table->string('nik', 16)->nullable();
            $table->string('member', 10)->nullable();
            $table->string('test', 10)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('keterangan', 200)->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->boolean('is_active')->default(1);

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->timestamps();

            $table->index('nik');
            $table->index('member');
            $table->index('test');
            $table->index('name');
            $table->foreign('batch_id')->references('batch')->on('member_test_result_batches')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_test_results');
    }
}
