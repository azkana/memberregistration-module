<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pub_id', 76)->comment('public ID');
            $table->string('id_member', 10)->nullable()->comment('No. Member');
            $table->string('full_name', 100)->nullable()->comment('Nama Lengkap');
            $table->string('nik', 16)->nullable()->comment('NIK');
            $table->string('email', 100)->nullable()->comment('Email');
            $table->string('no_hp', 30)->nullable()->comment('No. HP');
            $table->string('no_wa', 30)->nullable()->comment('No. WA');
            $table->string('tmp_lahir', 30)->nullable()->comment('Tempat Lahir');
            $table->date('tgl_lahir')->nullable()->comment('Tanggal Lahir');
            $table->string('gender', 1)->nullable()->comment('Gender');
            $table->integer('tb')->nullable()->comment('Tinggi Badan');
            $table->integer('bb')->nullable()->comment('Berat Badan');
            $table->string('alamat', 200)->nullable()->comment('Alamat');
            $table->string('kabupaten', 100)->nullable()->comment('Kabupaten');
            $table->string('provinsi', 70)->nullable()->comment('Provinsi');
            $table->string('kodepos', 5)->nullable()->comment('Kodepos');
            $table->string('jenjang', 10)->nullable()->comment('Jenjang');
            $table->string('sekolah', 100)->nullable()->comment('Sekolah');
            $table->string('jurusan', 100)->nullable()->comment('Jurusan');
            $table->string('thn_lulus', 4)->nullable()->comment('Tahun Lulus');
            /* Pengalaman Kerja */
            $table->string('exp_company_1', 100)->nullable()->comment('Nama Perusahaan / Kantor');
            $table->string('exp_position_1', 100)->nullable()->comment('Bagian / Jabatan');
            $table->string('exp_duration_year_1', 20)->nullable()->comment('Lama Kerja (Tahun)');
            $table->string('exp_duration_month_1', 20)->nullable()->comment('Lama Kerja (Bulan)');
            $table->string('exp_company_2', 100)->nullable()->comment('Nama Perusahaan / Kantor');
            $table->string('exp_position_2', 100)->nullable()->comment('Bagian / Jabatan');
            $table->string('exp_duration_year_2', 20)->nullable()->comment('Lama Kerja (Tahun)');
            $table->string('exp_duration_month_2', 20)->nullable()->comment('Lama Kerja (Bulan)');
            $table->text('skills', 500)->nullable()->comment('Keahlian');
            $table->string('id_card', 70)->nullable()->comment('ID Card');
            $table->string('pas_foto', 70)->nullable()->comment('Pas Foto');
            $table->string('status', 100)->nullable()->comment('Status');
            $table->date('reg_date')->comment('Tanggal Registrasi');
            $table->boolean('is_active')->default(false)->comment('Status Active');
            $table->boolean('mail_to_member')->default(false)->comment('member no sent by email');
            $table->dateTime('approved_date')->comment('Tanggal Approve');
            $table->integer('approved_by')->unsigned()->nullable()->comment('User yang melakukan approve');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
