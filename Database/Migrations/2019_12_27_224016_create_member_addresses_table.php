<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias', 50)->comment('Alias');
            $table->string('id_member', 10)->comment('ID Member');
            $table->string('alamat_1', 100)->comment('Alamat 1');
            $table->string('alamat_2', 100)->comment('Alamat 2');
            $table->string('kabupaten', 50)->comment('Kabupaten');
            $table->string('provinsi', 50)->comment('Provinsi');
            $table->string('kodepos', 5)->comment('Kodepos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_address');
    }
}
