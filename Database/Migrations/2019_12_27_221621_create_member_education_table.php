<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_education', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_member', 10)->comment('ID Member');
            $table->string('institusi', 100)->comment('Nama Institusi');
            $table->string('tahun_lulus', 4)->comment('Tahun Lulus');
            $table->string('kualifikasi', 10)->comment('Kualifikasi');
            $table->string('jurusan', 100)->comment('Jurusan');
            $table->string('nilai_akhir', 10)->comment('Nilai Akhir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_education');
    }
}
