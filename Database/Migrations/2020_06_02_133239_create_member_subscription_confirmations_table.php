<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberSubscriptionConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_subscription_confirmations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_subscription_id')->nullable()->comment('Subscription ID');
            $table->string('bank_account_id', 10)->nullable()->comment('ID Akun Bank');
            $table->double('transfer_amount')->nullable()->comment('Jumlah Transfer');
            $table->date('transfer_date')->nullable()->comment('Tanggal Transfer');
            $table->string('transfer_bank_id', 5)->nullable()->comment('ID Bank');
            $table->string('transfer_bank_name', 100)->nullable()->comment('Nama Bank');
            $table->string('transfer_bank_account', 50)->nullable()->comment('Akun Bank');
            $table->string('transfer_bank_holder_name', 50)->nullable()->comment('Pemengan Akun Bank');
            $table->string('transfer_attachment', 80)->nullable()->comment('Lampiran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_subscription_confirmations');
    }
}
