<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateVwMemberActive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW vw_member_active
            AS
            SELECT 
                m.id, m.pub_id, m.id_member, m.nik, m.full_name, m.email, 
                m.gender, m.kabupaten, m.jenjang, m.reg_date, m.approved_date, m.status,
                m.created_at
            FROM members m
            WHERE
                is_active = 1
            ORDER BY 
                created_at DESC
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS vw_member_active;
        ");
    }
}
