<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulirKaryawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rec_formulir_karyawan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pub_id', 100)->comment('Public ID');
            $table->string('id_member', 10)->nullable()->comment('No. Member');
            $table->string('fullname', 100)->comment('Full Name');
            $table->string('firstname', 30)->nullable()->comment('Full Name');
            $table->string('midname', 30)->nullable()->comment('Middle Name');
            $table->string('lastname', 30)->nullable()->comment('Last Name');
            $table->string('tmp_lahir', 30)->nullable()->comment('Tempat Lahir');
            $table->date('tgl_lahir')->nullable()->comment('Tanggal Lahir');
            $table->string('gender', 1)->nullable()->comment('Gender');
            $table->integer('agama')->nullable()->comment('Agama');
            $table->string('nik', 16)->nullable()->comment('NIK');
            $table->string('ktp_type',20)->comment('Jenis KTP');
            $table->integer('marital')->nullable()->comment('Status Pernikahan');
            $table->string('email', 100)->nullable()->comment('Email');
            $table->string('no_hp', 30)->nullable()->comment('No. HP');
            $table->string('npwp', 15)->nullable()->comment('NPWP');
            $table->string('kk', 16)->nullable()->comment('KK');
            $table->string('mother', 100)->nullable()->comment('Nama Ibu');
            $table->string('father', 100)->nullable()->comment('Nama Ayah');
            // Alamat KTP
            $table->string('alamat', 100)->nullable()->comment('Alamat');
            $table->string('alamat_rt', 5)->nullable()->comment('RT');
            $table->string('alamat_rw', 5)->nullable()->comment('RW');
            $table->string('kelurahan', 50)->nullable()->comment('Kelurahan');
            $table->string('kecamatan', 6)->nullable()->comment('Kecamatan');
            $table->string('kabupaten', 4)->nullable()->comment('Kabupaten');
            $table->string('provinsi', 2)->nullable()->comment('Provinsi');
            $table->string('kodepos', 50)->nullable()->comment('Kodepos');
            $table->string('phone', 50)->nullable()->comment('No. Telpon');
            // Alamat Domisili
            $table->string('alamat_dom', 100)->nullable()->comment('Alamat');
            $table->string('alamat_rt_dom', 5)->nullable()->comment('RT');
            $table->string('alamat_rw_dom', 5)->nullable()->comment('RW');
            $table->string('kelurahan_dom', 50)->nullable()->comment('Kelurahan');
            $table->string('kecamatan_dom', 6)->nullable()->comment('Kecamatan');
            $table->string('kabupaten_dom', 4)->nullable()->comment('Kabupaten');
            $table->string('provinsi_dom', 2)->nullable()->comment('Provinsi');
            $table->string('kodepos_dom', 50)->nullable()->comment('Kodepos');
            $table->string('phone_dom', 50)->nullable()->comment('No. Telpon');
            // Kontak Darurat
            $table->string('emr_name', 100)->nullable()->comment('Nama Lengkap');
            $table->integer('emr_hubkel')->nullable()->comment('Agama');
            $table->string('emr_phone', 50)->nullable()->comment('No. Telpon');
            $table->string('emr_alamat', 100)->nullable()->comment('Alamat');
            $table->string('emr_alamat_rt', 5)->nullable()->comment('RT');
            $table->string('emr_alamat_rw', 5)->nullable()->comment('RW');
            $table->string('emr_kelurahan', 50)->nullable()->comment('Kelurahan');
            $table->string('emr_kecamatan', 6)->nullable()->comment('Kecamatan');
            $table->string('emr_kabupaten', 4)->nullable()->comment('Kabupaten');
            $table->string('emr_provinsi', 2)->nullable()->comment('Provinsi');
            $table->string('emr_kodepos', 50)->nullable()->comment('Kodepos');
            // Pendidikan
            $table->string('jenjang', 20)->nullable()->comment('Jenjang');
            $table->string('sekolah', 100)->nullable()->comment('Nama Sekolah / Universitas');
            $table->string('jurusan', 100)->nullable()->comment('Jurusan');
            $table->string('sekolah_kab', 50)->nullable()->comment('Kabupaten');
            $table->float('thn_lulus')->nullable()->comment('Tahun Lulus');
            // Data Bank
            $table->integer('bank_name')->nullable()->comment('Nama Bank');
            $table->string('bank_account_name', 100)->nullable()->comment('Nama Pemegang Akun Bank');
            $table->string('bank_account_no', 50)->nullable()->comment('No. Rekening Bank');
            $table->string('bank_branch', 100)->nullable()->comment('Cabang Bank');
            // Pengalaman Kerja
            $table->string('exp_company_1', 100)->nullable()->comment('Nama Perusahaan');
            $table->string('exp_company_type_1', 100)->nullable()->comment('Jenis Perusahaan');
            $table->string('exp_position_1', 100)->nullable()->comment('Posisi Terakhir');
            $table->float('exp_salary_1')->nullable()->comment('Gaji');
            $table->string('exp_start_1', 50)->nullable()->comment('Tanggal Mulai Kerja');
            $table->date('exp_end_1')->nullable()->comment('Tanggal Selesai Kerja');
            $table->string('exp_company_2', 100)->nullable()->comment('Nama Perusahaan');
            $table->string('exp_company_type_2', 100)->nullable()->comment('Jenis Perusahaan');
            $table->string('exp_position_2', 100)->nullable()->comment('Posisi Terakhir');
            $table->float('exp_salary_2')->nullable()->comment('Gaji');
            $table->string('exp_start_2', 50)->nullable()->comment('Tanggal Mulai Kerja');
            $table->date('exp_end_2')->nullable()->comment('Tanggal Selesai Kerja');

            $table->integer('jemputan')->nullable()->comment('Lokasi Jemputan');
            $table->date('tgl_tes')->nullable()->comment('Tanggal Tes Tertulis');
            $table->date('skck_validity')->nullable()->comment('Masa berlaku SKCK');

            $table->string('file_ktp', 70)->nullable()->comment('KTP');
            $table->string('file_skck', 70)->nullable()->comment('SKCK');
            $table->string('file_tab', 70)->nullable()->comment('Buku Tabungan');
            $table->string('file_kk', 70)->nullable()->comment('Kartu Keluarga');
            $table->string('file_vaksin', 70)->nullable()->comment('Sertifikat Vaksin');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rec_formulir_karyawan');
    }
}
