<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_member', 10)->comment('ID Member');
            $table->string('posisi', 100)->comment('Posisi');
            $table->string('nama_pt', 100)->comment('Nama Perusahaan');
            $table->string('durasi', 100)->comment('Lama Bekerja');
            $table->text('keterangan')->comment('Keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_experiences');
    }
}
