<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateMemberNeedApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW vw_member_need_approval
            AS
            SELECT 
                m.id, ms.pub_id, m.nik, m.full_name, m.email, m.reg_date,
                msc.transfer_date, msc.transfer_amount,
                ms.payment_due_date, ms.payment_status, ms.subs_price + ms.unique_no as invoice_amount,
                msc.created_at 
            FROM member_subscription_confirmations msc
            INNER JOIN member_subscriptions ms ON msc.member_subscription_id = ms.id
            INNER JOIN members m ON ms.pub_id = m.pub_id
            WHERE 
                ms.payment_status = 'CP'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS vw_member_need_approval;
        ");
    }
}
