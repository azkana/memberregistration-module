<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pub_id', 76)->nullable()->comment('public ID');
            $table->double('subs_price')->nullable()->comment('Biaya Member');
            $table->integer('unique_no')->nullable()->comment('Kode Unik');
            $table->datetime('payment_due_date')->nullable()->comment('Jatuh Tempo Pembayaran');
            $table->string('payment_status', 10)->nullable()->comment('Status Pembayaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_subscriptions');
    }
}
