<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;

class FormulirKaryawan extends Model
{
    protected $table    = 'rec_formulir_karyawan';
    protected $dates    = ['tgl_lahir', 'tgl_tes', 'skck_validity', 'vac_1_date', 'vac_2_date'];
    protected $fillable = [
        'pub_id',
        'id_member',
        'fullname',
        'firstname',
        'midname',
        'lastname',
        'tmp_lahir',
        'tgl_lahir',
        'gender',
        'agama',
        'nik',
        'ktp_type',
        'marital',
        'email',
        'no_hp',
        'npwp',
        'kk',
        'mother',
        'father',
        'alamat',
        'alamat_rt',
        'alamat_rw',
        'kelurahan',
        'kecamatan',
        'kabupaten',
        'provinsi',
        'kodepos',
        'phone',
        'alamat_dom',
        'alamat_rt_dom',
        'alamat_rw_dom',
        'kelurahan_dom',
        'kecamatan_dom',
        'kabupaten_dom',
        'provinsi_dom',
        'kodepos_dom',
        'phone_dom',
        'emr_name',
        'emr_hubkel',
        'emr_phone',
        'emr_alamat',
        'emr_alamat_rt',
        'emr_alamat_rw',
        'emr_kelurahan',
        'emr_kecamatan',
        'emr_kabupaten',
        'emr_provinsi',
        'emr_kodepos',
        'jenjang',
        'sekolah',
        'jurusan',
        'sekolah_kab',
        'thn_lulus',
        'bank_name',
        'bank_account_name',
        'bank_account_no',
        'bank_branch',
        'exp_company_1',
        'exp_company_type_1',
        'exp_position_1',
        'exp_salary_1',
        'exp_start_1',
        // 'exp_end_1',
        'exp_company_2',
        'exp_company_type_2',
        'exp_position_2',
        'exp_salary_2',
        'exp_start_2',
        // 'exp_end_2',
        'jemputan',
        'tgl_tes',
        'skck_validity',

        'file_ktp',
        'file_skck',
        'file_tab',
        'file_kk',
        'file_vaksin',

        'vac_type',
        'vac_1',
        'vac_1_date',
        'vac_1_loc',
        'vac_2',
        'vac_2_date',
        'vac_2_loc',
    ];

    public function religion() {
    	return $this->belongsTo('App\Models\Master\Agama', 'agama');
    }

    public function bankAccount() {
    	return $this->belongsTo('App\Models\Master\Bank', 'bank_name');
    }

    public function rel_provinsi_ktp() {
    	return $this->belongsTo('App\Models\Master\Provinsi', 'provinsi', 'code');
    }

    public function rel_kab_ktp() {
    	return $this->belongsTo('App\Models\Master\Kabupaten', 'kabupaten', 'code');
    }

    public function rel_kec_ktp() {
    	return $this->belongsTo('App\Models\Master\Kecamatan', 'kecamatan', 'code');
    }

    public function rel_provinsi_emr() {
    	return $this->belongsTo('App\Models\Master\Provinsi', 'emr_provinsi', 'code');
    }

    public function rel_kab_emr() {
    	return $this->belongsTo('App\Models\Master\Kabupaten', 'emr_kabupaten', 'code');
    }

    public function rel_kec_emr() {
    	return $this->belongsTo('App\Models\Master\Kecamatan', 'emr_kecamatan', 'code');
    }

    public function rel_provinsi_dom() {
    	return $this->belongsTo('App\Models\Master\Provinsi', 'provinsi_dom', 'code');
    }

    public function rel_kab_dom() {
    	return $this->belongsTo('App\Models\Master\Kabupaten', 'kabupaten_dom', 'code');
    }

    public function rel_kec_dom() {
    	return $this->belongsTo('App\Models\Master\Kecamatan', 'kecamatan_dom', 'code');
    }
}
