<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberTest extends Model
{
    use TraitsUuid;

    protected $table = 'member_tests';

    public $incrementing = false;

    protected $fillable = [
        'batch_id',
        'nik',
        'member',
        'test',
        'name',
        'test_date',
        'start_date',
        'end_date',
        'is_active',
        'created_by',
        'updated_by'
    ];

    public function getBatch()
    {
        return $this->belongsTo(MemberTestBatch::class, 'batch_id', 'batch')
            ->withDefault([
                'batch' => null
            ]);
    }
}
