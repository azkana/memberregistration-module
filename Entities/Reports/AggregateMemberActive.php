<?php

namespace Modules\MemberRegistration\Entities\Reports;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;
use Carbon\CarbonPeriod;
use Carbon\Carbon;

class AggregateMemberActive extends Model
{
    use TraitsUuid;

    protected $table = 'member_agg_active';

    public $incrementing    = false;

    protected $fillable = [
        'date', 'gender', 'count'
    ];

    /**
     * Generate Aggregate for a month
     * Executed every first day of the month
     */
    public static function _generateDate()
    {
        $start  = Carbon::now()->startofMonth()->format('Y-m-d');
        $end    = Carbon::now()->endOfMonth()->format('Y-m-d');

        $period = new CarbonPeriod($start, '1 day', $end);

        $gender = ['L', 'P'];

        foreach ($period as $dt) {

            foreach ($gender as $gen) {

                $data['date']   = $date = $dt->format('Y-m-d');
                $data['gender']   = $gen;
                $data['count']  = 0;

                $check = self::_checkDate($date, $gen);

                if($check == true) {
                    AggregateMemberActive::create($data);
                }

            }

        }
    }

    /**
     * Check aggregate date is already exists or not
     * @param date $date
     * @param string $gen
     * @return boolean
     */
    private static function _checkDate($date, $gen)
    {
        $data = AggregateMemberActive::where('date', $date)->where('gender', $gen)->count();

        if($data > 0) {
            return false;
        }

        return true;
    }

    /**
     * Update Aggregate Count
     * @param array $datas
     */
    public static function _updateAggregate($datas = array())
    {
        foreach ($datas as $data) {

            $check = self::_checkDate($data->date, $data->gen);

            if($check == false) {

                AggregateMemberActive::where('date', $data->date)->where('gender', $data->gen)
                ->update([
                    'count' => $data->count
                ]);

            }
        }
    }

    /**
     * Update Aggregate daily
     * @param date $date
     */
    public static function _getAggregate($date)
    {
        $data = \DB::table('members')
            ->selectRaw('reg_date as date, gender as gen, count(id_member) as count')
            ->where('reg_date', $date)
            ->groupBy('reg_date', 'gender')
            ->get();
        return $data;
    }

}
