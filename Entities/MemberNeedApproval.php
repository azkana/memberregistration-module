<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberNeedApproval extends Model
{
    protected $table    = 'vw_member_need_approval';
}
