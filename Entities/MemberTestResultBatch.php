<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberTestResultBatch extends Model
{
    use TraitsUuid;

    protected $table = 'member_test_result_batches';

    public $incrementing    = false;

    protected $fillable = [
        'batch',
        'name',
        'desc',
        'start_date',
        'end_date',
        'is_active',
        'created_by',
        'updated_by'
    ];

    public static function _generateBatch()
    {
        return uniqid();
    }

    public function batchs()
    {
        return $this->hasMany(MemberTest::class, 'batch_id', 'batch')
            ->orderByRaw("CONVERT(test, UNSIGNED)");
    }
}
