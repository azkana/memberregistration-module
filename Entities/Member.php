<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Member extends Model
{
    use Notifiable;
    
    protected $dates    = ['tgl_lahir', 'reg_date'];
    protected $fillable = [
        'pub_id',
        'id_member',
        'full_name',
        'nik',
        'email',
        'no_hp',
        'no_wa',
        'tmp_lahir',
        'tgl_lahir',
        'gender',
        'blood_type',
        'tb',
        'bb',
        'alamat',
        'kabupaten',
        'provinsi',
        'kodepos',
        'jenjang',
        'sekolah',
        'jurusan',
        'thn_lulus',
        'exp_company_1',
        'exp_position_1',
        'exp_duration_year_1',
        'exp_duration_month_1',
        'exp_company_2',
        'exp_position_2',
        'exp_duration_year_2',
        'exp_duration_month_2',
        'status',
        'skills',
        'id_card',
        'pas_foto',
        'reg_date',
        'is_active',
        'mail_to_member',
        'approved_date',
        'approved_by'
    ];

    public function rel_provinsi() {
        return $this->belongsTo('App\Models\Master\Provinsi', 'provinsi', 'code');
    }

    public function has_subs() {
        return $this->hasOne('Modules\MemberRegistration\Entities\MemberSubscription', 'pub_id', 'pub_id');
    }

    public function rel_user() {
        return $this->belongsTo('App\Models\User', 'approved_by');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($member) {
            $member->has_subs()->delete();
        });
    }
}
