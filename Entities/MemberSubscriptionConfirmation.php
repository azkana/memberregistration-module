<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberSubscriptionConfirmation extends Model
{
    protected $table    = 'member_subscription_confirmations';
    protected $fillable = [
        'member_subscription_id',
        'bank_account_id',
        'transfer_amount',
        'transfer_date',
        'transfer_bank_id',
        'transfer_bank_name',
        'transfer_bank_account',
        'transfer_bank_holder_name',
        'transfer_attachment',
    ];

    public function member_subscription() {
    	return $this->belongsTo('Modules\MemberRegistration\Entities\MemberSubscription', 'member_subscription_id', 'id');
    }

    public function bank_rel() {
        return $this->belongsTo('App\Models\Master\Bank', 'transfer_bank_id', 'code');
    }
}
