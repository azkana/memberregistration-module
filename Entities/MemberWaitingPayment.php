<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberWaitingPayment extends Model
{
    protected $table    = 'vw_member_waiting_payment';
}
