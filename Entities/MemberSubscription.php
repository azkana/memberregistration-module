<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberSubscription extends Model
{
    protected $table    = 'member_subscriptions';
    protected $fillable = [
        'pub_id',
        'subs_price',
        'unique_no',
        'payment_due_date',
        'payment_status'
    ];

    public function member() {
    	return $this->belongsTo('Modules\MemberRegistration\Entities\Member', 'pub_id', 'pub_id');
    }

    public function has_confirmation() {
        return $this->hasOne('Modules\MemberRegistration\Entities\MemberSubscriptionConfirmation', 'member_subscription_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($memberSubscription) {
            $memberSubscription->has_confirmation()->delete();
        });
    }
}
