<?php

namespace Modules\MemberRegistration\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberActive extends Model
{
    protected $table = 'vw_member_active';
}
