<?php

namespace Modules\MemberRegistration\Exports;

use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Modules\MemberRegistration\Entities\FormulirKaryawan;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class FormulirKaryawanExport extends DefaultValueBinder implements FromView, ShouldAutoSize, WithCustomValueBinder, WithColumnFormatting, WithEvents
{

    public $data;
    protected $template;

    public function __construct($data = '')
    {
        $this->data = $data;
        $this->template = getOptions('iei_export_template');
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        if(is_string($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function view(): View
    {
        // $params = $this->data;
        if($this->template == '1') {
            $view = 'template-1';
        } elseif($this->template == '2') {
            $view = 'template-2';
        } else {
            $view = 'template-1';
        }
        return view('memberregistration::exports.iei.'.$view, [
            'karyawan' => FormulirKaryawan::all()
            // 'karyawan' => FormulirKaryawan::where('created_at', '<', $params['date'])
            //     ->get()
        ]);
    }

    public function columnFormats(): array
    {

        $templateColumnFormat1 = [
            'A' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'I' => NumberFormat::FORMAT_TEXT,
            'N' => NumberFormat::FORMAT_TEXT,
            'S' => NumberFormat::FORMAT_TEXT,
            'X' => NumberFormat::FORMAT_TEXT,
            'Y' => NumberFormat::FORMAT_TEXT,
            'AD' => NumberFormat::FORMAT_TEXT,
            'AE' => NumberFormat::FORMAT_TEXT,
            'AJ' => NumberFormat::FORMAT_TEXT,
            'AK' => NumberFormat::FORMAT_TEXT,
            'AM' => NumberFormat::FORMAT_TEXT,
            'AN' => NumberFormat::FORMAT_TEXT,
            'AQ' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'AU' => NumberFormat::FORMAT_TEXT,
            'AY' => NumberFormat::FORMAT_NUMBER,
            'BD' => NumberFormat::FORMAT_NUMBER,
            'BH' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];

        $templateColumnFormat2 = [
            'A' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'I' => NumberFormat::FORMAT_NUMBER,
            'J' => NumberFormat::FORMAT_NUMBER,
            'K' => NumberFormat::FORMAT_TEXT,
            'Q' => NumberFormat::FORMAT_TEXT,
            'V' => NumberFormat::FORMAT_TEXT,
            'AA' => NumberFormat::FORMAT_TEXT,
            'AB' => NumberFormat::FORMAT_TEXT,
            'AG' => NumberFormat::FORMAT_TEXT,
            'AH' => NumberFormat::FORMAT_TEXT,
            'AM' => NumberFormat::FORMAT_TEXT,
            'AN' => NumberFormat::FORMAT_TEXT,
            'AP' => NumberFormat::FORMAT_TEXT,
            'AQ' => NumberFormat::FORMAT_TEXT,
            'AV' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'AX' => NumberFormat::FORMAT_TEXT,
        ];

        if($this->template == '1') {
            return $templateColumnFormat1;
        } elseif($this->template == '2') {
            return $templateColumnFormat2;
        } else {
            return $templateColumnFormat1;
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event)
            {
                $event->sheet->getDelegate()->getParent()->getProperties()
                    ->setCreator('Azkana Media')
                    ->setTitle('Kandidat EPSON')
                    ->setCategory('Recruitment');

                $event->sheet->getDelegate()->setShowGridlines(false);

                $headerCellRange = 'A1:AX3';
                $event->sheet->getDelegate()->getStyle($headerCellRange)->applyFromArray($this->headerStyle);

                $latestColumnRow = $event->sheet->getDelegate()->getHighestRow('AX');
                $dataCellRange = 'A4:AX'.$latestColumnRow;
                $event->sheet->getDelegate()->getStyle($dataCellRange)->applyFromArray($this->dataStyle);
            }
        ];
    }

    private $headerStyle = array(
        'font' => array(
            'name' => 'Arial',
            'color' => array('rgb' => '000000'),
            'size' => 10
        ),
        'alignment' => array(
            'horizontal' => Alignment::HORIZONTAL_CENTER,
            'vertical' => Alignment::VERTICAL_CENTER
        ),
        'borders' => array(
            'allborders' => array(
                'style' => Border::BORDER_MEDIUM,
                'color' => array('rgb' => '808080')
            )
        )
    );

    private $dataStyle = array(
        'font' => array(
            'name' => 'Arial',
            'color' => array('rgb' => '000000'),
            'size' => 10
        ),
        'borders' => array(
            'inside' => array(
                'style' => Border::BORDER_MEDIUM,
                'color' => array('rgb' => '808080')
            )
        )
    );

}
