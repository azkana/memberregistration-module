<?php

namespace Modules\MemberRegistration\Exports;

use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\MemberRegistration\Entities\Member;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class MemberExport extends DefaultValueBinder implements FromView, ShouldAutoSize, WithCustomValueBinder, WithColumnFormatting
{
    public $data;

    public function __construct($data = '')
    {
        $this->data = $data;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        if(is_string($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function view(): View
    {
        $params = $this->data;
        $member = new Member();
        if(!empty($params['date_from']) AND !empty($params['date_to'])) {
            if($params['date_type'] == 'approvedate') {
                $member = $member->whereDate('approved_date', '>=', $params['date_from']);
                $member = $member->whereDate('approved_date', '<=', $params['date_to']);
            }

            if($params['date_type'] == 'regdate') {
                $member = $member->whereDate('reg_date', '>=', $params['date_from']);
                $member = $member->whereDate('reg_date', '<=', $params['date_to']);
            }
        }
        if(!empty($params['gender'])) {
            $member = $member->where('gender', $params['gender']);
        }
        $member = $member->where('id_member', '!=', '')
            ->whereHas('has_subs', function ($q) {
                return $q->where('payment_status', 'AM');
        });
        $member = $member->
            orderBy('id_member', 'ASC')->get();
        $data['data']   = $member;
        return view('memberregistration::exports.members', $data);
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'B' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
            'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'K' => NumberFormat::FORMAT_NUMBER,
            'L' => NumberFormat::FORMAT_NUMBER,
            'P' => NumberFormat::FORMAT_TEXT,
            'T' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
